<?php

include_spip('inc/utils');
include_spip('inc/formidable');
include_spip('inc/formidable_fichiers');
include_spip('inc/saisies');
include_spip('inc/filtres');
include_spip('inc/texte');
include_spip('facteur_fonctions');

function traiter_email_dist($args, $retours) {
	// Vérifier si on doit envoyer en cas de modification de réponses
	if (
			($retours['modification_reponse'] ?? '') == true
			&& ($args['options']['modification_reponse'] ?? '') == 'on'
	) {
		$retours['traitements']['email'] = true;
		return $retours;
	}

	$envoi_ok = true;// Passe à false si on échoue lors de l'envoi
	$envoi_done = false; // Passe à true si on tente effectivement d'envoyer quelque chose, reste à false si on n'envoie rien dans l'immédiat
	$retours['timestamp'] = time();
	$id_formulaire = $args['id_formulaire'];
	$options = $args['options'];


	$saisies = $args['saisies'];





	// Récuperer les valeurs saisies
	list($valeurs, $valeurs_libellees) = formidable_tableau_valeurs_saisies($saisies);

	$moment = 'prop'; // Par défaut, on envoie le mail prévu pour être envoyé immédiatement après la soumission du formulaire

	// Mais si on est sur la modification d'une réponse
	// on vérifie
	// 1. Si jamais il y a modération a priori ou a posteriori
	// 2. Si modération a priori, on vérifie le statut actuelle de la réponse, et on a adapte le cas échéant le moment
	// Si proposé, on envoi le mail post soumission
	// Si publié, on envoi le mail post publication
	//
	// le statut : si c'est publié, alors on chercher la validation au moment de la publication
	if (
		($retours['modification_reponse'] ?? false)
		&& (($args['traitements']['enregistrement']['moderation'] ?? '') === 'priori')
	) {
		$statut_reponse = sql_getfetsel('statut', 'spip_formulaires_reponses', 'id_formulaires_reponse = ' .  $retours['id_formulaires_reponse']);
		if ($statut_reponse === 'publie') {
			$moment = 'publie';
		}
	}
	// Savoir si modéré à posteriori, on considère que si pas enregistré, c'est que modéré à postériori
	$moderation_a_posteriori = ($args['traitements']['enregistrement']['moderation'] ?? 'posteriori') === 'posteriori';
	if (
		($options['activer_responsable'] ?? '')
		&& (
			$moderation_a_posteriori
			|| in_array($moment, $options['quand_envoyer_responsable'] ?? ['prop'])//?? ['prop'] pour retrocompat
		)
	) {
		$envoi_ok = $envoi_ok && traiter_email_envoyer_destinataires(
			$args,
			$retours,
			$saisies,
			$valeurs,
			$moment
		);
		$envoi_done = true;
	}

	if (
		($options['activer_accuse'] ?? '')
		&& (
			$moderation_a_posteriori
			|| in_array($moment, $options['quand_envoyer_accuse'] ?? ['prop'])//?? ['prop'] pour retrocompat
		)
	) {
		$envoi_ok = $envoi_ok && traiter_email_envoyer_accuse(
			$args,
			$retours,
			$saisies,
			$valeurs,
			$moment,
		);
		$envoi_done = true;
	}

	// Gestion du message retour
	if ($envoi_ok) {
		if (!$args['message_retour_general'] && $envoi_done) {
			// Si jamais message de retour general, ne pas mettre le message de retour spécifique. Cf https://git.spip.net/spip-contrib-extensions/formidable/issues/58
			// Ne mettre un message que si on a effectivement envoyé, et pas si on envoi tout en décalé
			$retours['message_ok'] = _T('formidable:traiter_email_message_ok');
		}
	} else {
			$retours['message_erreur'] = _T('formidable:traiter_email_message_erreur');
	}

	// noter qu'on a deja fait le boulot, pour ne pas risquer double appel
	$retours['traitements']['email'] = true;
	return $retours;
}

/**
 * Envoyer un mail après institution d'une réponse
 * @param array $flux le flux du pipeline post_edition
 * @param array $options les options du traitement
 * @return array $flux le flux modifié
 **/
function traiter_email_instituer_reponse(array $flux, array $options): array {
	$args = $flux['args'];
	$args['options'] = $options;
	$retours = [
		'id_formulaires_reponse' => $flux['args']['id_objet'],
		'id_formulaire' => $flux['args']['formulaire']['id_formulaire']
	];
	$id_formulaire = $retours['id_formulaire'];

	// Normaliser
	if (!($options['quand_envoyer_responsable'] ?? '')) {
		!$options['quand_envoyer_responsable'] = [];
	}
	if (!($options['quand_envoyer_accuse'] ?? '')) {
		!$options['quand_envoyer_accuse'] = [];
	}


	$statut_nouveau = $flux['data']['statut'];
	$envoi_ok = true;
	$id_formulaire = $flux['args']['id_formulaire'];

	$valeurs_bases = sql_select('nom, valeur', 'spip_formulaires_reponses_champs', 'id_formulaires_reponse = ' . $flux['args']['id_objet']);
	$valeurs = [];
	while ($row = sql_fetch($valeurs_bases)) {
		$valeurs[$row['nom']] = $row['valeur'];
	}

	$saisies = $flux['args']['saisies'];


	$saisies_fond_notification = traiter_email_saisies_fond_notification($saisies, $options, $valeurs);



	// Ce qui est fichier
	$saisies_fichiers = saisies_lister_avec_type($saisies, 'fichiers');
	// On fait comme si on venait juste de poster
	$retours['fichiers'] = [];
	foreach (array_keys($saisies_fichiers) as $fichier) {
		if ($valeurs[$fichier] ?? '') {
			$retours['fichiers'][$fichier] = formidable_deserialize($valeurs[$fichier]);
		}
	}



	if ($statut_nouveau === 'publie' && in_array('publie', $options['quand_envoyer_responsable'] ?? [])) {
		$envoi_ok = $envoi_ok && traiter_email_envoyer_destinataires(
			$args,
			$retours,
			$saisies,
			$valeurs,
			'publie',
		);
	}

	if (
		$statut_nouveau === 'publie'
		&& in_array('publie', $options['quand_envoyer_accuse'])
	) {
		$envoi_ok = $envoi_ok && traiter_email_envoyer_accuse(
			$args,
			$retours,
			$saisies,
			$valeurs,
			'publie',
		);
	}
	return $flux;
}
/**
 * Déterminer les saisies qu'on envoie pour construire le fond de notification
 * @param array $saisies
 * @param array $options (du traitement)
 * @param array $valeurs la liste des valeurs
 * @return array $saisies
 **/
function traiter_email_saisies_fond_notification(array $saisies, array $options, array $valeurs): array {
	$champs = saisies_lister_champs($saisies);
	if ($options['exclure_champs_email'] ?? '') {
		$champs_a_exclure = explode(',', $options['exclure_champs_email']);
		$champs = array_diff($champs, $champs_a_exclure);
		foreach ($champs_a_exclure as $champ_a_exclure) {
			$champ_a_exclure = formidable_trim($champ_a_exclure);
			$champ_a_exclure = str_replace('@', '', $champ_a_exclure);
			$saisies = saisies_supprimer($saisies, $champ_a_exclure);
		}
	}

	if (($options['masquer_champs_vides'] ?? '') === 'on') {
		$saisies = saisies_supprimer_sans_reponse($saisies, $valeurs);
	}
	return $saisies;
}

/**
 * Générer la liste des destinataires (hors accusé de reception)
 * @param array $args les args du traitement
 * @param array $retours les infos sur le retours
 * @param array $valeurs les valeurs (en provenance de _request ou de base)
 * @return array $destinataires
**/
function traiter_email_destinataires(array $args, array $retours, array $valeurs): array {
	$options = $args['options'];
	$destinataires = [];

	/**
	 * Les différents types de destinataires
	 * 1. Auteur·trices (toujours)
	 * 2. Email (pour raison historique , appelé "destinataire_plus" dans le code) (toujours)
	 * 3. En fonction d'une saisie (type radio/select) (toujours)
	 * 4. Par champ de formulaire de type destinataires/caché (toujours)
	 * 5. Par champ de formulaire de type input: conservé pour raison historique, mais déprécié (toujours)
	 * 6. Par appel de squelette (uniquement lors de la soumission du formulaire... ca n'a pas de sens sinon)
	 * 7. Par pipeline (toujours)
	**/

	// 1. Les auteurs·trices destinataires
	if ($options['destinataires_auteurs'] ?? []) {
		include_spip('base/abstract_sql');
		$destinataires = array_merge(
			$destinataires,
			array_column(
				sql_allfetsel('email', 'spip_auteurs', 'sql_in'('id_auteur', $options['destinataires_auteurs'])),
				'email'
			)
		);
	}

	// 2. On ajoute les destinataires "en plus" c'est à dire les adresses emails
	if ($options['destinataires_plus']) {
		$destinataires_plus = explode(',', $options['destinataires_plus']);
		$destinataires = array_merge($destinataires, $destinataires_plus);
	}


	// 3. On ajoute les destinataires en fonction des choix de saisie dans le formulaire
	// @selection_1@/choix1 : mail@domain.tld
	// @selection_1@/choix2 : autre@domain.tld, lapin@domain.tld
	if (!empty($options['destinataires_selon_champ'])) {
		if ($destinataires_selon_champ = traiter_email_destinataire_selon_champ($options['destinataires_selon_champ'], $valeurs)) {
			$destinataires = array_merge($destinataires, $destinataires_selon_champ);
		}
	}

	// 4. On récupère les destinataires passés par un champ de formulaire
	if (isset($options['champ_destinataires'])) {
		$destinataires = array_merge($destinataires, traiter_email_champ_destinataire($options['champ_destinataires'], $valeurs));
	}

	// 5. Conservé pour raison historique, mais mauvaise pratique
	// Cf fichier .yaml
	if ($options['champ_courriel_destinataire_form']) {
		$courriel_champ_form = _request($options['champ_courriel_destinataire_form'], $valeurs);
		$destinataires[] = $courriel_champ_form;
	}

	// 6. Les destinataires éventuellement passés au formulaire dans le squelette
	$traiter_email_destinataires = $args['options_appel']['traiter_email_destinataires'] ?? [];
	if (!is_array($traiter_email_destinataires)) {
		$traiter_email_destinataires = explode(',', $traiter_email_destinataires);
	}
	if (($args['options_appel']['traiter_email_destinataires_methode'] ?? 'ajouter') === 'remplacer') {
		$destinataires = $traiter_email_destinataires;
	} else {
		$destinataires = array_merge($destinataires, $traiter_email_destinataires);
	}

	// 7. On fini par passer tout cela au pipeline (par ex pour le plugin formidable_participation_destinataire_supplementaire)
	$destinataires = pipeline('formidable_traiter_email_destinataires', [
		'args' => array_merge(
			$args,
			[
				'id_formulaires_reponse' => isset($retours['id_formulaires_reponse']) ? $retours['id_formulaires_reponse'] : '',
				'id_formulaire' => $args['formulaire']['id_formulaire']//Un peu redondant, mais ca aide
			]
		),
		'data' => $destinataires]);


	// S'assurer que les destinataires ne soient pas en doublons
	$destinataires = array_map('formidable_trim', $destinataires);
	$destinataires = array_unique($destinataires);

	return $destinataires;
}


/**
 * Retourne la liste des destinataires mentionnés dans un ou plusieurs champs destinataires (auteur·e·s enregistré·e·s)
 * @param array|string $champ l'option champ_destinataire
 * @param array $valeurs les valeurs (soit en _request, soit depuis la base)
 * @return array la liste des mails destinataires.
**/
function traiter_email_champ_destinataire($champ, array $valeurs) {
	if (!is_array($champ)) {
		$champ = [$champ];
	}
	//Trouver tout les id destinataires
	$destinataires = [];
	foreach ($champ as $c) {
		$destinataires_c = _request($c, $valeurs);
		if (!is_array($destinataires_c)) {
			$destinataires_c = formidable_deserialize($destinataires_c);
			if (is_array($destinataires_c)) {// Si on a reussi à convertir en array, champagne, plus rien à faire
			} else if (intval($destinataires_c)) {
				$destinataires_c = [$destinataires_c];
			} else {
				$destinataires_c = [];
			}
		}
		$destinataires = array_merge($destinataires, $destinataires_c);
	}
	if (count($destinataires)) {
		// On récupère les mails des destinataires
		$destinataires = array_map('intval', $destinataires);
		$destinataires = sql_allfetsel(
			'email',
			'spip_auteurs',
			sql_in('id_auteur', $destinataires)
		);
		$destinataires = array_column($destinataires, 'email');
	}
	return $destinataires;
}
/**
 * Retourne la liste des destinataires sélectionnés en fonction
 * de l'option 'destinataires_selon_champ' du traitement email.
 *
 * @param string $description
 *     Description saisie dans l'option du traitement du formulaire,
 *     qui respecte le schéma prévu, c'est à dire : 1 description par ligne,
 *     tel que `@champ@/valeur : mail@domain.tld, mail@domain.tld, ...`
 *     {@example : `@selection_2@/choix_1 : toto@domain.tld`}
 * @param array $valeurs les valeurs postées / prise en base
 * @return array
 *     Liste des destinataires, s'il y en a.
 **/
function traiter_email_destinataire_selon_champ($description, array $valeurs) {
	$destinataires = [];

	// 1 test à rechercher par ligne
	$descriptions = explode("\n", formidable_trim($description));
	$descriptions = array_map('formidable_trim', $descriptions);
	$descriptions = array_filter($descriptions);

	// pour chaque test, s'il est valide, ajouter les courriels indiqués
	foreach ($descriptions as $test) {
		// Un # est un commentaire
		if ($test[0] == '#') {
			continue;
		}
		// Le premier caractère est toujours un @
		if ($test[0] != '@') {
			continue;
		}


		list($champ, $reste) = explode('/', $test, 2);
		$champ = substr(formidable_trim($champ), 1, -1); // enlever les @

		if ($reste) {
			list($valeur, $mails) = explode(':', $reste, 2);
			$valeur = formidable_trim($valeur);
			$mails = explode(',', $mails);
			$mails = array_map('formidable_trim', $mails);
			$mails = array_filter($mails);
			if ($mails) {
				// obtenir la valeur du champ saisi dans le formulaire.
				// cela peut être un tableau.
				$champ = _request($champ, $valeurs);
				if (!is_null($champ)) {
					$ok = is_array($champ) ? in_array($valeur, $champ) : ($champ == $valeur);

					if ($ok) {
						$destinataires = array_merge($destinataires, $mails);
					}
				}
			}
		}
	}

	return $destinataires;
}

/**
 * Gère une saisie de type fichiers dans le traitement par email.
 * C'est à dire:
 *	- S'il y a eu un enregistement avant, ne déplace pas le fichier
 *	- S'il n'y a pas eu d'enregistrement avant, déplace le fichier
 *		dans un dossier nommé en fonction du timestamp du traitement
 *	- Renvoie un tableau décrivant les fichiers, avec une url d'action sécurisée valable seulement
 *		_FORMIDABLE_EXPIRATION_FICHIERS_EMAIL (sauf si cette constantes est définie à 0)
 * @param array $saisie la description de la saisie
 * @param string $nom le nom de la saisie
 * @param int|string $id_formulaire le formulaire concerné
 * @param array $retours ce qu'a envoyé le précédent traitement
 * @param int $timestamp un timestamp correspondant au début du processus de création du courriel
 * @return array un tableau décrivant la saisie
 **/
function traiter_email_fichiers($saisie, $nom, $id_formulaire, $retours, $timestamp) {
	//Initialisation
	$id_formulaire = strval($id_formulaire);//précaution
	$vue = [];
	$id_formulaires_reponse = $retours['id_formulaires_reponse'] ?? '';
	if ($id_formulaires_reponse) { // cas simple: les réponses ont été enregistrées
		if (isset($retours['fichiers'][$nom])) { // petite précaution
			$options = [
				'id_formulaire' => $id_formulaire,
				'id_formulaires_reponse' => $retours['id_formulaires_reponse']
			];
			$vue = traiter_email_ajouter_action_recuperer_fichier_par_email($retours['fichiers'][$nom] ?? [], $nom, $options);
		}
	} else { // si les réponses n'ont pas été enregistrées
		$vue = formidable_deplacer_fichiers_produire_vue_saisie($saisie, ['id_formulaire' => $id_formulaire, 'timestamp' => $timestamp]);
			$options = [
				'id_formulaire' => $id_formulaire,
				'timestamp' => $timestamp
			];
			$vue = traiter_email_ajouter_action_recuperer_fichier_par_email($vue, $nom, $options);
	}

	return $vue;
}

/**
 * Ajuster les expéditeurs
 *	- avoir toujours un expediteurs
 *	- le cas échéant forcer le from
 * @param array $corps
 * @param bool $forcer_from
 * @return array
**/
function traiter_email_ajuster_expediteur(array $corps, bool $forcer_from): array {
	include_spip('inc/config');
	$config_facteur = lire_config('facteur/adresse_envoi');

	if (!($corps['from'] ?? '')) {
		if ($config_facteur === 'oui') {
			$corps['from'] = lire_config('facteur/adresse_envoi_email');
		} else {
			$corps['from'] = lire_config('email_webmaster');
		}
	}

	if (!($corps['nom_envoyeur'] ?? '')) {
		if ($config_facteur === 'oui') {
			$corps['nom_envoyeur'] = lire_config('facteur/adresse_envoi_nom');
		} else {
			$corps['nom_envoyeur'] = lire_config('nom_site');
		}
	}
	$corps['nom_envoyeur'] = filtrer_entites(
		supprimer_tags(
			typo($corps['nom_envoyeur'])
		)
	);

	if ($forcer_from) {
		$domaine_from = explode('@', $corps['from']);
		$domaine_from = end($domaine_from);

		$domaine_spip = parse_url(lire_config('adresse_site'), PHP_URL_HOST);
		if ($domaine_spip && $domaine_from !== $domaine_spip) {
			$corps['repondre_a'] = $corps['from'];
			if ($config_facteur === 'oui') {
				$corps['from'] = lire_config('facteur/adresse_envoi_email');
			} else {
				$corps['from'] = lire_config('email_webmaster');
			}
		}
	}
	return $corps;
}

/**
 * Pour une saisie de type 'fichiers'
 * insère dans la description du résultat de cette saisie
 * l'url de l'action pour récuperer la saisie par email
 * Ajoute également une vignette correspondant à l'extension
 * @param array $saisie_a_modifier
 * @param string $nom_saisie
 * @param array $options options qui décrit l'endroit où est stocké le fichier
 * @return array $saisie_a_modifier
 **/
function traiter_email_ajouter_action_recuperer_fichier_par_email($saisie_a_modifier, $nom_saisie, $options) {
	if (!$saisie_a_modifier) {
		return $saisie_a_modifier;
	}
	$vignette_par_defaut = charger_fonction('vignette', 'inc/');

	if (_FORMIDABLE_EXPIRATION_FICHIERS_EMAIL > 0) {
		$delai = traiter_email_secondes_en_jour(_FORMIDABLE_EXPIRATION_FICHIERS_EMAIL);
	}
	foreach ($saisie_a_modifier as $i => $valeur) {
		$url = formidable_generer_url_action_recuperer_fichier_email($nom_saisie, $valeur['nom'], $options);
		$saisie_a_modifier[$i]['url'] = $url;
		if (_FORMIDABLE_EXPIRATION_FICHIERS_EMAIL > 0) {
			$saisie_a_modifier[$i]['fichier'] = $valeur['nom'];
			$saisie_a_modifier[$i]['nom'] = '[' . _T('formidable:lien_expire', ['delai' => $delai]) . '] ' . $valeur['nom'];
		} else {
			$saisie_a_modifier[$i]['fichier'] = $valeur['nom'];
			$saisie_a_modifier[$i]['nom'] = $valeur['nom'];
		}
		if (isset($valeur['extension'])) {
			$saisie_a_modifier[$i]['vignette'] = $vignette_par_defaut($valeur['extension'], false);
		}
	}
	return $saisie_a_modifier;
}
/**
 * Supprime dans une vue de saisie 'fichiers'
 * l'url de récupération par email
 * et l'information sur le délai d'expiration
 * @param array $vue
 * @return array $vue
**/
function traiter_email_supprimer_action_recuperer_fichier_par_email($vue) {
	foreach ($vue as $f => &$desc) {
		if (isset($desc['url'])) {
			unset($desc['url']);
		}
		$desc['nom'] = $desc['fichier'];
	}
	return $vue;
}

/**
 * Dans l'ensemble de vues des saisies
 * recherche les saisies 'fichiers'
 * et supprime pour chacune d'entre elle les actions de récupération de fichier
 * @param array $saisies
 * @param array $vues
 * @return array $vues
**/
function traiter_email_vues_saisies_supprimer_action_recuperer_fichier_par_email($saisies, $vues) {
	foreach ($saisies as $saisie => $description) {
		if ($description['saisie'] == 'fichiers') { // si de type fichiers
			$nom_saisie = $description['options']['nom'];
			$vues[$nom_saisie] = traiter_email_supprimer_action_recuperer_fichier_par_email($vues[$nom_saisie]);
		}
		if (isset($description['saisies'])) {
			$vues = traiter_email_vues_saisies_supprimer_action_recuperer_fichier_par_email($description['saisies'], $vues);
		}
	}
	return $vues;
}


/**
 * Converti une description d'une vue fichiers en description passable à facteur
 * @param array $vue
 * @return array $tableau_facteur
**/
function traiter_email_vue_fichier_to_tableau_facteur($vue) {
	if (!$vue) {
		return [];
	}
	$tableau_facteur = [];
	foreach ($vue as $fichier) {
		$arg = base64_decode(parametre_url($fichier['url'], 'arg'));
		$arg = json_decode($arg, true);
		$tableau_facteur[] = [
			'chemin' => formidable_generer_chemin_fichier($arg),
			'nom' => $fichier['fichier'],
			'encodage' => 'base64',
			'mime' => $fichier['mime']];
	}
	return $tableau_facteur;
}

/**
 * Retourne des secondes sous une jolie forme, du type xx jours, yy heures, zz minutes, aa secondes
 * @param int $secondes
 * @return string
**/
function traiter_email_secondes_en_jour($secondes) {
	$jours = floor($secondes / (24 * 3600));
	$heures = floor(($secondes - $jours * 24 * 3600) / 3600);
	$minutes = floor(($secondes - $jours * 24 * 3600 - $heures * 3600) / 60);
	$secondes = $secondes - $jours * 24 * 3600 - $heures * 3600 - $minutes * 60;
	$param = [
		'j' => $jours,
		'h' => $heures,
		'm' => $minutes,
		's' => $secondes
	];
	if ($jours > 0) {
		return _T('formidable:jours_heures_minutes_secondes', $param);
	} elseif ($heures > 0) {
		return _T('formidable:heures_minutes_secondes', $param);
	} elseif ($minutes > 0) {
		return _T('formidable:minutes_secondes', $param);
	} else {
		return _T('formidable:secondes', $param);
	}
}

/**
 * Gestion des champs de type fichier
 * 1. Calculer la taille totale des fichiers
 * 2. Ajouter au facteur
 * 3. Modifier l'affichage du champ
 * @todo splitter en plusieurs morceaux, mais sans doute faudrait tout refaire en objet
 * @param array $saisies
 * @param array $valeurs les valeurs des champs
 * @param int $id_formulaire
 * @param int $timestamp
 * @param array $retours retours du traitement
 * @return array {champs: array, valeursfichiers_facteur: array, taille_fichiers: int, retours: array}
**/
function traiter_email_champs_fichiers(array $saisies, array $valeurs, int $id_formulaire, int $timestamp, array $retours): array {
	// Traitement à part pour les saisies de types fichiers :
	$saisies_fichiers = saisies_lister_avec_type($saisies, 'fichiers');
	$taille_fichiers = 0; //taille des fichiers en email
	$fichiers_facteur = []; // tableau qui stockera les fichiers à envoyer avec facteur
	if (!isset($retours['fichiers'])) {
		$retours['fichiers'] = [];
		$retours_fichier = [];
		$ajouter_fichier = true;
	} else {
		$ajouter_fichier = false;
	}
	foreach (array_keys($saisies_fichiers) as $champ) {
		if (array_key_exists($champ, $saisies_fichiers)) {
			$valeurs[$champ] = traiter_email_fichiers($saisies_fichiers[$champ], $champ, $id_formulaire, $retours, $timestamp);
			if ($ajouter_fichier) {
				$retours_fichiers[$champ] = $valeurs[$champ];
			}
			$taille_fichiers += formidable_calculer_taille_fichiers_saisie($valeurs[$champ] ?? []);
			$fichiers_facteur = array_merge(
				$fichiers_facteur,
				traiter_email_vue_fichier_to_tableau_facteur($valeurs[$champ])
			);
		}
	}
	return [
		'valeurs' => $valeurs,
		'fichiers_facteur' => $fichiers_facteur,
		'taille_fichiers' => $taille_fichiers,
		'retours' => $retours
	];
}

/**
 * Prend un nom d'option
 * Recherche sa valeur
 * Remplace les @@
 * Retourne le résultat
 * @param string $nom_option
 * @param array $options options du traitement `email`
 * @param array $saisies
 * @param array $retours le retour des traitements deja fait
 * @param string $moment ('' pour immédiatement, 'publie' pour lors de la publication de la réponse)
 * @return string
**/
function traiter_email_option2arobases(string $nom_option, array $options, array $saisies, array $retours, string $moment): string {
	$return = '';
	$options_arobase = [
		'sans_reponse' => '',
		'contexte' => "traiter_email_{$nom_option}",
		'brut' => $options["{$nom_option}_valeurs_brutes"] ?? '',
		'id_formulaires_reponse' =>  $retours['id_formulaires_reponse'] ?? '',
		'id_formulaire' => $retours['id_formulaire']
	];
	$valeur_option = $options[$nom_option] ?? '';

	if ($moment) {
		if (isset($options["{$nom_option}_{$moment}"])) {
			$valeur_option = $options["{$nom_option}_{$moment}"];
		}
		$options_arobase['source'] = 'base';
	}
	if ($valeur_option) {
		$return = formidable_raccourcis_arobases_2_valeurs_champs(
			$valeur_option,
			$saisies,
			$options_arobase
		);
	}
	return $return;
}

/**
 * Construire le nom de l'envoyeur
 * @param array $options du traitement
 * @param array $saisies
 * @param array $retours
 * @param string $moment ('' pour immédiatement, 'publie' pour lors de la publication de la réponse)
 * @return string
**/
function traiter_email_nom_envoyeur(array $options, array $saisies, array $retours, string $moment = ''): string {
	return traiter_email_option2arobases('champ_nom', $options, $saisies, $retours, $moment);
}

/**
 * Construire le sujet du mail
 * @param array $options du traitement
 * @param array $saisies
 * @param array $retours
 * @param string $nom_envoyeur
 * @param string $courriel_envoyeur
 * @param string $moment ('' pour immédiatement, 'publie' pour lors de la publication de la réponse)
**/
function traiter_email_sujet(array $options, array $saisies, array $retours, string $nom_envoyeur, ?string $courriel_envoyeur = '', string $moment = ''): string {
	$sujet = traiter_email_option2arobases('champ_sujet', $options, $saisies, $retours, $moment);
	if (!$sujet) {
		$sujet = _T('formidable:traiter_email_sujet', ['nom' => ($nom_envoyeur ?? $courriel_envoyeur)]);
	}

	if (($options['champ_sujet_modif_reponse']  ?? '') && ($retours['modification_reponse'] ?? '')) {
		$sujet = $sujet . ' ' . _T('formidable:traitement_email_sujet_courriel_modif_reponse');
	}
	$sujet = filtrer_entites($sujet);
	return $sujet;
}



/**
 * Construire le sujet de l'AR
 * @param array $options du traitement
 * @param array $saisies
 * @param array $retours
 * @param string $moment ('' pour immédiatement, 'publie' pour lors de la publication de la réponse)
 * @return string
**/
function traiter_email_sujet_accuse(array $options, array $saisies, array $retours, string $moment = ''): string {
	$sujet_accuse = traiter_email_option2arobases('sujet_accuse', $options, $saisies, $retours, $moment);
	if (!$sujet_accuse) {
		$sujet_accuse = _T('formidable:traiter_email_sujet_accuse');
	}
	$sujet_accuse = filtrer_entites($sujet_accuse);
	return $sujet_accuse;
}


/**
 * Construire le texte de l'AR
 * @param array $options du traitement
 * @param array $saisies
 * @param array $retours
 * @param string $moment ('' pour immédiatement, 'publie' pour lors de la publication de la réponse)
 * @return string
**/
function traiter_email_texte_accuse(array $options, array $saisies, array $retours, string $moment = ''): string {
	return traiter_email_option2arobases('texte_accuse', $options, $saisies, $retours, $moment);
}


/**
 * Est-ce qu'on joint les PJ
 * @param array $options les options du traitement
 * @param int $taille_fichiers la taille des fichiers
 * @return bool
**/
function traiter_email_joindre_pj(array $options, int $taille_fichiers): bool {
	$joindre_pj = false;
	if (
		$taille_fichiers < 1024 * 1024 * _FORMIDABLE_TAILLE_MAX_FICHIERS_EMAIL
		&& $options['pj'] === 'on'
	) {
		$joindre_pj = true;
	}
	return $joindre_pj;
}

/**
 * Envoyer le mail principal aux destinataires
 * @param array $args ce qui est passé à traiter_email_dist
 * @param array $retours les retours des traitements
 * @param array $saisies
 * @param array $valeurs les valeurs des champs
 * @param string $moment de l'envoi du mail
 *	- 'publie' si c'est lorsque la réponse est publiée
 *	- '' ou 'prop' si c'est lorsque la réponse est proposée / sans enregistrement de réponse
 * @return bool `false` si erreur d'envoi, true si ok
**/
function traiter_email_envoyer_destinataires(
	array $args,
	array $retours,
	array $saisies,
	array $valeurs,
	string $moment = ''
): bool {

	// Si pas de destinataires, on arrete
	$destinataires = traiter_email_destinataires($args, $retours, $valeurs);
	if (!$destinataires) {
		return true;
	}


	// Par retrocompatibilité, 'prop' est basculé en '', car les champs de réglages ne sont pas suffixé dans ce cas (vu qu'il n'y avait que ca au début).
	if ($moment === 'prop') {
		$moment = '';
	}

	$options = $args['options'];
	$formulaire = $args['formulaire'];
	$id_formulaire = $formulaire['id_formulaire'];
	$traitements = $args['traitements'];

	$saisies = saisies_supprimer_depublie_sans_reponse($saisies, $valeurs);

	$saisies_fond_notification = traiter_email_saisies_fond_notification($saisies, $options, $valeurs);
	$courriel_envoyeur = $valeurs[$options['champ_courriel'] ?? ''] ?? '';

	$info_champs_fichiers = traiter_email_champs_fichiers($saisies, $valeurs, $id_formulaire, $retours['timestamp'] ?? 0, $retours);
	$fichiers_facteur = $info_champs_fichiers['fichiers_facteur'];
	$taille_fichiers = $info_champs_fichiers['taille_fichiers'];
	$retours = $info_champs_fichiers['retours'];
	$valeurs = $info_champs_fichiers['valeurs'];

	$joindre_pj = traiter_email_joindre_pj($options, $taille_fichiers);


	$forcer_from = !boolval($options['activer_vrai_envoyeur'] ?? '');

	$nom_envoyeur = traiter_email_nom_envoyeur($options, $saisies, $retours, $moment);
	$sujet = traiter_email_sujet($options, $saisies, $retours, $nom_envoyeur, $courriel_envoyeur, $moment);
	// Mais quel va donc être le fond ?
	if (find_in_path('notifications/formulaire_' . $formulaire['identifiant'] . '_email.html')) {
		$notification = 'notifications/formulaire_' . $formulaire['identifiant'] . '_email';
	} else {
		$notification = 'notifications/formulaire_email';
	}

	// Est-ce qu'on est assez léger pour joindre les pj
	// On génère le mail avec le fond
	$html = recuperer_fond(
		$notification,
		[
			'id_formulaire' => $args['id_formulaire'],
			'id_formulaires_reponse' => isset($retours['id_formulaires_reponse']) ? $retours['id_formulaires_reponse'] : '',
			'titre' => _T_ou_typo($formulaire['titre']),
			'traitements' => $traitements,
			'saisies' => $saisies_fond_notification,
			'valeurs' => $valeurs,
			'masquer_liens' => $options['masquer_liens'],
			'ip' => $options['activer_ip'] ? $GLOBALS['ip'] : '',
			'envoi_precedent' => isset($retours['envoi_precedent']) ? $retours['envoi_precedent'] : '' ,
			'courriel_envoyeur' => $courriel_envoyeur,
			'nom_envoyeur' => filtrer_entites($nom_envoyeur)
		]
	);

	// On génère le texte brut
	$texte = facteur_mail_html2text($html);

	// On utilise la forme avancée de Facteur
	$corps = [
		'html' => $html,
		'texte' => $texte,
		'nom_envoyeur' => filtrer_entites($nom_envoyeur),
		'headers' => [
			'X-Originating-IP:' . $GLOBALS['ip']
		],
		'from' => $courriel_envoyeur
	];

	$corps = traiter_email_ajuster_expediteur($corps, $forcer_from);
	// Joindre les pj si léger
	if ($joindre_pj) {
		$corps['pieces_jointes'] = $fichiers_facteur;
	}

	// On envoie enfin le message
	$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	return $envoyer_mail($destinataires, $sujet, $corps);
}

/**
 * Envoyer l'accusé de reception
 * @param array $args ce qui est passé à traiter_email_dist
 * @param array $retours les retours des traitements
 * @param array $saisies
 * @param array $valeurs les valeurs des champs
 * @return bool `false` si erreur d'envoi, true si ok
 * @param string $moment ('' ou 'prop' pour une réponse proposée / pas d'enregistrement en base, 'publie' pour une réponse publié)
**/
function traiter_email_envoyer_accuse(
	array $args,
	array $retours,
	array $saisies,
	array $valeurs,
	string $moment = ''
): bool {
	$options = $args['options'];
	$courriel_envoyeur = $valeurs[$options['champ_courriel'] ?? ''] ?? '';
	// S'il n'y pas lieu d'envoyer l'accusé
	if (!$courriel_envoyeur || !($options['activer_accuse'] ?? '')) {
		return true;
	}

	// Par retrocompatibilité, 'prop' est basculé en '', car les champs de réglages ne sont pas suffixé dans ce cas (vu qu'il n'y avait que ca au début).
	if ($moment === 'prop') {
		$moment = '';
	}

	$saisies = saisies_supprimer_depublie_sans_reponse($saisies, $valeurs);
	$saisies_fond_notification = traiter_email_saisies_fond_notification($saisies, $options, $valeurs);

	$formulaire = $args['formulaire'];
	$id_formulaire = $formulaire['id_formulaire'];
	$traitements = $args['traitements'];

	$forcer_from = !boolval($options['activer_vrai_envoyeur'] ?? '');

	$sujet_accuse = traiter_email_sujet_accuse($options, $saisies, $retours, $moment);
	$message_retour = traiter_email_texte_accuse($options, $saisies, $retours, $moment);
	if (!$message_retour) {
		$message_retour = formidable_raccourcis_arobases_2_valeurs_champs(
			$args['formulaire']['message_retour'],
			$saisies,
			[
				'id_formulaire' => $retours['id_formulaire'],
				'id_formulaires_reponse' => $retours['id_formulaires_reponse'] ?? '',
				'source' => (($retours['id_formulaires_reponse'] ?? '') ? 'base' : 'request'),
				'contexte' => 'texte_accuse'
			]
		);
	}


	$info_champs_fichiers = traiter_email_champs_fichiers($saisies, $valeurs, $id_formulaire, $retours['timestamp'] ?? 0, $retours);
	$fichiers_facteur = $info_champs_fichiers['fichiers_facteur'];
	$taille_fichiers = $info_champs_fichiers['taille_fichiers'];
	$retours = $info_champs_fichiers['retours'];
	$valeurs = $info_champs_fichiers['valeurs'];

	$joindre_pj = traiter_email_joindre_pj($options, $taille_fichiers);

	// Mais quel va donc être le fond ?
	if (find_in_path('notifications/formulaire_' . $formulaire['identifiant'] . '_accuse.html')) {
		$accuse = 'notifications/formulaire_' . $formulaire['identifiant'] . '_accuse';
	} else {
		$accuse = 'notifications/formulaire_accuse';
	}

	// On génère l'accusé de réception
	if (_FORMIDABLE_LIENS_FICHIERS_ACCUSE_RECEPTION == false) {
		$valeurs = traiter_email_vues_saisies_supprimer_action_recuperer_fichier_par_email($saisies, $valeurs);
	}


	$parametres_accuse = [
		'id_formulaire' => $formulaire['id_formulaire'],
		'id_formulaires_reponse' => isset($retours['id_formulaires_reponse']) ? $retours['id_formulaires_reponse'] : '',
		'titre' => _T_ou_typo($formulaire['titre']),
		'message_retour' => $message_retour,
		'traitements' => $traitements,
		'saisies' => $options['masquer_valeurs_accuse'] ? [] : $saisies_fond_notification,
		'valeurs' => $options['masquer_valeurs_accuse'] ? [] : $valeurs
	];
	$parametres_accuse = pipeline(
		'formidable_parametres_accuse',
		[
			'args' => ['id_formulaire' => $parametres_accuse['id_formulaire'], 'id_formulaires_reponse' => $parametres_accuse['id_formulaires_reponse']],
			'data' => $parametres_accuse
		]
	);

	$html_accuse = recuperer_fond(
		$accuse,
		$parametres_accuse
	);

	// On génère le texte brut
	$texte = facteur_mail_html2text($html_accuse);

	// Si un nom d'expéditeur est précisé pour l'AR, on l'utilise,
	// sinon on utilise le nomde l'envoyeur du courriel principal
	$nom_envoyeur_accuse = formidable_trim($options['nom_envoyeur_accuse'] ?? '');
	//A fortiori, si un courriel d'expéditeur est précisé pour l'AR, on l'utilise
	$courriel_envoyeur_accuse = $options['courriel_envoyeur_accuse'] ?? '';

	$corps = [
		'html' => $html_accuse,
		'texte' => $texte,
		'nom_envoyeur' => filtrer_entites($nom_envoyeur_accuse),
		'headers' => [
			'X-Originating-IP:' . $GLOBALS['ip']
		],
		'from' => $courriel_envoyeur_accuse
	];

	$corps = traiter_email_ajuster_expediteur($corps, $forcer_from);

	// Joindre les pj si léger et nécessaire
	if ($joindre_pj && _FORMIDABLE_LIENS_FICHIERS_ACCUSE_RECEPTION == false) {
		$corps['pieces_jointes'] = $fichiers_facteur;
	}

	$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
	return $envoyer_mail($courriel_envoyeur, $sujet_accuse, $corps);
}
