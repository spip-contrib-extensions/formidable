<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}
include_spip('inc/formidable_fichiers');
include_spip('action/editer_objet');
include_spip('inc/autoriser');
include_spip('inc/modifier');
function traiter_enregistrement_dist($args, $retours) {
	include_spip('inc/formidable');
	include_spip('base/abstract_sql');
	include_spip('inc/formidable');
	$retours['fichiers'] = []; // on va stocker des infos sur les fichiers, pour les prochains traitement
	$options = $args['options'];
	$formulaire = $args['formulaire'];
	$id_formulaire = $args['id_formulaire'];
	$saisies = saisies_lister_par_nom($args['saisies']);
	$variable_php = '';
	// La personne a-t-elle un compte ?
	$id_auteur = isset($GLOBALS['visiteur_session']) ? (isset($GLOBALS['visiteur_session']['id_auteur']) ?
		$GLOBALS['visiteur_session']['id_auteur'] : 0) : 0;

	// traitement de l'anonymisation de l'auteur lorsque la méthode d'identification se fait par l'identifiant
	if ($options['anonymiser'] === 'on' && $id_auteur) {
		if ($options['identification'] === 'id_auteur') {
			$variable_php = formidable_hasher_id_auteur($id_auteur);
		}
		$id_auteur = 0;
	}

	// On cherche le cookie et sinon on le crée
	$nom_cookie = formidable_generer_nom_cookie($id_formulaire);
	if (isset($_COOKIE[$nom_cookie])) {
		$cookie = $_COOKIE[$nom_cookie];
	} else {
		include_spip('inc/acces');
		$cookie = creer_uniqid();
	}

	// On crée un identifiant depuis l'éventuelle variable php d'identification
	if ($options['identification'] == 'variable_php') {
		$variable_php = formidable_variable_php_identification($options['variable_php'], $id_formulaire);
	}

	// On regarde si c'est une modif d'une réponse existante
	$id_formulaires_reponse = $args['id_formulaires_reponse'];
	if (!$args['forcer_modif']) {//si on dit lors de l'appel du formulaire qu'on doit modifier dans tous les cas, inutile de chercher si on peut modifier
		$id_formulaires_reponse = formidable_trouver_reponse_a_editer($id_formulaire, $id_formulaires_reponse, $options, true);
	}

	// Est-ce qu'il faut rebasculer la réponse en proposée
	if (
		($options['modif_instituer_prop'] ?? '')
		&& !($args['options_appel']['traiter_enregistrement_desactiver_modif_instituer_prop'] ?? '')
		&& !traiter_enregistrement_publier_automatiquement($id_formulaire, $options)
	) {
		autoriser_exception('instituer', 'formulairesreponse', $id_formulaires_reponse);
		objet_instituer('formulaires_reponse', $id_formulaires_reponse, ['statut' => 'prop']);
		autoriser_exception('instituer', 'formulairesreponse', $id_formulaires_reponse, false);
	}


	// Si ce n'est pas une modif d'une réponse existante, on crée d'abord la réponse
	if (!$id_formulaires_reponse) {
		spip_log("Création d'une nouvelle réponse pour le formulaire $id_formulaire", 'formidable' . _LOG_INFO);
		autoriser_exception('inserer', 'formulairesreponse');
		$id_formulaires_reponse = objet_inserer(
			'formulairesreponse',
			'',
			[
				'id_formulaire' => $id_formulaire,
				'id_auteur' => $id_auteur,
				'cookie' => $cookie,
				'variable_php' => $variable_php,
				'ip' => $args['options']['ip'] == 'on' ? $GLOBALS['ip'] : '',
				'date' => date('Y-m-d H:i:s'),
				'date_envoi' => date('Y-m-d H:i:s'),
				'statut' => 'prop'
			]
		);
		autoriser_exception('creer', 'formulairesreponse', '', false);
		spip_log("La réponse créée est $id_formulaires_reponse", 'formidable' . _LOG_INFO);


		// Si on a pas le droit de répondre plusieurs fois ou que les réponses seront modifiables,
		// il faut poser un cookie
		if (!$options['multiple'] || $options['modifiable']) {
			include_spip('inc/cookie');
			// Expiration dans 30 jours
			spip_setcookie($nom_cookie, $_COOKIE[$nom_cookie] = $cookie, time() + 30 * 24 * 3600);
		}
		$retours['modification_reponse'] = false;// signaler aux traitements qu'il s'agit d'une nouvelle réponse
	} else { // si c'est une modif de réponse existante
		spip_log("Modification de la réponse $id_formulaires_reponse", 'formidable' . _LOG_INFO);
		// simple mise à jour du champ maj de la table spip_formulaires_reponses
		$envoi_precedent = sql_getfetsel('date_envoi', 'spip_formulaires_reponses', "id_formulaires_reponse = $id_formulaires_reponse");
		autoriser_exception('modifier', 'formulairesreponse', $id_formulaires_reponse);
		objet_modifier_champs(
			'formulairesreponse',
			$id_formulaires_reponse,
			[],
			[
				'maj' => date('Y-m-d H:i:s'),
				'date_envoi' => date('Y-m-d H:i:s'),
			]
		);
		autoriser_exception('modifier', 'formulairesreponse', $id_formulaires_reponse, false);
		//effacer les fichiers existant
		formidable_effacer_fichiers_reponse($id_formulaire, $id_formulaires_reponse);
		$retours['envoi_precedent'] = $envoi_precedent;
		$retours['modification_reponse'] = true;// signaler aux traitements qui viendraient après qu'il s'agit d'une modif
	}
	// Si l'id n'a pas été créé correctement alors erreur
	if (!($id_formulaires_reponse > 0)) {
		spip_log('La réponse n\'a pas été enregistrée', 'formidable' . _LOG_ERREUR);
		$retours['message_erreur'] .= "\n<br/>" . _T('formidable:traiter_enregistrement_erreur_base');
	} else {
		// Sinon on continue à mettre à jour
		$insertions = [];
		foreach ($saisies as $nom => $saisie) {
			if (saisies_saisie_est_fichier($saisie)) { // traiter à part le cas des saisies fichiers
				$valeur = traiter_enregistrement_fichiers($saisie, $id_formulaire, $id_formulaires_reponse);
				if (($valeur !== null)) {
					$insertions[] = [
						'id_formulaires_reponse' => $id_formulaires_reponse,
						'nom' => $nom,
						'valeur' => is_array($valeur) ? formidable_serialize($valeur) : $valeur
					];
					$retours['fichiers'][$nom] = $valeur;
				}
			} elseif (($valeur = _request($nom)) !== null || saisies_saisie_est_tabulaire($saisie)) {
				// Pour le saisies différentes de fichiers,
				// on ne prend que les champs qui ont effectivement été envoyés par le formulaire
				$insertions[] = [
					'id_formulaires_reponse' => $id_formulaires_reponse,
					'nom' => $nom,
					'valeur' => is_array($valeur) ? formidable_serialize($valeur) : $valeur
				];
			}
		}
		// On supprime d'abord TOUT les champs, y compris ceux qui ne viennent pas d'être envoyé.
		// En effet, ils pouvaient y avoir des champs remplis lors du précédent enregistrement
		// Qui ne le sont plus au nouvel enregistrement, car la condition d'affichage (afficher_si) n'est plus remplie
		// Dans ce cas il ne faut pas qu'ils continuent à être stockés en base, car cela peut fausser les affichages divers (type tableaux et autres)
		spip_log("Suppression des potentielles anciennes valeurs pour $id_formulaires_reponse", 'formidable' . _LOG_INFO);
		sql_delete(
			'spip_formulaires_reponses_champs',
			[
				'id_formulaires_reponse = ' . $id_formulaires_reponse
			]
		);

		spip_log("Insertion des nouvelles valeurs pour $id_formulaires_reponse", 'formidable' . _LOG_INFO);
		// Puis on insère les nouvelles valeurs
		if ($insertions) {
			sql_insertq_multi(
				'spip_formulaires_reponses_champs',
				$insertions
			);
		}
		if (!$args['message_retour_general']) {// Si jamais message de retour general, ne pas mettre le message de retour spécifique. Cf https://git.spip.net/spip-contrib-extensions/formidable/issues/58
			$retours['message_ok'] = _T('formidable:traiter_enregistrement_message_ok');
		}
		$retours['id_formulaires_reponse'] = $id_formulaires_reponse;
	}


	// En tout dernier on passe par l'institution de la réponse si elle est nouvelle.
	// Et si besoin.
	if (
		!$args['id_formulaires_reponse']
		&& traiter_enregistrement_publier_automatiquement($id_formulaire, $options)
	) {
		autoriser_exception('instituer', 'formulairesreponse', $id_formulaires_reponse);
		objet_instituer('formulaires_reponse', $id_formulaires_reponse, ['statut' => 'publie']);
		autoriser_exception('instituer', 'formulairesreponse', $id_formulaires_reponse, false);
	}
	// noter qu'on a deja fait le boulot, pour ne pas risquer double appel
	$retours['traitements']['enregistrement'] = true;
	return $retours;
}


function traiter_enregistrement_verifier_dist($args, $erreurs) {
	$id_formulaire = $args['id_formulaire'];
	$options = $args['options'];
	$id_formulaires_reponse = $args['id_formulaires_reponse'];
	$etape = $args['etape'];
	if (
		($unicite = $options['unicite'] ?? '') && !($erreurs[$unicite] ?? '')
		&&
		(
			$etape === null
			|| (
			 array_key_exists($unicite, saisies_lister_par_nom($args['etapes']['etape_' . $etape]['saisies']))
			)
		)
	) {
		if (!$id_formulaires_reponse) { // si pas de réponse explictement passée au formulaire, on cherche la réponse qui serait édité
			$id_formulaires_reponse = formidable_trouver_reponse_a_editer($id_formulaire, $id_formulaires_reponse, $options);
		}
		
		if (!formidable_verifier_unicite($id_formulaire, $id_formulaires_reponse, _request($unicite))) {
			$erreurs[$unicite] = $options['message_erreur_unicite'] ?
				_T($options['message_erreur_unicite']) : _T('formidable:erreur_unicite');
		}
	}

	return $erreurs;
}

/**
 * Pour une saisie 'fichiers' particulière,
 * déplace chaque fichier envoyé dans le dossier
 * config/fichiers/formidable/formulaire_$id_formulaire/reponse_$id_formulaires_reponse.
 * @param array $saisie la description de la saisie
 * @param int $id_formulaire le formulaire
 * @param int $id_formulaires_reponse
 * @return array|null un tableau organisé par fichier, contenant 'nom', 'extension','mime','taille'
**/
function traiter_enregistrement_fichiers($saisie, $id_formulaire, $id_formulaires_reponse) {
	return formidable_deplacer_fichiers_produire_vue_saisie($saisie, ['id_formulaire' => $id_formulaire, 'id_formulaires_reponse' => $id_formulaires_reponse]);
}

/**
 * Indique pour un formulaire donnée si la réponse doit être publiée automatiquement ou pas
 * @param int $id_formulaire
 * @param array $options les options du traitement
 * @return bool
**/
function traiter_enregistrement_publier_automatiquement(int $id_formulaire, array $options): bool {
	// Modération a posteriori : on publie automatiquement
	if ($options['moderation'] === 'posteriori') {
		return true;
	}

	// Est-ce qu'on est autorisé à instituer ?
	if (autoriser('instituer', 'formulairesreponse', '', null, ['id_formulaire' => $id_formulaire, 'nouveau_statut' => 'publie'])) {
		// Et qu'on n'a pas dit qu'on devait aussi modérer les admins
		if (!($options['moderer_admins'] ?? '')) {
			return true;
		}
	}

	// Par défaut on ne publie pas
	return false;
}
