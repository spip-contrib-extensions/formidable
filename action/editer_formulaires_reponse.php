<?php

include_spip('base/abstract_sql');
include_spip('inc/autoriser');
include_spip('formidable_fonctions');

/**
 * Institue (modifie le statut) d'une réponse en invalidant ou pas le cache
 * (contrairement à la fonction de base de SPIP qui invalide systématiquement le cache)
 * @param int $id_formulaires_reponse
 * @param array $champs comme pour objet_instituer
 * @return string
**/
function formulaires_reponse_instituer(int $id_formulaires_reponse, array $champs): string {

	$statut = $champs['statut'] ?? '';

	// La fonction d'institution est appelée indirectement par SPIP via `objet_modifier()` même pas de modification de statut
	if (!$statut) {
		return '';
	}
	$id_formulaire = sql_getfetsel('id_formulaire', 'spip_formulaires_reponses', "id_formulaires_reponse = $id_formulaires_reponse");

	// Est-ce qu'on peut instituer ?
	if (!autoriser('instituer', 'formulairesreponse', $id_formulaires_reponse, null, ['id_formulaire' => $id_formulaire, 'nouveau_statut' => $statut])) {
		return 'pas_content';
	}

	$table_sql = 'spip_formulaires_reponses';
	$table_objet = 'formulaires_reponses';
	$trouver_table = charger_fonction('trouver_table', 'base');
	$desc = $trouver_table($table_sql);

	$liste_statuts_publies = explode(',', $desc['statut'][0]['publie']);// Le 0 est une approximation qui devrait suffire dans 99% des cas
	$sel = ['statut', 'date'];
	$row = sql_fetsel($sel, $table_sql, "id_formulaires_reponse=$id_formulaires_reponse");



	$statut_ancien = $row['statut'];
	$date_ancienne = $row['date'];

	// Si on publie, modifier la date le cas échéant
	if (in_array($statut, $liste_statuts_publies) && !in_array($statut_ancien, $liste_statuts_publies)) {
		$date = date('Y-m-d H:i:s');
	} else {
		$date = $date_ancienne;
	}

	$champs['date'] = $date;
	// Pour les flux de pipeline, ce qu'on trouve de base dans SPIP
	$flux = [
		'args' => [
			'action' => 'instituer',
			'table' => $table_sql,
			'table_objet' => $table_objet,
			'spip_table_objet' => $table_sql,
			'objet' => 'formulaires_reponse',
			'id_objet' => $id_formulaires_reponse,
			'statut_ancien' => $statut_ancien,
			'date_ancienne' => $date_ancienne,
		],
		'data' => &$champs
	];

	// Puis toutes les infos utiles sur le formulaire parent
	$flux['args']['id_formulaire'] = $id_formulaire;
	$flux['args']['formulaire'] = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $flux['args']['id_formulaire']);
	$flux['args']['saisies'] = formidable_deserialize($flux['args']['formulaire']['saisies']);
	$flux['args']['traitements'] = formidable_deserialize($flux['args']['formulaire']['traitements']);

	// Les choses habituelles : pipeline pre et post edition
	$champs = pipeline('pre_edition', $flux);
	sql_updateq($table_sql, $champs, "id_formulaires_reponse=$id_formulaires_reponse");
	$champs = pipeline('post_edition', $flux);



	// Notifications
	if ($notifications = charger_fonction('notifications', 'inc')) {
		$notifications(
			'formulaires_reponse_instituer',
			$id_formulaires_reponse,
			[
				'statut' => $statut,
				'statut_ancien' => $statut_ancien,
				'date' => $date,
				'date_ancienne' => $date_ancienne,
				'champs' => $champs,
			]
		);
		$notifications(
			'objet_instituer',
			$id_formulaires_reponse,
			[
				'objet' => 'formulaires_reponse',
				'id_objet' => $id_formulaires_reponse,
				'statut' => $statut,
				'statut_ancien' => $statut_ancien,
				'date' => $date,
				'date_ancienne' => $date_ancienne,
				'champs' => $champs,
			]
		);

		// Rétro-compat
		$notifications(
			'instituerformulaires_reponse',
			$id_formulaires_reponse,
			['statut' => $statut, 'statut_ancien' => $statut_ancien, 'date' => $date, 'date_ancienne' => $date_ancienne]
		);
	}

	// Puis le cas échéant on invalide le cache
	if (
		($flux['args']['traitements']['enregistrement']['invalider'] ?? '')
		&& (
			in_array($statut, $liste_statuts_publies)
			|| in_array($statut_ancien, $liste_statuts_publies)
		)
	) {
		include_spip('inc/invalideur');
		suivre_invalideur("formulaires_reponse/$id_formulaires_reponse");
	}
	return '';
}
