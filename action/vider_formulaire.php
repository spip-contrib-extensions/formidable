<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/*
 * Action de suppression des réponses à un formulaire
 * @param int $arg
 * @return unknown_type
 */
function action_vider_formulaire_dist($arg = null) {
	if (is_null($arg)) {
		$securiser_action = charger_fonction('securiser_action', 'inc');
		$arg = $securiser_action();
	}

	include_spip('inc/autoriser');
	// si id_formulaire n'est pas un nombre, on ne fait rien
	if (
		($id_formulaire = intval($arg))
		&& autoriser('instituer', 'formulairesreponse', null, null, array('id_formulaire' => $id_formulaire))
	) {
		// On les réponses en formulaire
		include_spip('action/editer_objet');
		$rows = sql_select('id_formulaires_reponse', 'spip_formulaires_reponses',
			['id_formulaire=' . $id_formulaire,
			 'statut!=\'poubelle\''
			]
		);
		while ($row = sql_fetch($rows)) {
			objet_modifier('formulaires_reponse', $row['id_formulaires_reponse'], ['statut' => 'poubelle']);
		}
		// on redirige vers les réponses
		$GLOBALS['redirect'] = parametre_url(generer_url_ecrire('formulaires_reponses'), 'id_formulaire', $id_formulaire);
	}
}
