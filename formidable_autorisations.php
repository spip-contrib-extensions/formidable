<?php

/**
 * Déclaration des autorisations
 *
 * @package SPIP\Formidable\Autorisations
**/

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('action/editer_liens');
include_spip('inc/config');

/**
 * Autorisation par auteur et par formulaire
 *
 * Seuls les auteurs associés à un formulaire peuvent y accéder
 *
 * @param  int   $id_formulaire id du formulaire à tester
 * @param  int   $id_auteur id de l'auteur à tester, si ==0 => auteur courant
 * @return bool  true s'il a le droit, false sinon
 *
*/
function formidable_autoriser_par_auteur($id_formulaire, $id_auteur = 0) {
	if ($id_formulaire == 0) {
		return true;
	}

	$retour = false;

	if ($id_auteur == 0) {
		include_spip('inc/session');
		$id_auteur = session_get('id_auteur');
	}

	// Si on a bien un id_auteur
	if (($id_auteur = intval($id_auteur)) > 0) {
		// On cherche si cet auteur est lié à ce formulaire
		$autorisations = objet_trouver_liens(['formulaire' => $id_formulaire], ['auteur' => $id_auteur]);
		$retour = count($autorisations) > 0;
	}

	return $retour;
}

/**
 * Réponses à un formulaire éditable par un auteur
 *
 * Est-on en présence d'un auteur qui tente de modifier les réponses d'un formulaire
 * et que Formidable est configuré pour prendre en compte les auteurs
 * et que les auteurs sont en droit de modifier les réponses de leurs formulaires ?
 *
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @return bool  true s'il a le droit, false sinon
 *
*/
function formidable_auteur_admin_reponse($qui) {
	$retour = false;
	$statut = $qui['statut'] ?? '';
	$restreint = $qui['restreint'] ?? '';

	if (
		// Si c'est un admin complet
		($statut === '0minirezo' && !$restreint)
		// Ou un admin restreint et qu'on a autorisé les restreints entiers à être admins des formulaires
		|| ($statut === '0minirezo' && $restreint && lire_config('formidable/autoriser_admin_restreint'))
		// Ou qu'on a autorisé les auteurs à être liés et donc admins de formulaires ET de leurs réponses
		|| (lire_config('formidable/auteur') && lire_config('formidable/admin_reponses_auteur'))
	) {
		$retour = true;
	}

	return $retour;
}

/**
 * Fonction d'appel pour le pipeline
 * @pipeline autoriser
 */
function formidable_autoriser() {
}

/**
 * Autorisation d'éditer un formulaire formidable
 *
 * Seuls les admins peuvent éditer les formulaires ou les auteurs explicitement associés à un formulaire
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulaire_editer_dist($faire, $type, $id, $qui, $opt) {
	$ok = false;
	$statut = $qui['statut'] ?? '';
	$restreint = $qui['restreint'] ?? '';
	if (
		// Si on est admin complet
		($statut === '0minirezo' && !$restreint)
		// Ou admin restreint avec l'option droit pour admin restreint
		|| ($statut === '0minirezo' && $statut && lire_config('formidable/autoriser_admin_restreint'))
		// Ou si ya les auteurs liés et qu'on est bien lié à ce formulaire
		|| (lire_config('formidable/auteur') && formidable_autoriser_par_auteur($id, $qui['id_auteur']))
	) {
		$ok = true;
	}

	return $ok;
}

/**
 * Autorisation de voir la liste des formulaires formidable
 *
 *  Admins et rédacs peuvent voir les formulaires existants
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulaires_menu_dist($faire, $type, $id, $qui, $opt) {
	if (in_array($qui['statut'] ?? '', ['1comite', '0minirezo'])) {
		return true;
	} else {
		return false;
	}
}


/**
 * Autorisation de répondre à un formulaire formidable
 *
 * On peut répondre à un formulaire si :
 * - c'est un formulaire classique
 * - on enregistre et que multiple = oui
 * - on enregistre et que multiple = non et que la personne n'a pas répondu encore
 * - on enregistre et que multiple = non et que modifiable = oui
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulaire_repondre_dist($faire, $type, $id, $qui, $opt) {
	$id = intval($id);

	// On regarde si il y a déjà le formulaire dans les options
	if (isset($opt['formulaire'])) {
		$formulaire = $opt['formulaire'];
	} else {
		// Sinon on va le chercher
		$formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $id);
	}

	include_spip('formidable_fonctions');
	$traitements = formidable_deserialize($formulaire['traitements']);

	// S'il n'y a pas d'enregistrement, c'est forcément bon
	$options = $traitements['enregistrement'] ?? '';
	if (!$options) {
		return true;
	} else {
		// Sinon faut voir les options
		// Si multiple = oui c'est bon
		if ($options['multiple']) {
			return true;
		} else {
			// Si c'est modifiable, c'est bon
			if ($options['modifiable']) {
				return true;
			} else {
				include_spip('inc/formidable');
				// Si la personne n'a jamais répondu, c'est bon
				if (!formidable_verifier_reponse_formulaire($id, $traitements['enregistrement']['identification'], $traitements['enregistrement']['variable_php'], $traitements['enregistrement']['anonymiser'])) {
					return true;
				} else {
					return false;
				}
			}
		}
	}
}

/**
 * Autorisation d'associer un nouvel auteur à un formulaire
 *
 * mêmes autorisations que pour éditer le formulaire
 *
**/
function autoriser_formulaire_associerauteurs_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('editer', $type, $id, $qui, $opt);
}

/**
 * Autorisation de modifier un formulaire
 *
 * mêmes autorisations que pour éditer le formulaire
 *
**/
function autoriser_formulaire_modifier_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('editer', $type, $id, $qui, $opt);
}


/**
 * Autorisation de voir les réponses d'un formulaire formidable
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_formulaire_voirreponses_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('editer', $type, $id, $qui, $opt);
}

/**
 * Autorisation d'instituer une réponse
 *
 * On peut modérer une réponse si on a le droit d'édition sur le formulaire parent,
 * et si on n'est pas admin, s'il y a l'option pour que les auteurs liés aient le droit de modérer les réponses aussi
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet (formulaires_reponse)
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulairesreponse_instituer_dist($faire, $type, $id, $qui, $opt) {
	$ok = false;

	// Quel formulaire parent
	if (isset($opt['id_formulaire'])) {
		$id_formulaire = intval($opt['id_formulaire']);
	}
	else {
		$id_formulaire = sql_getfetsel('id_formulaire', 'spip_formulaires_reponses', 'id_formulaires_reponse = ' . intval($id));
	}

	include_spip('inc/formidable');
	if(!formidable_verifier_unicite((int)$id_formulaire, (int)$id)){
		return false;
	}
		
	if (
		// Si on a bien trouvé le formulaire parent
		$id_formulaire
		// Si on peut éditer le formulaire parent
		&& autoriser('editer', 'formulaire', $id_formulaire, $qui, $opt)
		&& (
			// Et soit on est admin
			(($qui['statut'] ?? '') === '0minirezo')
			||
			// Soit c'est autorisé pour les auteurs liés (et si on est là c'est forcément qu'on est auteur lié puisqu'autorisé à éditer)
			lire_config('formidable/admin_reponses_auteur')
		)
	) {
		$ok = true;
	}

	return $ok;
}

/**
 * Autorisation de voir les réponses d'un formulaire formidable
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet (formulaires_reponse)
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulairesreponse_voir_dist($faire, $type, $id, $qui, $opt) {
	$id_formulaire = sql_getfetsel('id_formulaire', 'spip_formulaires_reponses', 'id_formulaires_reponse = '.(int)$id);
	return autoriser('editer', 'formulaire', $id_formulaire, $qui, $opt);
}

/**
 * Autorisation de modifier une réponse d'un formulaire formidable
 *
 * Comme le droit d'instituer
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet (formulaires_reponse)
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulairesreponse_modifier_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('instituer', $type, $id, $qui, $opt);
}

/**
 * Autorisation de supprimer une réponse d'un formulaire formidable
 *
 * Il faut pouvoir modifier les réponses d'un formulaire pour pouvoir les en supprimer
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet (formulaires_reponse)
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulairesreponse_supprimer_dist($faire, $type, $id, $qui, $opt) {
	$retour = autoriser('modifier', $type, $id, $qui, $opt);

	return $retour;
}

/**
 * Autorisation de poster une réponse en Collection+JSON
 *
 * Tout le monde peut, l'autorisation réelle se fera après pendant le traitement, suivant l'id_formulaire envoyé
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet (formulaires_reponse)
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
**/
function autoriser_formulairesreponse_post_collection_dist($faire, $type, $id, $qui, $opt) {
	return true;
}

/**
 * Autorisation d'accéder à un fichier depuis un lien dans un email
 *
 * Tout le monde peut par défaut
 *
 * @param  string $faire Action demandée
 * @param  string $type  Type d'objet sur lequel appliquer l'action
 * @param  int    $id    Identifiant de l'objet
 * @param  array  $qui   Description de l'auteur demandant l'autorisation
 * @param  array  $opt   Options de cette autorisation
 * @return bool          true s'il a le droit, false sinon
 **/
function autoriser_formulairesreponse_recupererfichieremail_dist($faire, $type, $id, $qui, $opt) {
	return true;
}
