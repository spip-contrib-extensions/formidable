<?php

include_spip('formidable_fonctions');
include_spip('base/abstract_sql');
include_spip('inc/saisies');

/**
 * Vérifie que les arobases passés sont cohérents avec les saisies d'un formulaire
 * @param array|string $valeur
 * @param array $options tableau `{'id_formulaire' : int}`
 * @return string chaine d'erreur éventuelle
**/
function verifier_formidable_coherence_arobase($valeur, $options) {
	static $cache;
	$id_formulaire = $options['id_formulaire'];
	if (!isset($cache[$id_formulaire])) {
		$saisies = sql_getfetsel('saisies', 'spip_formulaires', "id_formulaire = $id_formulaire");
		$saisies = formidable_deserialize($saisies);
		$saisies = pipeline('saisies_afficher_si_saisies', $saisies);
		$cache[$id_formulaire] = saisies_lister_par_nom($saisies);
	}
	$erreurs = verifier_formidable_coherence_arobase_selon_saisies($valeur, $cache[$id_formulaire]);
	$erreurs = pipeline('verifier_formidable_coherence_arobase', [
		'data' => $erreurs,
		'args' => ['id_formulaire' => $id_formulaire, 'saisies' => $cache[$id_formulaire]]
	]);

	$message_erreur = '';
	// Formater les erreurs
	if ($erreurs) {
		$erreurs = array_map(function ($i) {
			return "<code>@$i@</code>";
		}, $erreurs);
		$erreurs = implode(' ; ', $erreurs);
		return _T('formidable:erreur_arobase', ['erreurs' => $erreurs]);
	}
	return $message_erreur;
}

/**
 * Fonction qui en pratique vérifie la cohérence des arobases
 * En pratique c'est une fonction "pure" car elle ne nécessite pas d'accès SQL ni de connaissance des chaines de langue.
 * Elle est donc plus facilement testable
 * @param array|string $valeur
 * @param array $saisies triées par nom
 * @return array liste @@ qui pose problème (sans le @@ autour)
 **/
function verifier_formidable_coherence_arobase_selon_saisies($valeur, array $saisies): array {
	if (is_array($valeur)) {// A priori ne s'applique sur sur les chaines, pas sur les champs tabulaires
		return [];
	}
	preg_match_all('/@(([[:alnum:]]|_)*)@/', $valeur, $arobases, PREG_PATTERN_ORDER);
	$arobases = $arobases[1];
	$admissibles = array_merge(array_keys($saisies), ['nom_site_spip', 'id_formulaires_reponse', 'message_retour']);
	$erreurs = array_diff($arobases, $admissibles);

	return $erreurs;
}
