<?php

/**
 * Fallback pour l'analyse
 * @param array $valeurs
 * @return array
 *		- Si les valeurs individuelles sont des chaines, envoie les infos : vide, plein, taille moyenne
 *		- Si les valeurs individuelles sont des tableaux de clé : répartition des clés
 *		- Un clé `type` indique si string ou array, pour l'affichage final
 * Le mieux étant que le plugin qui fournit une saisie crée sa propre analyse
**/
function formidable_fallback_analyse(array $valeurs): array {
	$retours = ['type' => ''];
	if (!$valeurs) {// Si personne n'a répondu
		$retours = [
			'type' => 'string',
			'moyenne' => 0,
			'plein' => 0,
			'vide' => 0
		];
		return $retours;
	}


	// On tente de deviner si on a un tableau de chaine ou un tableau de tableau
	if (is_array($valeurs[0])) {
		$retours['type'] = 'array';
		if (count($valeurs) != count(array_filter($valeurs, 'is_array'))) {
			$retours['type'] = 'mixed';
		}
	} elseif (is_string($valeurs[0])) {
		$retours['type'] = 'string';
		if (count($valeurs) != count(array_filter($valeurs, 'is_string'))) {
			$retours['type'] = 'mixed';
		}
	} else {
		return $retours;
	}

	// Cas le plus simple : des chaines
	if ($retours['type'] === 'string') {
		$plein = array_filter($valeurs);
		$vide = array_diff_key($valeurs, $plein);
		if (count($plein) > 0) {
			$moyenne = array_sum(array_map('str_word_count', $plein)) / count($plein);
		} else {
			$moyenne = 0;
		}
		$retours['plein'] = count($plein);
		$retours['vide'] = count($vide);
		$retours['moyenne'] = $moyenne;
	}


	// Cas le plus complexe : des array, dans ce cas on fait comme si c'était une saisie checkbox
	if ($retours['type'] === 'array') {
		$data = array_reduce(
			$valeurs,
			function ($carry, $item) {
				return array_merge($carry, $item);
			},
			[]
		);
		foreach ($data as $d) {
			$retours['data'][$d] = $d;
		}
	}

	return $retours;
}
