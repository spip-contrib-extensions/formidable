<?php

/**
 * Chargement des fonctions pour les squelettes
 *
 * @package SPIP\Formidable\Fonctions
 **/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/formidable');
include_spip('public/formidable_criteres');

/**
 * #VOIR_REPONSE{checkbox_2} dans une boucle (FORMULAIRES_REPONSES)
 *
 * @param Pile $p
 * @return Pile
 */
function balise_VOIR_REPONSE_dist($p) {
	$nom = interprete_argument_balise(1, $p);
	if (!$type_retour = interprete_argument_balise(2, $p)) {
		$type_retour = 'null';
	}
	if (!$sans_reponse = interprete_argument_balise(3, $p)) {
		$sans_reponse = 'null';
	}
	$id_formulaires_reponse = champ_sql('id_formulaires_reponse', $p);
	$id_formulaire = champ_sql('id_formulaire', $p);
	$boucle = $p->boucles;
	$boucle = current($boucle);
	$sql_serveur = $boucle->sql_serveur;
	$sql_serveur = "'$sql_serveur'";
	$p->code = "calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $nom, $sql_serveur, $type_retour, $sans_reponse)";
	return $p;
}

/**
 * @param int $id_formulaires_reponse
 * @param int $id_formulaire
 * @param string $nom
 * @param string $sql_serveur
 * @param string $type_retour
 *   'brut' : valeur brute
 *   'valeur_uniquement' : la valeur seulement
 *   'label' : le label associé à la saisie
 *   'edit' : pour les crayons
 *   defaut : tout le HTML de la saisie
 * @param null|string $sans_reponse
 *   texte affiche si aucune valeur en base pour ce champ
 * @return array|string
 */
function calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $nom, $sql_serveur = '', $type_retour = null, $sans_reponse = null) {
	static $formulaires_saisies = [];
	static $reponses_valeurs = [];
	include_spip('formidable_fonctions');

	// Si pas déjà présent, on cherche les saisies de ce formulaire
	if (!isset($formulaires_saisies[$id_formulaire])) {
		$formulaires_saisies[$id_formulaire] = formidable_deserialize(
			sql_getfetsel(
				'saisies', //select
				'spip_formulaires', //from
				'id_formulaire = ' . intval($id_formulaire), //where
				'', //groupby
				'', //orderby
				'', //limit
				'', //having
				$sql_serveur
			)
		);
	}
	// Si pas déjà présent, on cherche les valeurs de cette réponse
	if (!isset($reponses_valeurs[$id_formulaires_reponse])) {
		if (
			$champs = sql_allfetsel(
				'nom,valeur,id_formulaires_reponses_champ', //select
				'spip_formulaires_reponses_champs', //from
				'id_formulaires_reponse = ' . intval($id_formulaires_reponse), //where
				'', //groupby
				'', //orderby
				'', //limit
				'', //having
				$sql_serveur//
			)
		) {
			foreach ($champs as $champ) {
				$reponses_valeurs[$id_formulaires_reponse][$champ['nom']] = [
					'valeur' =>  formidable_ajouter_action_recuperer_fichier(
						formidable_deserialize($champ['valeur']),
						$champ['nom'],
						$formulaires_saisies[$id_formulaire],
						$id_formulaire,
						$id_formulaires_reponse
					),
					'id' => $champ['id_formulaires_reponses_champ']
				];
			}
		}
	}

	// Si on demande la valeur brute, on ne génère rien, on renvoie telle quelle
	if ($type_retour == 'brut') {
		//Si c'est bien un vrai champ, et pas une explication
		if (isset($reponses_valeurs[$id_formulaires_reponse][$nom]['valeur'])) {
			return $reponses_valeurs[$id_formulaires_reponse][$nom]['valeur'];
		} else {
			return '';
		}
	}

	// Si on demande edit > mode crayon > on génère le crayon correspond
	if ($type_retour == 'edit') {
		if (isset($reponses_valeurs[$id_formulaires_reponse][$nom]['id'])) {
			$valeur = $reponses_valeurs[$id_formulaires_reponse][$nom]['id'];
		} else {
			$valeur = sql_insertq('spip_formulaires_reponses_champs', ['id_formulaires_reponse' => $id_formulaires_reponse, 'nom' => $nom]);
		}
		return 'crayon ' . 'formulaires_reponses_champ-valeur-' . $valeur;
	}
	// Si on trouve bien la saisie demandée
	if ($saisie = saisies_chercher($formulaires_saisies[$id_formulaire], $nom)) {
		// Si on demande le label, on ne génère rien, on renvoie juste le label
		if ($type_retour == 'label') {
			return $saisie['options']['label'];
		}
		// On génère la vue de cette saisie avec la valeur trouvée précédemment
		return recuperer_fond(
			'saisies-vues/_base',
			array_merge(
				[
					'type_saisie' => $saisie['saisie'],
					'valeur' => isset($reponses_valeurs[$id_formulaires_reponse][$nom]['valeur']) ? $reponses_valeurs[$id_formulaires_reponse][$nom]['valeur'] : '',
					'valeur_uniquement' => ($type_retour == 'valeur_uniquement' ? 'oui' : 'non'),
					'sans_reponse' => $sans_reponse,
				],
				$saisie['options']
			)
		);
	}
	return '';// Normalement ca ne devrait pas arriver là, c'est pour PHPstan
}

/**
 * Afficher le resume d'une reponse selon un modele qui contient des noms de champ "@input_1@ ..."
 *
 * @param int $id_formulaires_reponse
 * @param int $id_formulaire
 * @param string $modele_resume
 * @return string
 */
function affiche_resume_reponse($id_formulaires_reponse, $id_formulaire = null, $modele_resume = null) {
	static $modeles_resume = [];
	static $saisies_form = [];
	$saisies = [];

	include_spip('formidable_fonctions');

	if (is_null($id_formulaire)) {
		$id_formulaire = sql_getfetsel(
			'id_formulaire',
			'spip_formulaires_reponses',
			'id_formulaires_reponse=' . intval($id_formulaires_reponse)
		);
	}

	if (is_null($modele_resume) && !isset($modeles_resume[$id_formulaire])) {
		$row = sql_fetsel('saisies, traitements', 'spip_formulaires', 'id_formulaire=' . intval($id_formulaire));
		$saisies = formidable_deserialize($row['saisies']);
		$saisies_form[$id_formulaire] = $saisies;
		$traitements = formidable_deserialize($row['traitements']);
		if (isset($traitements['enregistrement']['resume_reponse'])) {
			$modeles_resume[$id_formulaire] = $traitements['enregistrement']['resume_reponse'];
		} else {
			$modeles_resume[$id_formulaire] = '';
		}
	}
	if (is_null($modele_resume)) {
		$modele_resume = $modeles_resume[$id_formulaire];
		$saisies = $saisies_form[$id_formulaire];
	}

	if (!$modele_resume) {
		return '';
	}

	$valeurs = [];
	$chaine = formidable_raccourcis_arobases_2_valeurs_champs(
		$modele_resume,
		$saisies,
		[
			'brut' => false,
			'sans_reponse' => '',
			'source' => 'base',
			'id_formulaires_reponse' => $id_formulaires_reponse,
			'id_formulaire' => $id_formulaire,
			'contexte' => 'affiche_resumer_reponse'
		]
	);

	return $chaine;
}

/**
 * Supprimer les balise d'une vue de saisies
 * sans pour autant faire un trim
 * @param string $valeur
 * @return string
**/
function formidable_nettoyer_saisie_vue($valeur) {
	// on ne veut pas du \n de PtoBR, mais on ne veut pas non plus faire un trim
	$valeur = str_ireplace('</p>', '', $valeur);
	$valeur = PtoBR($valeur);
	if (strpos($valeur, '</li>')) {
		$valeur = explode('</li>', $valeur);
		array_pop($valeur);
		$valeur = implode(', ', $valeur);
	}
	$valeur = supprimer_tags($valeur);
	$valeur = str_replace("\n", ' ', $valeur);
	$valeur = str_replace("\r", ' ', $valeur);
	$valeur = str_replace("\t", ' ', $valeur);
	return $valeur;
}

/**
 * Si une saisie est de type 'fichiers'
 * insère dans la description du résultat de cette saisie
 * l'url de l'action pour récuperer la saisie
 * Ajoute également une vignette correspondent à l'extention
 * @param array $saisie_a_modifier
 * @param string $nom_saisie
 * @param array $saisies_du_formulaire
 * @param int|string $id_formulaire
 * @param int|string $id_formulaires_reponse
 * return array $saisie_a_modifier
 **/
function formidable_ajouter_action_recuperer_fichier($saisie_a_modifier, $nom_saisie, $saisies_du_formulaire, $id_formulaire, $id_formulaires_reponse) {
	// précaution
	include_spip('inc/saisies_lister');
	include_spip('inc/formidable_fichiers');
	$id_formulaire = strval($id_formulaire);
	$id_formulaires_reponse = strval($id_formulaires_reponse);
	$vignette_par_defaut = charger_fonction('vignette', 'inc/');
	if (array_key_exists($nom_saisie, saisies_lister_avec_type($saisies_du_formulaire, 'fichiers'))) { //saisies SPIP
		if (is_array($saisie_a_modifier)) {
			foreach ($saisie_a_modifier as $i => $valeur) {
				$url = formidable_generer_url_action_recuperer_fichier(
					$id_formulaire,
					$id_formulaires_reponse,
					$nom_saisie,
					$valeur['nom']
				);
				$saisie_a_modifier[$i]['url'] = $url;
				if (isset($valeur['extension'])) {
					$extension = strtolower($valeur['extension']);
					if (in_array($extension, ['png','jpg','gif'])) {
						$saisie_a_modifier[$i]['vignette'] = _DIR_FICHIERS_FORMIDABLE . "formulaire_$id_formulaire/reponse_$id_formulaires_reponse/$nom_saisie/" . $valeur['nom'];
					}	else {
						$saisie_a_modifier[$i]['vignette'] = $vignette_par_defaut($extension, false);
					}
				}
			}
		}
	}
	return $saisie_a_modifier;
}



/**
 * Génère le "titre" d'une réponse
 * Utile pour l'affichage des révisions (si on a des champs extras)
 * @param int $id_formulaires_reponse
 * @param array|null $champs (idéalement pas null, mais il semblerait qu'il y ait des cas en SPIP qui font cela, dans ecrire/inc/filtres.php)
 * @return string
**/
function generer_titre_formulaires_reponse(int $id_formulaires_reponse, $champs): string {
	return affiche_resume_reponse($id_formulaires_reponse);
}

/**
 * Afficher le status en clair si pas dans la liste $not_in
 * @param string $statut
 * @param array $not_in
 * @return string
 */
function formidable_afficher_statut_si_different_de($statut, $not_in = []) {
	if (!in_array($statut, $not_in)) {
		include_spip('inc/puce_statut');
		return statut_titre('formidable', $statut);
	}

	return '';
}

/**
 * Tente de déserialiser un texte
 *
 * Si le paramètre est un tableau, retourne le tableau,
 * Si c'est une chaîne, tente de la désérialiser
 *	- d'abord depuis du JSON
 *	- puis depuis de la serialsation par défaut
 * Si échec, retourne la chaîne
 *
 * @filtre
 *
 * @param string|array $texte
 *	 Le texte (possiblement sérializé) ou un tableau
 * @return array|string
 *	 Tableau, texte désérializé ou texte
**/
function formidable_deserialize($texte) {
	if (is_null($texte)) {
		$texte = '';
	}
	// Cas 1. Deja tableau
	if (is_array($texte)) {
		return $texte;
	}
	// Cas 2. Tableau serializé en json
	$tmp = json_decode($texte, true);
	if (is_array($tmp)) {
		return $tmp;
	}
	// Cas 3. Tableau serializé en PHP, si jamais ca echout on renvoie le texte
	$tmp = @unserialize($texte);
	if (is_array($tmp)) {
		return $tmp;
	}
	return $texte;
}
