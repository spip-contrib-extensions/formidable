<?php

/**
 * Utilisation de pipelines
 *
 * @package SPIP\Formidable\Pipelines
**/

include_spip('inc/formidable_fichiers');
define(
	'_RACCOURCI_MODELE_FORMIDABLE',
	'(<(formulaire\|formidable|formidable|form)' # <modele
	. '([0-9]*)\s*' # id
	. '([|](?:<[^<>]*>|[^>])*)?' # |arguments (y compris des tags <...>)
	. '>)' # fin du modele >
	. '\s*(<\/a>)?' # eventuel </a>
);

/**
 * Ajouter la protection NoSpam de base a formidable (jeton)
 *
 * @param $formulaires
 * @return array
 */
function formidable_nospam_lister_formulaires($formulaires) {
	$formulaires[] = 'formidable';
	return $formulaires;
}

/**
 * Trouver les liens <form
 * @param $texte
 * @return array
 */
function formidable_trouve_liens($texte) {
	$formulaires = [];
	if (preg_match_all(',' . _RACCOURCI_MODELE_FORMIDABLE . ',ims', $texte, $regs, PREG_SET_ORDER)) {
		foreach ($regs as $r) {
			$id_formulaire = 0;
			if ($r[2] == 'formidable') {
				$id_formulaire = $r[3];
			} elseif ($r[2] == 'form') {
				$id_formulaire = sql_getfetsel(
					'id_formulaire',
					'spip_formulaires',
					'identifiant=' . sql_quote('form' . $r[3])
				);
			} elseif ($r[2] == 'formulaire|formidable') {
				$args = ltrim($r[4], '|');
				if (strpos($args, '=') !== false) {
					$args = explode('=', $args);
					$args = $args[1];
				}
				if (strpos($args, '|') !== false) {
					$args = explode('|', $args);
					$args = trim(reset($args));
				}
				if (is_numeric($args)) {
					$id_formulaire = intval($args);
				} else {
					$id_formulaire = sql_getfetsel(
						'id_formulaire',
						'spip_formulaires',
						'identifiant=' . sql_quote($args)
					);
				}
			}
			if ($id_formulaire = intval($id_formulaire)) {
				$formulaires[$id_formulaire] = $id_formulaire;
			}
		}
	}
	return $formulaires;
}

/**
 * 1. Appliquer les traitements post institution
 * 2. Associer/dissocier les formulaires a un objet qui les utilise (ou ne les utilise plus)
 * @param $flux
 * @return mixed
 */
function formidable_post_edition($flux) {
	// 1. Appliquer les traitements post institution
	if (
		($flux['args']['objet'] ?? '') === 'formulaires_reponse'
		&& ($flux['args']['action'] ?? '') === 'instituer'
	) {
		include_spip('formidable_fonctions');
		$flux['args']['id_formulaire'] = sql_getfetsel('id_formulaire', 'spip_formulaires_reponses', 'id_formulaires_reponse = ' . $flux['args']['id_objet']);
		$flux['args']['formulaire'] = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $flux['args']['id_formulaire']);
		$flux['args']['saisies'] = formidable_deserialize($flux['args']['formulaire']['saisies']);
		$flux['args']['traitements'] = formidable_deserialize($flux['args']['formulaire']['traitements']);
		foreach ($flux['args']['traitements'] as $traitement => $options) {
			$instituer_reponse = charger_fonction('instituer_reponse', "traiter/$traitement", true);
			if ($instituer_reponse) {
				$flux = $instituer_reponse($flux, $options);
			}
		}
	}

	// 2. Associer/dissocier les formulaires a un objet qui les utilise (ou ne les utilise plus)
	if (
		($table = $flux['args']['table'] ?? '')
		&& ($id_objet = intval($flux['args']['id_objet']))
		&& ($primary = id_table_objet($table))
		&& ($row = sql_fetsel('*', $table, "$primary=" . intval($id_objet)))
	) {
		$objet = objet_type($table);
		$contenu = implode(' ', $row);

		// On cherche les modèles insérés dans les textes du contenu
		$formulaires = formidable_trouve_liens($contenu);

		// On cherche les liaisons "vu dans le texte" qui étaient là avant
		include_spip('action/editer_liens');
		$deja = objet_trouver_liens(['formulaire' => '*'], [$objet => $id_objet], ['vu="oui"']);

		// On compare pour savoir quelles liaisons "dans le texte" n'est plus là désormais
		$del = [];
		if (count($deja)) {
			foreach ($deja as $l) {
				if (!isset($formulaires[$l['id_formulaire']])) {
					$del[] = $l['id_formulaire'];
				}
			}
		}

		// Pour les liens trouvés dans la dernière version des textes
		if (count($formulaires)) {
			objet_associer(['formulaire' => $formulaires], [$objet => $id_objet], ['vu' => 'oui']);
			objet_qualifier_liens(['formulaire' => $formulaires], [$objet => $id_objet], ['vu' => 'oui']);
		}

		// Pour les liens qui étaient avant dans le texte mais qui n'y sont plus
		// => on garde toujours la liaison mais avec vu=non, à chacun de retirer la liaison si on ne veut plus du tout, comme pour les docs
		if (count($del)) {
			objet_qualifier_liens(['formulaire' => $del], [$objet => $id_objet], ['vu' => 'non']);
		}
	}

	return $flux;
}

/**
 * Afficher les formulaires utilises par un objet
 * @param $flux
 * @return mixed
 */
function formidable_affiche_droite($flux) {
	if (
		($e = trouver_objet_exec($flux['args']['exec']))
		&& ($objet = $e['type'] ?? '')
		&& ($id = $flux['args'][$e['id_table_objet']] ?? '')
		&& sql_countsel('spip_formulaires_liens', 'objet=' . sql_quote($objet) . ' AND id_objet=' . intval($id))
	) {
		$flux['data'] .= recuperer_fond(
			'prive/squelettes/inclure/formulaires_lies',
			['objet' => $objet, 'id_objet' => $id]
		);
	}
	return $flux;
}

/**
 * Afficher l'édition des liens sur les objets configurés
 * + les dernières réponses sur la page d'accueil (si configurée)
 **/
function formidable_affiche_milieu($flux) {

	//Afficher l'édition des liens sur les objets configurés
	include_spip('inc/config');
	$texte = '';
	$e = trouver_objet_exec($flux['args']['exec']);

	if ($e && !$e['edition'] && isset($flux['args'][$e['id_table_objet']]) && in_array($e['table_objet_sql'], lire_config('formidable/objets', []))) {
		$texte .= recuperer_fond('prive/objets/editer/liens', [
			'table_source' => 'formulaires',
			'objet' => $e['type'],
			'id_objet' => $flux['args'][$e['id_table_objet']]
		]);
	}

	if ($texte) {
		if ($p = strpos($flux['data'], '<!--affiche_milieu-->')) {
			$flux['data'] = substr_replace($flux['data'], $texte, $p, 0);
		} else {
			$flux['data'] .= $texte;
		}
	}

	if (
		in_array($flux['args']['exec'], ['accueil', 'suivi_edito'])
		&& lire_config('formidable/reponses_page_accueil')
	) {
		$flux['data'] .= recuperer_fond('prive/objets/liste/formulaires_reponses', [], ['ajax' => true]);
	}
	return $flux;
}

/**
 * Optimiser la base de donnée en enlevant les liens de formulaires supprimés
 *
 * @pipeline optimiser_base_disparus
 * @param array $flux
 *     Données du pipeline
 * @return array
 *     Données du pipeline
 */
function formidable_optimiser_base_disparus($flux) {
	// Les formulaires qui sont à la poubelle
	$res = sql_select(
		'id_formulaire AS id',
		'spip_formulaires',
		'statut=' . sql_quote('poubelle')
	);
	$res2 = sql_select(
		'id_formulaire AS id',
		'spip_formulaires',
		'statut=' . sql_quote('poubelle')
	);//Copie pour supprimer les fichiers

	// On génère la suppression
	$flux['data'] += optimiser_sansref('spip_formulaires', 'id_formulaire', $res);

	while ($reponse = sql_fetch($res2)) {
		$flux['data'] += formidable_effacer_fichiers_formulaire($reponse['id']);
	}

	// les reponses qui sont associees a un formulaire inexistant
	$res = sql_select(
		'R.id_formulaire AS id',
		'spip_formulaires_reponses AS R LEFT JOIN spip_formulaires AS F ON R.id_formulaire=F.id_formulaire',
		'R.id_formulaire > 0 AND F.id_formulaire IS NULL'
	);

	$flux['data'] += optimiser_sansref('spip_formulaires_reponses', 'id_formulaire', $res);

	// Les réponses qui sont à la poubelle
	$res = sql_select(
		'id_formulaires_reponse AS id, id_formulaire AS form',
		'spip_formulaires_reponses',
		'statut=' . sql_quote('poubelle')
	);
	$res2 = sql_select(
		'id_formulaires_reponse AS id, id_formulaire AS form',
		'spip_formulaires_reponses',
		'statut=' . sql_quote('poubelle')
	);	//Copie pour la suppression des fichiers des réponses, c'est idiot de pas pouvoir faire une seule requete
	// On génère la suppression
	$flux['data'] += optimiser_sansref('spip_formulaires_reponses', 'id_formulaires_reponse', $res);
	while ($reponse = sql_fetch($res2)) {
		$flux['data'] += formidable_effacer_fichiers_reponse($reponse['form'], $reponse['id']);
	}


	// les champs des reponses associes a une reponse inexistante
	$res = sql_select(
		'C.id_formulaires_reponse AS id',
		'spip_formulaires_reponses_champs AS C
			LEFT JOIN spip_formulaires_reponses AS R ON C.id_formulaires_reponse=R.id_formulaires_reponse',
		'C.id_formulaires_reponse > 0 AND R.id_formulaires_reponse IS NULL'
	);

	$flux['data'] += optimiser_sansref('spip_formulaires_reponses_champs', 'id_formulaires_reponse', $res);


	// Les liens morts entre formulaires et objets supprimes
	include_spip('action/editer_liens');
	$flux['data'] += objet_optimiser_liens(['formulaire' => '*'], '*');

	return $flux;
}


/** Hasher les ip régulièrement
 *  @param array $flux
 *  @return array $flux
**/
function formidable_taches_generales_cron($flux) {
	$flux['formidable_hasher_ip'] = 24 * 3600;
	$flux['formidable_effacer_fichiers_email'] = 24 * 3600;
	$flux['formidable_effacer_enregistrements'] = 24 * 3600;
	return $flux;
}

/** Déclarer les formulaires et les réponses
 * au plugin corbeille
 * @param array $flux;
 * @return array $flux;
**/
function formidable_corbeille_table_infos($flux) {
	$flux['formulaires'] = [
		'statut' => 'poubelle',
		'table' => 'formulaires',
		'tableliee' => ['spip_formulaires_liens']
	];
	$flux['formulaires_reponses'] = [
		'statut' => 'poubelle',
		'table' => 'formulaires_reponses'
	];
	return $flux;
}


/**
 * Définir une fonction de contrôleur pour Crayons si on tente d'éditer un champ d'une réponse de formulaire.
 *
 * Si on édite un champ d'une réponse de formulaires avec Crayons, sans avoir créé manuellement de contrôleur spécifique
 * pour le champ en question, Crayons propose soit un textarea, soit un input.
 *
 * Vu que l'on connaît les déclarations de nos champ , on va les utiliser pour créer
 * un formulaire d'édition plus adapté à notre champ.
 *
 * @pipeline crayons_controleur
 * @param array $flux
 * @return array
 */
function formidable_crayons_controleur($flux) {
	// si Crayons a déjà trouvé de contrôleur PHP, on ne fait rien
	if ($flux['data'] != 'controleur_dist') {
		return $flux;
	}
	$type = $flux['args']['type'];

	if ($type === 'formulaires_reponses_champ') {
			$flux['data'] = charger_fonction('formulaires_reponses_champ', 'controleurs');
	}

	return $flux;
}

/**
 * Vérifier une saisie envoyée depuis un formulaire de Crayons.
 *
 * @pipeline crayons_verifier
 * @param array $flux
 * @return array
 */
function formidable_crayons_verifier($flux) {
	// Le nom du modèle envoyé par le controleur/formulaires_reponses_champs.
	if ($flux['args']['modele'] !== 'valeur' || $flux['args']['type'] !== 'formulaires_reponses_champ') {
		return $flux;
	}
	include_spip('inc/saisies');
	$id = $flux['args']['id'];
	$valeur = $flux['args']['content']['valeur'];
	$data = sql_fetsel(
		['nom', 'saisies', 'f.id_formulaire', 'r.id_formulaires_reponse'],
		['spip_formulaires_reponses_champs AS c', 'spip_formulaires_reponses AS r', 'spip_formulaires AS f'],
		["id_formulaires_reponses_champ = $id",  'r.id_formulaires_reponse = c.id_formulaires_reponse', 'f.id_formulaire = r.id_formulaire']
	);
	include_spip('formidable_fonctions');

	$saisies = formidable_deserialize($data['saisies']);
	$saisie = saisies_chercher($saisies, $data['nom']);
	$atrouver = 'content_' . $flux['args']['wid'] . '_valeur';

	if (saisies_saisie_est_tabulaire($saisie)) {
		$valeur = saisies_request($atrouver);
		$flux['data']['normaliser']['valeur'] = formidable_serialize($valeur);
	}
	saisies_set_request($data['nom'], $valeur);
	$normaliser = '';//Pour stocker la forme normalisée
	$erreur = saisies_verifier([$saisie], false, $normaliser);

	if ($erreur) {
		$flux['data']['erreurs']['valeur'] = implode('<br />', $erreur);
	} elseif (saisies_request($data['nom']) !== $valeur) {
		$flux['data']['normaliser']['valeur'] = $normaliser;
	}

	if (!$erreur && saisies_saisie_est_fichier($saisie)) {
		$flux = formidable_crayons_verifier_fichier($flux, $saisie, $data);
	}
	return $flux;
}

/**
 *
 * Vérification des crayons pour le cas d'une saisie de type fichier
 * @param array $flux de formulaire
 * @param array $saisie description de la saisie
 * @param array $data données sur le champ, tirées de sql
 * @return array $flux;
**/
function formidable_crayons_verifier_fichier(array $flux, array $saisie, array $data): array {
	include_spip('inc/cvtupload');
	include_spip('traiter/enregistrement');
	include_spip('inc/formidable_fichiers');
	include_spip('action/ajouter_documents');

	$valeur = $flux['args']['content']['valeur'];
	$nom_crayons = 'content_' . $flux['args']['wid'] . '_valeur';
	$nom_saisie = $saisie['options']['nom'];

	// Faire comme si on était un vrai formulaire CVT, et donc s'appuyer sur CVTupload qui va se charger de mettre les fichiers où il faut comme il faut
	include_spip('cvtupload_pipelines');
	cvtupload_formulaire_receptionner([
		'args' => [
			'form' => 'formidable_crayons',
			'args' => [$nom_crayons]
		]
	]);

	if (isset($_FILES[$nom_crayons])) {
		$_FILES[$nom_saisie] = $_FILES[$nom_crayons];
	}
	formidable_effacer_fichiers_champ($data['id_formulaire'], $data['id_formulaires_reponse'], $saisie['options']['nom']);
	$valeur = traiter_enregistrement_fichiers($saisie, $data['id_formulaire'], $data['id_formulaires_reponse']);
	if (is_array($valeur)) {
		$valeur = formidable_serialize($valeur);
	} else {
		$valeur = formidable_serialize([]);
	}
	$flux['data']['normaliser']['valeur'] = $valeur;

	return $flux;
}



/**
 * Function _revision pour le crayonnage direct des champs,
 * permet ensuite d'utiliser si besoin un pipeline
 * cf échange avec Cerdic sur https://git.spip.net/spip-contrib-extensions/crayons/pulls/4
 * Notes : peut être qu'il faudrait vraiment déclarer les champs comme des objets?
 * A reflechir quand on travaillera vraiment au versionnage
 * Note : paramètres obscures, mais tentons :
 * @param int $id id de la réponse
 * @param array $champs champs sql à modifier, pour nous juste un
 * @param string $type type de crayonnage
 * @param string $ref ref technique du crayon, pas utilisé chez nous
 **/
function formulaires_reponses_champ_revision($id, $champs, $type, $ref) {
	include_spip('inc/modifier');
	return objet_modifier_champs($type, $id, [], $champs);
}


/**
 * Ajouter le lien vers les formulaires
 * en renvoyant vers les formulaires publiés si existants
 * Sinon vers tous les formulaires
 * @param array $data;
 * @return array
**/
function formidable_ajouter_menus(array $data): array {
	include_spip('base/abstract_sql');
	$publies = sql_getfetsel('id_formulaire', 'spip_formulaires', 'statut = ' . sql_quote('publie'), '', '', '0,1');
	if ($publies) {
		$url = generer_url_ecrire('formulaires', 'statut=publie');
	} else {
		$url = generer_url_ecrire('formulaires');
	}
	include_spip('inc/boutons');
	if (class_exists(Spip\Admin\Bouton::Class)) {
		//SPIP 4.2 >
		$data['menu_edition'] -> sousmenu['formulaires'] = new Spip\Admin\Bouton(
			chemin_image('formulaire-xx.svg'),
			'formidable:bouton_formulaires',
			$url
		);
	} else {
		$data['menu_edition'] -> sousmenu['formulaires'] = new Bouton(
			chemin_image('formulaire-xx.svg'),
			'formidable:bouton_formulaires',
			$url
		);
	}
	return $data;
}

/**
 * Mettre le modèles de formulaire à part
 **/
function formidable_inserer_modeles_lister_categories(array $flux): array {
	$flux['formulaire'] = [
		'nom' => _T('formidable:formulaire')
	];
	return $flux;
}
