# Changelog
## Changed

### Added

- Pour `inserer_modele`, catégorie à part pour le modèle d'insertion de formulaire

## 7.0.4 - 2024-02-22

### Fixed

- Ne pas provoquer de fatal lorsqu'on passe des valeurs lors d'un appel du formulaire via un modèle

### Removed

- !formidable_traitements Dans la fonction `formulaires_formidable_traiter_dist()`, supprimer l'appel à `formidable_traitements` dont les `data` n'étaient pas identique en terme de structure aux autres appels. La seule raison d'être était de s'assurer de l'ordre, mais la manière dont est stockée la config en BDD suffit.

## 7.0.2 - 2025-02-09

### Fixed

- Permettre d'utiliser dans les `@@` les "pseudo-saisies" déclarées via le pipeline `saisies_afficher_si_saisies`
- Trouver les bons sujets d'email lorsque les réponses sont modérées à posteriori
- Dans la fonction `formidable_raccourcis_arobases_2_valeurs_champs()` ne pas provoquer d'erreur/warning si jamais le pipeline `saisies_afficher_si_saisies` est utilisé
- Éviter une fatal lorsqu'on utilise le raccourci `@message_retour@` dans le texte envoyé par email à l'internaute.

## 7.0.1 - 2025-02-06

### Fixed

- #286 Rétablir le message de retour général comme message pour l'accusé de réception si on n'a pas défini de message spécifique à l'accusé de réception

## 7.0.0 - 2025-01-22

### Added

- #283 Possibilité de dire qu'un traitement `utilise` un autre traitement, et passe donc après
- #283 Les traitements qui en nécessite un autre passent systématiquement après celui-ci
- #277 Vérification `formidable_coherence_arobase` qui vérifie si les @@ présent dans un champ correspondent aux raccourcis possible pour un formulaire donné
- #277 Pipeline `verifier_formidable_coherence_arobase` pour modifier le comportement de cette vérification
- #90 Possibilité d'activer une prévisualisation avant soumission définitive
- #276 Pour le traitement `enregistrement` pouvoir dire que la modification d'une réponse existante par l'internaute la rebascule en proposée, désactivable au cas par cas avec l'option d'appel `traiter_enregistrement_desactiver_modif_instituer_prop`
- #79 Chaque traitement peut avoir une fonction `traiter_<xxx>_instituer_reponse()` appelée lors du changement de statut d'une réponse. Reçoit le `$flux` du pipeline `post_edition`  et `$options` les options du traitement. Retourne le `$flux` éventuellement modifié.
- Lors de l'institution d'une réponse, les pipelines `pre_edition` et `post_edition` reçoivent systématiquement en `args` les infos les plus courantes sur le formulaire (identifiant, traitements, saisies)
- #264 #55 Pouvoir envoyer les emails au moment de la validation de la réponse (passage en statut publié)


### Changed

- L'action `vider_formulaire` passer par l'API d'objet de SPIP pour mettre à la poubelle les réponses, permettant ainsi l'exécution des pipelines pre et post institution
- #283 Se servir de la clé `necessite` pour indiquer le traitement `email` doit passer après le traitement `enregistrement`
- #277 Lors de la configuration du message général de retour, vérifier si les `@@` correspondent à des raccourcis inteprétables
- #277 Lors de la configuration des traitements, vérifier si les `@@` correspondent à des raccourcis inteprétables
- #281 Lors qu'une erreur est rencontrée durant l'execution d'un traitement,  `$retours['message_erreur']` ne contient que l'erreur du traitement courant
- #281 Les erreurs à l'exécution du traitement sont réunions dans des `div` spécifique à chaque traitement
- #281 Lors de l'execution d'un traitement `$retours['erreurs']` contient un tableau des erreurs traitement par traitement
- Pour le formulaire de configuration des traitements, utiliser l'API de `Saisies`, pour ne pas enregistrer les valeurs des champs de config masqués par `afficher_si`
- Configuration du traitement `enregistrement` : réorganisation ergonomique
- #197 Utiliser l'API de `saisies` pour tester les type de saisie dans `formidable_tableau_valeurs_saisies()`
- Déléguer à `saisies` tout l'affichage du formulaire
- Lors de la soumission d'une réponse, passer par `objet_inserer()` ou `objet_modifier_champs()` plutôt que d'aller directement à la couche SQL
- Lors de la soumission d'une réponse sur un formulaire à moderation _a posteriori_, la réponse n'est pas publiée tout de suite, mais on l'institue juste après soumission, ce qui permet de profiter des pipelines
- Les réponses ont désormais leur propre fonction d'institution

### Fixed

- #278 Rétablir le crayonnage des champs fichiers
- !269 Pour un formulaire contenant uniquement une case à cocher, pouvoir décocher la case d'une réponse déjà en base
- #188 Ne pas invalider le cache au changement de statut d'une réponse, mais uniquement si demandée dans la configuration du traitement et si on va vers/on quitte le statut `publie`
- #285 Vérification de l'unicité des réponses : définir les statuts de réponses à vérifier

### Removed

- Le champ `public` de la table `spip_formulaires` jamais appelé ni rempli en formidable est supprimé
- Antispam historique interne, remplacé depuis longtemps par celui de `Nospam`
- Le passage de l'option `_titre` à l'appel de `#FORMULAIRE_FORMIDABLE` est supprimé. À la place, configurer un contenu à insérer avant le formulaire : lors de l'édition des champs, se rendre sur "Options globales" puis "Autour du formulaire" champ "Texte au début du formulaire" (possibilité d'utiliser les raccourcis typographiques de SPIP).
- !260 fichier d'action `supprimer_formulaire` et `supprimer_formulaires_reponse`
- Le filtre/la fonction `tenter_unserialize` doit être remplacé par `formidable_deserialize` (même paramètre)
- Le critère `{tri_selon_donnee}` doit être remplacé par `{tri_selon_reponse}` (même paramètre)

### Deprecated

- Le second argument de `#FORMULAIRE_FORMIDABLE` doit être un tableau. Préferez un tableau vide (`#ARRAY`) à une chaîne vide. La compatibilité sera supprimé en v8.0.0
- #271 Le troisième argument de `formidable_raccourcis_arobases_2_valeurs_champs()` doit être un tableau. Les 4e, 5e, 6e, 7e arguments sont dépréciés. La retrocompatibilité sera supprimée en formidable v8.0.0.
- #271 Le troisième argument de `#FORMULAIRE_FORMIDABLE` doit être un tableau. Les 4e et 5e arguments sont dépréciés. La retrocompatiblité sera supprimée en formidable v8. Voir https://contrib.spip.net/3284#3emearg.


### Fixed


## 6.6.2 - 2024-12-27

### Fixed

- #272 Lors de la migration en json, ne pas envoyer d'email d'erreur si on a un champ traitements/saisies valant `'b:0;'`

## 6.6-1 - 2024-11-26

### Fixed

- #272 Ne pas indiquer un problème de migration `serialize` vers `json` s'il n'y aucun traitement / aucune saisie
- Problème de migration depuis les versions antérieures à 6

### Removed

- #229 Pas besoin de cocher la case `Envoyer à des reponsables` lors de la migration

## 6.6.0 - 2024-11-05


### Added

- #267 Raccourci `@id_formulaires_reponse@` pour les messages de retours indiquant l'identifiant de la réponse

### Changed

- #263 Migration progressive via `job_queue` des champs `saisies` et `traitements` de `spip_formulaires` en json lorsque pas déjà fait


## 6.5.0 - 2024-10-04
### Added

- #260 Pouvoir associer un logo à un formulaire si les pages de formulaire sont activées

### Fixed

- #439 Ne pas envoyer par mail les réponses aux saisies dépubliées, sauf si modification d'une réponse existant remplie au moment où la saisie n'était pas dépubliée- spip-contrib-extensions/saisies#439 Ne pas envoyer par mail les réponses aux saisies dépubliées, sauf si modification d'une réponse existant remplie au moment où la saisie n'était pas dépubliée
- #259 Pouvoir éditer une réponse dont tout les champs sont vides

## 6.4.0 - 2024-09-04
## Added

- #258 Position / nombre de réponses en dessous de précédent/suivant dans la boite d'info d'une réponse
- #245 Possibilité d'insérer un texte avant et après le formulaire (masqué si le formulaire est dépublié)

### Changed

- #257 Mettre le résumé de la réponse comme titre sur les pages de vue et d'édition d'une réponse

### Fixed

- #256 Avoir un fallback pour l'analyse des réponses saisies
- #253 Éviter une erreur PHP quand visiteur_session n'est pas définie (plugins-dist ou spip-cli)
- #237 Ne pas proposer d'envoyer un courriel aux visiteur·euses

### Deprecated

- #245 Le passage de `_titre` à l'appel d'un formulaire formidable est deprécié et sera supprimé dans formidable v7.0.0. Utiliser la configuration d'insertion en début de formulaire.

## 6.3.2 - 2024-06-17

### Fixed

- #251 erreur de squelettes sur les puces statut des formulaires liés
- #250 chaîne de langue "aucun formulaire" sur les formulaires liés

## 6.3.1 - 2024-05-30

### Fixed

- #248 Rétablissement du bouton « Supprimer toutes les réponses »
## 6.3.0 - 2024-05-19

### Added


- #238 Depuis la page d'un objet, pouvoir créer et lier un formulaire

### Fixed

- #244 Pouvoir utiliser les puces de changement rapide depuis la liste de formulaires liés
- Se débarrasser de PHP dans les modèles

## 6.2.0 - 2024-04-24

### Added

- !231 On passe les informations sur la saisies complète au squelette qui en analyse les réponses
### Fixed

- spip-contrib-extensions/saisies#417 faire fonctionner les `afficher_si` pour les formulaires multiétape utilisant AJAX et n'ayant pas d'`afficher_si` sur la première étape
- !224 Correction sur une coquille dans le formulaire de configuration, et migration des configurations erronées
- #241 Tenir compte du Timezone PHP pour les dates associées aux réponses, et non pas du Timezone du serveur SQL

## 6.1.2 - 2024-03-28

### Fixed

- #229 Éviter une fatale à la mise à jour vers le schéma 1.5.0 si jamais un champ `traitements` est corrompu

## 6.1.1 - 2024-03-23

### Fixed

- #234 Ne pas provoquer de fatale lors du traitement d'un formulaire avec valeurs multivaluées (bug introduit en v6.1.0)
## 6.1.0 - 2024-03-20

### Added

- #231 Les formulaires sont chargés en ajax
- #232 Il est possible de désactiver l'ajax sur un formulaire précis en passant `#ARRAY{no_ajax,oui}` en 3eme argument de l'appel à `#FORMULAIRE_FORMIDABLE`

### Fixed


- #228 mise à plat de l'id et des classes associé·es à un formulaire
	- L'identifiant est de la form `formulaire_formidable_<slug>`, ce qui
		permet par exemple de cibler un formulaire spécifique dans un jeu de
		squelettes générique sans avoir à connaître à l'avance l'identifiant
		numérique ; on ne gère pas le cas où deux formulaires formidable identique sont insérés dans la même page
	- Les classes associées sont `formulaire_formidable_<id_formulaire> formulaire_formidable_<slug>`
		ceci permet de cibler par slug ou par identifiant.
		Pour rétro-compatibilité, on garde `formulaire_formidable-<id_formulaire>` qui était là historiquement.
- #230 Au retour d'un formulaire, mettre l'ancre de l'URL à celui-ci (bug introduit en v6.0.0)

### Security

- Sanitizer les valeurs soumises à un formulaire formidable

## 6.0.0 2024-02-22

### Added

- #146 Pour les emails, trimmer aussi les espaces insécables et espaces fines insécables
- #207 Les traitements peuvent avoir une propriété (tabulaire) `necessite`, indiquant les traitements nécessaires à leur activation (gestion de dépendance)
- #205 Pouvoir choisir des auteurs/autrices comme destinataires d'un courriel
- #218 Ajout d'une autorisation `voirreponses` qui reçoit un `id_formulaire`
- #219 Autorisation d'accéder à un fichier depuis un lien dans un email

### Changed

- #206 Amélioration de l'ergonomie de la configuration générale du plugin
- #198 Amélioration de l'ergonomie de la configuration du traitement `envoyer par email`
- #205 Amélioration de la terminologie des différents destinataires possibles d'un email
- #154 Traitement email : si nom ou adresse expéditrice non défini, suivre la config du plugin `facteur`
- Traitement email: si aucun sujet est défini, et si aucun nom expéditeur n'est défini, alors mettre "<adresse_courriel> vous à écrit" comme sujet

### Fixed

- #196 Faire fonctionner l'export de l'analyse des réponse
- #208 Affichage des valeurs saisies après soumission : insérer correctement les fichiers
- Indiquer correctement le numéro de formulaire dans les classes du `div` englobant
- #218 L'autorisation `formulairesreponse_voir` reçoit un `id_formulaires_reponse` comme les autres
- #218 Ne pas appeler directement les autorisations, afin de permettre leur surcharge

## 5.7.1 - 2024-01-06

### Added

- #190 Passage de paramètres à l'aide-mémoire pour préciser le contexte d'usage

### Fixed

- #186 Le menu `formulaires` renvoie à la liste des formulaires publiés, sauf si aucun formulaire n'est publié, auquel cas renvoie à l'ensemble des formulaires

### Changed

- Adaptation de l'appel à l'aide-mémoire pour `saisies` 5.1.0

## 5.6.1 - 2023-11-21

### Changed

- #179 Saisie `champ` transférée dans le plugin `saisies`
- #179 Modèle `aide_memoire` transférée dans le plugin `saisies` (paramètres modifiés)

### Fixed

- formidable_ts#36 Pouvoir crayonner un champ fichier
- #182 Sur la page d'analyse des réponses, permettre aux images dans les explications de s'afficher
- #178 Ne pas afficher les saisies `conteneur_inline` sur l'aide mémoire

### Removed

- #179 Chaine de langue `formidable:traitement_champ_aucun` (à remplacer par `saisies:saisie_champ_aucun`)
- #179 Chaine de langue `formidable:traitements_aide_memoire`(à remplacer par `saisies:saisies_aide_memoire|label_ponctuer:>`)
## 5.6.0 - 2023-10-22

### Added

- #176 Ajouter une classe `formulaire_multietapes` sur les formulaires multiétapes pour pouvoir les cibler en css

### Fixed
- saisies#331  Multiétapes : ajuster dynamiquement le libellé des boutons précédents/suivants en fonction des `afficher_si`

## 5.5.2 - 2023-10-05

### Fixed

- Retour de la redirection après duplication d'un formulaire

## 5.5.2 - 2023-10-05

### Changed

- Cette version n'est jamais sortie officiellement (problème de tag)
## 5.5.0 - 2023-30-09

### Added

- #106 #160 Interface : pousser à choisir un traitement, indiquer lorsqu'aucun traitement n'est activé

### Fixed

- #170 Recherche dans les champs de formulaires
- Lorsqu'on crayonne un champ, le normaliser correctement

### Removed

- #106 #160 Ne plus mettre de message d'erreur / envoyer de mail lorsqu'aucun traitement n'est activé.

## 5.4.0 - 2023-08-13

### Added

- #150 possibilité de déclarer des traitements sans aucune option

### Fixed

- #147 Traitement "envoyer par email" : faire fonctionner correctement l'option "Masquer les champs vides"
- #156 Traitement "enregistrer" : ne pas provoquer d'erreur SQL lorsqu'on vérifie l'unicité d'un champ date

### Changed

- #156 Les vérifications spécifiques aux traitements ont lieu après que les saisies aient été normalisées et vérifiées

## 5.3.2 - 2023-04-16

### Fixed

- Correction du critère `{tri_selon_reponse}` lorsqu'il s'agit de trier suivant une date

## 5.3.1 - 2023-04-02

### Fixed

- #136 En spip 4.0, faire fonctionner le zippage des fichiers de type inconnus
- Comptatible SPIP 4.2

## 5.3.0 - 2023-07-01

### Changed

- #113 Amélioration de l'ergonomie de la configuration des traitements : chaque traitement se trouve désormais dans un onglet (vertical)

### Fixed

- #134 Correction bug `{tri_selon_reponse}` sous PHP 8+

## 5.2.3 - 2022-11-08

### Fixed

- #123 Faire fonctionner l'analyse des réponses pour une saisie `choix_grille` lorsque les clés sont numériques
- #127 Zipper correctement les fichiers d'un type non autorisé par SPIP
- #128 Refaire fonctionner le téléchargement des fichiers joints aux réponses lors de l'export CSV/XLSX des réponses.

## 5.2.2 - 2022-08-20

### Added

- #119 Remplacer les caractères spéciaux dans les noms des fichiers envoyés

### Fixed

- Afficher correctement la date de la précédente réponse dans le mail envoyé après modification d'une réponse

## 5.2.1 - 2022-06-08

### Fixed

- Retour du filtre historique `tenter_unserialize` (deprécié)

## 5.2.0 - 2022-05-31

### Added

- Lors de la construction du formulaire, vérifier à la fin si les champs conditionnant des affichages sont bien présents, retourner un message d'erreur dans le cas contraire
- #105 Ajout des pipelines `formidable_pre_raccourcis_arobases` et `formidable_post_raccourcis_arobases` pour ajustement les traitements avant/après l'interprétation des raccourcis `@champs@`
- #99 #111 Ajout du filtre `|formidable_deserialize` qui reçoit au choix :
 * Un tableau déjà déserializé
 * Un tableau serializé via `json_encode()`
 * Un tableau serializé via `serialize()`
 Et renvoie le tableau deserializé, ou la valeur reçue en cas d'échec.
- #111 Les traitements reçoivent comme arguments les forment déserializées des traitements et saisies associées au formulaire

### Changed

- #99 #111 Les tableaux stockés en base (saisies et traitements d'un formulaire, réponses de champs multivaluées) sont à l'avenir encodés via `json_encode()` (à travers `formidable_serialize()`).
	Le changement se fait au fur et à mesure de l'édition des formulaires. On peut donc dans une même base avoir des tableau serializé via `serialize()` (anciens) et `json_encode()` (nouveaux).
  Pour les plugins qui étendent formidable :
		- dans les traitements, on peut récuperer directement les valeurs déserializées des tableaux, qui sont désormais passés en arguments;
		- dans les squelettes et autres fonctions perso, utiliser le filtre `|formidable_deserialize`.
- #105 `formidable_raccourcis_arobases_2_valeurs_champs` reçoit désormais les arguments suivants :
	1. `$chaine` chaîne à transformer
	2. `$saisies` liste des saisies du formulaire
	3. `$options` tableau d'options, dont :
		- `brut` (bool) : mettre à true pour donner les  "valeurs brutes" ("valeurs techniques")
		- `sans_reponse` (string|bool) chaîne indiquant l'absent de réponse ; mettre à `true` pour utiliser la chaîne par défaut
		- `source` (string) source pour trouver la valeur du champ : au choix : `'request'` ou `'base'`
		- `id_formulaires_reponse` (int) : la réponse concernée
		- `id_formulaire` : le formulaire concerné
		- `contexte` contexte d'emploi  où l'on cherche à transformer les arobases
	- La rétrocompatiblité avec les anciens arguments est assuré, sauf pour les deux derniers `$&valeurs` et `&$valeurs_libelles`
- #5 Changement des arguments de `#FORMULAIRE_FORMIDABLE` :
	1. Id numérique ou identifiant textuel (inchangé)
	2. Tableau de valeurs par défaut (inchangé)
	3. Tableau d'options, anciennement identifiant de la réponse. Les options possibles sont :
		-  `id_formulaires_reponse` (int) : identifiant de la réponse à modifier
		-  `forcer_modif` (bool) : permettre de forcer la modification d'une réponse, même si non autorisé (utilisé pour la modification des réponses côté privé)
		-  `url_redirect` (string) : url de redirection
		-  `traiter_email_destinataires` (array/csv) : destinataires supplémentaires pour le traitement `email`
		-  `traiter_email_destinataires_methode` (string) : `remplacer`  pour que les emails passés en arguments  remplacent ceux de la configuration du traitements, `ajouter` pour qu'ils s'y ajoutent ; le réglage par défaut est `ajouter`
	4. Déprécié : anciennement `url_redirect`
	5. Déprécié : anciennement `forcer_modif`
	* La rétrocompatibilité des appels est assurée

### Removed

- #105 pipeline `formidable_affiche_resume_reponse` supprimé, remplacée par `formidable_pre_raccourcis_arobases` et  `formidable_post_raccourcis_arobases`

### Deprecated

- Filtre `tenter_unserialize`, utiliser à la place `formidable_deserialize`

### Fixed

- #114 Afficher correctement les erreurs lors de la saisie d'une configuration de formulaire
- Possibilité d'utiliser des emojis dans les saisies / traitements
- #108 #109 Les modèles historiques `<form>` et `<formidable>` dans l'espace privé affichent une alerte si le formulaire n'est pas publié
- `secret_du_formidable` créé également à l'installation du plugin, pas seulement à la MAJ
- Inclusivité des chaînes de langue
- Envoi de fichiers par formulaire :
	* `_FORMIDABLE_LIENS_FICHIERS_ACCUSE_RECEPTION` fonctionne désormais aussi pour les saisies `fichiers` au sein d'un `fieldset`
	* Même si l'on insère directement les fichiers dans l'email, préciser par ailleurs à quels champs ils correspondent
- Appeller les autorisations de voir les formulaires sur
	* La liste des formulaires
	* La page individuelle d'un formulaire
- N'afficher que les formulaires publiés dans la liste des formulaires que l'on peut associer à un objet
