<?php

namespace Spip\Formidable\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers verifier_formidable_coherence_arobase_selon_saisies()
 * @internal
 */

class VerifierTest extends TestCase {

	public static function dataCoherenceArobaseSelonSaisies() {
		return [
			'nopb' => [
				// Expected
				[],
				// Valeur
				'Il y a un @input_1@ mais aussi @nom_site_spip@, @id_formulaires_reponse@,  @message_retour@',
				// Saisies
				[
					'input_1' => []
				]
			],
			'arrax' => [
				// Expected
				[],
				// Valeur
				[],
				// Saisies
				[
					'input_1' => []
				]
			],
			'pb' => [
				// Expected
				['input_1'],
				// Valeur
				'Il y a un @input_1@',
				// Saisies
				[
					'input_pas_1' => []
				]
			],
			'Pas_de_pb' => [
				// Expected
				[],
				// Valeur
				'Envoyer un email à maieul@maieul.net et toto@toto.fr',
				// Saisies
				[
					'input_pas_1' => []
				]
			]
		];
	}

	/**
	 * @dataProvider dataCoherenceArobaseSelonSaisies
	**/
	public function testCoherenceArobaseSelonSaisies($expected, $valeur, $saisies) {
		$actual = verifier_formidable_coherence_arobase_selon_saisies($valeur, $saisies);
		$this->assertSame($expected, $actual);
	}


}
