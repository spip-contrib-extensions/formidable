<?php

namespace Spip\Formidable\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers formulaires_formidable_sanitizer_valeur()
 * @covers formidable_traitements_trier()
 * @covers formidable_traitements_trouver_apres_quoi()
 * @internal
 */
class FormidableTest extends TestCase {

	public static function dataSanitizerValeur() {
		return [
			'vide' => [
				// Expected
				'',
				// Provided
				''
			],
			'string' => [
				// Expected
				safehtml('string'),
				// Provided
				'string'
			],
			'tableau' => [
				// Expected
				[
					'a' => [
						['b' => safehtml('c')]
					]
				],
				[
					'a' => [
						['b' => 'c']
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataSanitizerValeur
	**/
	public function testSanitizerValeur($expected, $provided) {
		$actual = formulaires_formidable_sanitizer_valeur($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataTraitementsTrier() {
		return [
			'defaut' => [
				// Expected
				[
					'enregistrement' => [],
					'email' => ['utilise' => ['enregistrement']],
					'participation' => ['necessite' => ['enregistrement']],
					'a' => ['necessite' => ['email'], 'utilise' => ['participation']]
				],
				// Provided
				[
					'email' => ['utilise' => ['enregistrement']],
					'a' => ['necessite' => ['email'], 'utilise' => ['participation']],
					'participation' => ['necessite' => ['enregistrement']],
					'enregistrement' => [],
				],
			]
		];
	}

	/**
	 * @dataProvider dataTraitementsTrier
	**/
	public function testTraitementsTrier($expected, $provided) {
		$actual = formidable_traitements_trier($provided);
		$this->assertSame(array_keys($expected), array_keys($actual));
	}

	public static function dataTraitementsTrouverApresQuoi() {
		return [
			'defaut' => [
				// Expected
				[
					'enregistrement' => ['_apres' => []],
					'email' => ['utilise' => ['enregistrement'], '_apres' => ['enregistrement']],
					'participation' => ['necessite' => ['enregistrement'], '_apres' => ['enregistrement']],
					'a' => ['necessite' => ['email'], 'utilise' => ['participation'], '_apres' => ['email', 'enregistrement', 'participation']]
				],
				// Provided
				[
					'email' => ['utilise' => ['enregistrement']],
					'a' => ['necessite' => ['email'], 'utilise' => ['participation']],
					'participation' => ['necessite' => ['enregistrement']],
					'enregistrement' => [],
				],
			]
		];
	}

	/**
	 * @dataProvider dataTraitementsTrouverApresQuoi
	**/
	public function testTraitementsTrouverApresQuoi($expected, $provided) {
		$actual = formidable_traitements_trouver_apres_quoi($provided);
		$this->assertEquals($expected, $actual);
	}

}
