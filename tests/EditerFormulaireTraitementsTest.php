<?php

namespace Spip\Formidable\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers formulaires_editer_formulaire_traitements_generer_libelles()
 * @covers formulaires_editer_formulaire_traitements_verifier_necessite()
 * @covers formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite()
 * @covers formulaires_editer_formulaire_traitements_formater_erreur_necessite()
 * @internal
 */

class EditerFormulaireTraitementsTest extends TestCase {

	public static function dataGenererLibelles() {
		$data_afficher_si_1 = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite(['traitement0']);
		$data_afficher_si_2 = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite(['traitement0', 'traitement1']);
		$data_afficher_si_2_1_only = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite(['traitement1']);
		return [
			'vide' => [
				// Expected
				[
					'traitement0' => [
						'description' => 'description0',
						'libelle' => 'description0',
					]
				],
				// Provided
				[
					'traitement0' => [
						'description' => 'description0',
					]
				]
			],
			'1necessite' => [
				// Expected
				[
					'traitement0' => [
						'description' => 'description0',
						'libelle' => 'description0',
					],
					'traitement1' => [
						'description' => 'description1',
						'necessite' => ['traitement0'],
						'libelle' => 'description1 <div class="necessite" data-afficher_si="'. $data_afficher_si_1 .'">'._T('formidable:editer_formulaire_traitements_necessite') . '<ul><li data-afficher_si="' . $data_afficher_si_1 . '">description0</li></ul></div>',
					]
				],
				// Provided
				[
					'traitement0' => [
						'description' => 'description0',
					],
					'traitement1' => [
						'description' => 'description1',
						'necessite' => ['traitement0'],
					]
				]
			],
			'2necessite' => [
				// Expected
				[
					'traitement0' => [
						'description' => 'description0',
						'libelle' => 'description0',
					],
					'traitement1' => [
						'description' => 'description1',
						'necessite' => ['traitement0'],
						'libelle' => 'description1 <div class="necessite" data-afficher_si="'. $data_afficher_si_1 .'">'._T('formidable:editer_formulaire_traitements_necessite') . '<ul><li data-afficher_si="' . $data_afficher_si_1 . '">description0</li></ul></div>',
					],
					'traitement2' => [
						'description' => 'description2',
						'libelle' => 'description2 <div class="necessite" data-afficher_si="'. $data_afficher_si_2 .'">'._T('formidable:editer_formulaire_traitements_necessite') . '<ul><li data-afficher_si="'. $data_afficher_si_1 .'">description0</li><li data-afficher_si="' . $data_afficher_si_2_1_only .'">description1</li></ul></div>',
						'necessite' => ['traitement0', 'traitement1'],
					]
				],
				// Provided
				[
					'traitement0' => [
						'description' => 'description0',
					],
					'traitement1' => [
						'description' => 'description1',
						'necessite' => ['traitement0'],
					],
					'traitement2' => [
						'description' => 'description2',
						'necessite' => ['traitement0', 'traitement1'],
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataGenererLibelles
	**/
	public function testGenererLibelles($expected, $provided) {
		$actual = formulaires_editer_formulaire_traitements_generer_libelles($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataVerifierNecessite() {
		return [
			'no_satisfaction' => [
				// Expected
				[
					'a' => [
						'titre' => 'a',
						'necessite' => [],
					],
					'b' => [
						'titre' => 'b',
						'necessite' => [],
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a', 'b']
					]
				],
				// Choisis
				['c'],
				// Disponibles
				[
					'a' => [
						'titre' => 'a',
					],
					'b' => [
						'titre' => 'b',
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a', 'b']
					]
				],
			],
			'1_satisfaction' => [
				// Expected
				[
					'a' => [
						'titre' => 'a',
						'necessite' => [],
					],
					'b' => [
						'titre' => 'b',
						'necessite' => [],
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a']
					]
				],
				// Choisis
				['b', 'c'],
				// Disponibles
				[
					'a' => [
						'titre' => 'a',
					],
					'b' => [
						'titre' => 'b',
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a', 'b']
					]
				],
			],
			'2_satisfaction' => [
				// Expected
				[
					'a' => [
						'titre' => 'a',
						'necessite' => [],
					],
					'b' => [
						'titre' => 'b',
						'necessite' => [],
					],
					'c' => [
						'titre' => 'c',
						'necessite' => []
					]
				],
				// Choisis
				['a', 'b', 'c'],
				// Disponibles
				[
					'a' => [
						'titre' => 'a',
					],
					'b' => [
						'titre' => 'b',
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a', 'b']
					]
				],
			],
			'multiple_insatisfaction' => [
				// Expected
				[
					'a' => [
						'titre' => 'a',
						'necessite' => [],
					],
					'b' => [
						'titre' => 'b',
						'necessite' => ['a'],
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a']
					]
				],
				// Choisis
				['b', 'c'],
				// Disponibles
				[
					'a' => [
						'titre' => 'a',
					],
					'b' => [
						'titre' => 'b',
						'necessite' => ['a']
					],
					'c' => [
						'titre' => 'c',
						'necessite' => ['a']
					]
				],
			]
		];
	}

	/**
	 * @dataProvider dataVerifierNecessite
	**/
	public function testFiltrerChoisisSelonNecessite($expected, $traitements_choisis, $traitements_disponibles) {
		$actual = formulaires_editer_formulaire_traitements_verifier_necessite($traitements_choisis, $traitements_disponibles);
		$this->assertEquals($expected, $actual);
	}

	public static function dataGenererAfficherSiFromNecessite() {
		$data_a = [
			'champ' => 'traitements_choisis',
			'operateur' => 'IN',
			'valeur' => 'a',
			'negation' => '!',
		];
		$data_b = $data_a;
		$data_b['valeur'] = 'b';
		return [
			[
				// Expected
				str_replace('"', '&quot;', saisies_afficher_si_js_defaut($data_a, []) . ' || ' . saisies_afficher_si_js_defaut($data_b, [])),
				// Provided
				['a', 'b']
			]
		];
	}

	/**
	 * @dataProvider dataGenererAfficherSiFromNecessite
	**/
	public function testGenererAfficherSiFromNecessite($expected, $provided) {
		$actual = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite($provided);
		$this->assertEquals($expected, $actual);
	}

	public static function dataFormaterErreurNecessite() {
		return [
			[
				// Expected
				'<div>libelle1</div><div>libelle2</div>',
				// Provided
				[
					'traitement1' => [
						'libelle' => 'libelle1',
						'necessite' => ['traitement0']
					],
					'traitement2' => [
						'libelle' => 'libelle2',
						'necessite' => ['traitement0']
					],
					'traitement3' => [
						'libelle' => 'libelle3',
						'necessite' => []
					],
					'traitement4' => [
						'libelle' => 'libelle4',
					]
				]
			]
		];
	}

	/**
	 * @dataProvider dataFormaterErreurNecessite
	**/
	public function testFormaterErreurNecessite($expected, $provided) {
		$actual = formulaires_editer_formulaire_traitements_formater_erreur_necessite($provided);
		$this->assertSame($expected, $actual);
	}
}
