<?php


if (!function_exists('_T')) {

	function _T(string $chaine, array $arg = []): string {
		return $chaine . '/' . json_encode($arg);
	}
}
if (!function_exists('include_spip')) {

	function include_spip($i) {
		return '';
	}
}

if (!function_exists('saisies_afficher_si_js_defaut')) {
	function saisies_afficher_si_js_defaut($parse, $champs) {
		return json_encode($parse);
	}
}

if (!function_exists('safehtml')) {
	function safehtml($i) {
		return "<safehtml>$i</safehtml>";
	}
}


require_once __DIR__ . '/../vendor/autoload.php';
require_once (__DIR__ . "/../traiter/email.php");
require_once (__DIR__ . "/../verifier/formidable_coherence_arobase.php");
require_once (__DIR__ . "/../saisies-analyses/_base_fonctions.php");
require_once (__DIR__ . "/../formulaires/editer_formulaire_traitements.php");
require_once (__DIR__ . "/../inc/formidable.php");
require_once (__DIR__ . "/../formulaires/formidable.php");
