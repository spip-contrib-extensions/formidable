<?php

namespace Spip\Formidable\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers formidable_fallback_analyse()
 * @internal
 */

class AnalysesTest extends TestCase {

	public static function dataFallbackAnalyse() {
		return [
			'sans_reponse' => [
				// Expected
				[
					'type' => 'string',
					'moyenne' => 0,
					'plein' => 0,
					'vide' => 0
				],
				// Provided
				[]
			],
			'string' => [
				// Expected
				[
					'type' => 'string',
					'moyenne' => 2,
					'plein' => 3,
					'vide' => 2
				],
				// Provided
				[
					'', // Premier vide
					'', // Second vide
					'Un', // Un mot
					'Un deux', // Deux mots
					'Un deux trois', // Trois mot
				]
			],
			'array' => [
				// Expected
				[
					'type' => 'array',
					'data' => [
						'a' => 'a',
						'b' => 'b',
						'c' => 'c'
					],
				],
				// Provided
				[
					['a'] ,
					['b'],
					['c'],
				]
			],
			'mixed' => [
				// Expected
				['type' => 'mixed'],
				// Provided
				[
					'a',
					[]
				]
			],
			'mixed2' => [
				// Expected
				['type' => 'mixed'],
				// Provided
				[
					[],
					'a'
				]
			]
		];
	}

	/**
	 * @dataProvider dataFallbackAnalyse
	**/
	public function testFallbackAnalyse($expected, $provided) {
		$actual = formidable_fallback_analyse($provided);
		$this->assertEquals($expected, $actual);
	}
}
