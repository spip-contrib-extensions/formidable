<?php

namespace Spip\Formidable\Tests;

use PHPUnit\Framework\TestCase;

/**
 * @covers formidable_trim
 * @internal
 */

class TraiterEmailTest extends TestCase {

	public static function dataTrim() {
		return [
			'espace' => [
				'trimmed',
				' trimmed'
			],
			'tabulation' => [
				'trimmed',
				"\ttrimmed"
			],
			'retour' => [
				'trimmed',
				"trimmed\n\r"
			],
			'tabulation_vertical' => [
				'trimmed',
				"trimmed\v"
			],
			'espace_fine_insecable' => [
				'trimmed',
				"trimmed\u{00A0}"
			],
			'espace_fine' => [
				'trimmed',
				"trimmed\u{202f}"
			],
			'null' => [
				'',
				null
			]
		];
	}

	/**
	 * @dataProvider dataTrim
	**/
	public function testsAplatirChaine($expected, $input) {
		$actual = formidable_trim($input);
		$this->assertSame($expected, $actual);
	}


}
