<?php

/**
 * Retourner le code HTML de la vue d'un champ d'une réponse formidable pour Crayons
 *
 * @param string $type
 *     Type d'objet
 * @param string $modele
 *     Nom du modèle donné par le contrôleur
 * @param int $id
 *     Identifiant de l'objet
 * @param array $content
 *     Couples champs / valeurs postés.
 * @param $wid
 *     Identifiant du formulaire
 */
function vues_formulaires_reponses_champ_dist($type, $modele, $id, $content, $wid) {
	include_spip('formidable_fonctions');
	$data = sql_fetsel(
		['f.id_formulaire', 'r.id_formulaires_reponse', 'nom'],
		['spip_formulaires_reponses_champs AS c', 'spip_formulaires_reponses AS r', 'spip_formulaires AS f'],
		["id_formulaires_reponses_champ = $id",  'r.id_formulaires_reponse = c.id_formulaires_reponse', 'f.id_formulaire = r.id_formulaire']
	);
	return calculer_voir_reponse($data['id_formulaires_reponse'], $data['id_formulaire'], $data['nom']);
}
