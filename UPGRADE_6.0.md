# Upgrade de `formidable` de 5.x à 6.0

## Rupture de compatibilité

La seule rupture de compatibilité entre la v5.x et la v6.0 concerne l'autorisation `formulairesreponse_voir` qui reçoit désormais un `id_formulaires_reponse` et non pas un `id_formulaire`.

Les adaptations à faire :
- si vous surchargez cette autorisation, tenir compte du fait qu'elle reçoit désormais un `id_formulaires_reponse`
- si vous appelez cette autorisation via la fonction `autoriser`, passer le bon paramètre

Un grep sur votre code avec `formulairesreponse` vous permettra de trouver facilement les cas d'usage.

## Fonctions, filtres, critères dépréciées
Les fonctions, filtres et critères ci-dessous sont dépréciés. Ils disparaitront dans la version 7.0 de formidable.

- Le filtre/la fonction `tenter_unserialize` doit être remplacé par `formidable_deserialize` (même paramètre)
- Le critère `{tri_selon_donnee}` doit être remplacé par `{tri_selon_reponse}` (même paramètre)
