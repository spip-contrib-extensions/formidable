# Mise à jour de formidable vers la version 7.0

## Modification

### Gestion du statut des réponses lors de la soumission

Lors de la soumission d'une réponse de formulaire, celle-ci est d'abord mise en statut "proposée", puis, selon la configuration du traitement `enregistrer`, elle est automatiquement basculée si besoin en statut "publiée" via une fonction d'institution.

Cela veut dire que lorsqu'une réponse est automatiquement publiée (en fonction de la configuration), elle passe par les pipelines de pre/post edition et institution.

Il peut donc être nécessaire d'adapter les pipelines de pre/post edition et institution lorsqu'ils traitent des réponses. De même, il peut être nécessaire d'adapter certains traitements qui passent après le traitement `enregistrer`.

Un exemple de changement nécessaire : https://git.spip.net/spip-contrib-extensions/formidable_participation/-/merge_requests/31

## Cohérence des raccourcis `@@` dans la config des traitements

La nouvelle version du plugin vérifie que les `@champ@` utilisée dans la config des traitements existent bien.

Pour les plugins rajoutant des raccourcis `@@` personnalisé via les pipelines `formidable_pre_raccourcis_arobases` et `formidable_post_raccourcis_arobases`, il est possible d'autoriser ces raccourcis via le pipeline `verifier_formidable_coherence_arobase` qui recoit `data` les `@@` considérés comme des erreurs et en `args` l'identifiant du formulaire et la liste des saisies.

## Fonctionnalitées supprimées

### Champ `public`

Le champ `public` de la table `spip_formulaires` jamais appelé ni rempli en formidable est supprimé.


### Critères et filtres dépréciés en v6.0

- Le filtre/la fonction `tenter_unserialize` doit être remplacé par `formidable_deserialize` (même paramètre)
- Le critère `{tri_selon_donnee}` doit être remplacé par `{tri_selon_reponse}` (même paramètre)

### Options à l'appel de `#FORMULAIRE_FORMIDABLE`

L'option `_titre` est supprimée lors de l'appel à `#FORMULAIRE_FORMIDABLE`. À la place, configurer un contenu à insérer avant le formulaire : lors de l'édition des champs, se rendre sur "Options globales" puis "Autour du formulaire" champ "Texte au début du formulaire" (possibilité d'utiliser les raccourcis typographiques de SPIP).


### Fichier d'actions

Les fichiers d'action `supprimer_formulaires_reponse` et `supprimer_formulaire` sont supprimés. Utiliser le passage en statut `poubelle` à la place.

## Dépréciations

### Arguments de `#FORMULAIRE_FORMIDABLE`


Le second argument de `#FORMULAIRE_FORMIDABLE` doit être un tableau. Préferez un tableau vide (`#ARRAY`) à une chaîne vide. La compatibilité sera supprimé en v8.

Le troisième argument de `#FORMULAIRE_FORMIDABLE` doit être un tableau. Les 4e et 5e arguments sont dépréciés. La retrocompatiblité sera supprimée en formidable v8. Voir https://contrib.spip.net/3284#3emearg.

### Arguments de `formidable_raccourcis_arobases_2_valeurs_champs()`

Le troisième argument de `formidable_raccourcis_arobases_2_valeurs_champs()` doit être un tableau. Les 4e, 5e, 6e, 7e arguments sont dépréciés. La retrocompatibilité sera supprimée en formidable v8.
