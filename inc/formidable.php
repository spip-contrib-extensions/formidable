<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/*
 * Liste tous les traitements configurables (ayant une description)
 *
 * @return array Un tableau listant des saisies et leurs options
 */
function traitements_lister_disponibles() {
	static $traitements = null;

	if (is_null($traitements)) {
		$traitements = [];
		$liste = find_all_in_path('traiter/', '.+[.]yaml$');
		ksort($liste);

		if (count($liste)) {
			foreach ($liste as $fichier => $chemin) {
				$type_traitement = preg_replace(',[.]yaml$,i', '', $fichier);
				// On ne garde que les traitements qui ont bien la fonction
				if (
					charger_fonction($type_traitement, 'traiter', true)
					&& (
						is_array($traitement = traitements_charger_infos($type_traitement))
					)
				) {
					$necessite = $traitement['necessite'] ?? [];
					if (!is_array($necessite)) {
						$traitement['necessite'] = [$necessite];
					} else {
						$traitement['necessite'] = $necessite;
					}
					$traitements[$type_traitement] = $traitement;
				}
			}
		}
	}


	$traitements = pipeline(
		'formidable_traitements',
		['data' => $traitements, 'args' => []]
	);

	// On trie après le pipeline, pour permettre à celui ci d'injecter des dépendances
	// Par exemple un traitement "générer une facture" pourrait proposer un raccourci @lien_facture@ utiisable dans le traitement "email", auquel cas il faut que l'email passe après
	$traitements = formidable_traitements_trier($traitements);

	return $traitements;
}

/**
 * Charger les informations contenues dans le yaml d'un traitement
 *
 * @param string $type_traitement Le type du traitement
 * @return array Un tableau contenant le YAML décodé
 */
function traitements_charger_infos($type_traitement) {
	include_spip('inc/yaml');
	$fichier = find_in_path("traiter/$type_traitement.yaml");
	$traitement = yaml_decode_file($fichier);

	if (is_array($traitement)) {
		$traitement += ['titre' => '', 'description' => '', 'icone' => ''];
		$traitement['titre'] = $traitement['titre'] ? _T_ou_typo($traitement['titre']) : $type_traitement;
		$traitement['description'] = $traitement['description'] ? _T_ou_typo($traitement['description']) : '';
		$traitement['icone'] = $traitement['icone'] ? find_in_path($traitement['icone']) : '';
	}
	return $traitement;
}

/*
 * Liste tous les types d'échanges (export et import) existant pour les formulaires
 *
 * @return array Retourne un tableau listant les types d'échanges
 */
function echanges_formulaire_lister_disponibles() {
	// On va chercher toutes les fonctions existantes
	$liste = find_all_in_path('echanger/formulaire/', '.+[.]php$');
	$types_echange = ['exporter' => [], 'importer' => []];
	if (count($liste)) {
		foreach ($liste as $fichier => $chemin) {
			$type_echange = preg_replace(',[.]php$,i', '', $fichier);

			// On ne garde que les échanges qui ont bien la fonction
			if ($f = charger_fonction('exporter', "echanger/formulaire/$type_echange", true)) {
				$types_echange['exporter'][$type_echange] = $f;
			}
			if ($f = charger_fonction('importer', "echanger/formulaire/$type_echange", true)) {
				$types_echange['importer'][$type_echange] = $f;
			}
		}
	}
	return $types_echange;
}

/*
 * Génère le nom du cookie qui sera utilisé par le plugin lors d'une réponse
 * par un visiteur non-identifié.
 *
 * @param int $id_formulaire L'identifiant du formulaire
 * @return string Retourne le nom du cookie
 */
function formidable_generer_nom_cookie($id_formulaire) {
	return $GLOBALS['cookie_prefix'] . 'cookie_formidable_' . $id_formulaire;
}


/*
 * Trouver la réponse à éditer pour un formulaire donné,
 * dans un contexte donné
 * en fonction de la configuration du formulaire.
 * @param int $id_formulaire L'identifiant du formulaire
 * @param int $id_formulaires_reponse L'identifant de réponse passé au moment de l'appel du formulaire
 * @param array $options Les options d'enregistrement du formulaire
 * @param boolean $verifier_est_auteur si égal à true, on vérifie si $id_formulaires_reponse est passé que l'auteur connecté est bien l'auteur de la réponse passée en argument
 * @return int $id_formulaires_reponse L'identifiant de la réponse à modifier effectivement.
 *
 */
function formidable_trouver_reponse_a_editer($id_formulaire, $id_formulaires_reponse, $options, $verifier_est_auteur = false) {
	$id_formulaires_reponse = intval($id_formulaires_reponse);
	// Si on passe un identifiant de reponse, on edite cette reponse si elle existe
	if ($id_formulaires_reponse && ($verifier_est_auteur == false || $options['identification'] == 'id_reponse')) {
		return $id_formulaires_reponse;
	} else {
		// calcul des paramètres d'anonymisation

		$reponses = formidable_verifier_reponse_formulaire(
			$id_formulaire,
			$options['identification'] ?? '',
			$options['variable_php'] ?? ''
		);

		//A-t-on demandé de vérifier que l'auteur soit bien celui de la réponse?
		if (
			($id_formulaires_reponse = intval($id_formulaires_reponse))
			&& $verifier_est_auteur == true
		) {
			if (!is_array($reponses) || in_array($id_formulaires_reponse, $reponses) == false) {
				$id_formulaires_reponse = false;
			}
			return $id_formulaires_reponse;
		}

		// Si multiple = non mais que c'est modifiable, alors on va chercher
		// la dernière réponse si elle existe
		if (
			!($options['multiple'] ?? '')
			&& ($options['modifiable'] ?? '')
			&& is_array($reponses)
		) {
				$id_formulaires_reponse = array_pop($reponses);
		}
	}
	return $id_formulaires_reponse;
}

/**
 * Vérifie si le visiteur a déjà répondu à un formulaire
 *
 * @param int $id_formulaire L'identifiant du formulaire
 * @param string $choix_identification Comment verifier une reponse. Priorite sur 'cookie' ou sur 'id_auteur'
 * @param string $variable_php_identification : la variable php servant à identifier une réponse
 * @param string $anonymiser : si 'on', le formulaire doit-être anonymisé
 * @return array|false Retourne un tableau contenant les id des réponses si elles existent, sinon false
 **/
function formidable_verifier_reponse_formulaire($id_formulaire, $choix_identification = 'cookie', $variable_php_identification = '', $anonymiser = '') {
	global $auteur_session;
	$id_auteur = $auteur_session ? intval($auteur_session['id_auteur']) : 0;
	$nom_cookie = formidable_generer_nom_cookie($id_formulaire);
	$cookie = isset($_COOKIE[$nom_cookie]) ? $_COOKIE[$nom_cookie] : false;
	$variable_php_identification = formidable_variable_php_identification($variable_php_identification, $id_formulaire);
	$where = '';

	// ni cookie ni id, ni variable_php,  on ne peut rien faire
	if (!$cookie && !$id_auteur && !$variable_php_identification) {
		return false;
	}

	// Determiner les différentes clauses $WHERE possible en fonction de ce qu'on a
	$where_id_auteur = '';
	$where_cookie = '';
	$where_variable_php = '';
	if ($id_auteur) {
		if ($anonymiser == 'on') {
			$id_auteur = formidable_hasher_id_auteur($id_auteur);
			$where_id_auteur = 'variable_php="' . $id_auteur . '"';
		} else {
			$where_id_auteur = 'id_auteur=' . $id_auteur;
		}
	}
	if ($cookie) {
		$where_cookie = 'cookie=' . sql_quote($cookie);
	}
	if ($variable_php_identification) {
		$where_variable_php = 'variable_php=' . $variable_php_identification;
	}

	// Comment identifie-t-on? Attention, le choix d'identification indique une PRIORITE, donc cela veut dire que les autres méthodes peuvent venir après, sauf dans le cas d'identification explicitement par id_reponse
	if ($choix_identification === 'cookie' || !$choix_identification) {
		if ($cookie) {
			$where = [$where_cookie];
		} else {
			$where = [$where_id_auteur, $where_variable_php];
		}
	} elseif ($choix_identification == 'id_auteur') {
		if ($id_auteur) {
			$where = [$where_id_auteur];
		} else {
			if ($anonymiser) {
				$where = [$where_cookie];
			} else {
				$where = [$where_cookie, $where_variable_php];
			}
		}
	} elseif ($choix_identification == 'variable_php') {
		if ($variable_php_identification) {
			$where = [$where_variable_php];
		} else {
			$where = [$where_cookie, $where_id_auteur];
		}
	} elseif ($choix_identification == 'id_reponse') {//Si le filtrage se fait par réponse, on prend tout (mais normalement on devrait pas aboutir ici si tel est le cas)
		$where = ['1=1'];
	}
	$where = array_filter($where);//Supprimer les wheres null
	$where = implode(' OR ', $where);

	$reponses = sql_allfetsel(
		'id_formulaires_reponse',
		'spip_formulaires_reponses',
		[
			['=', 'id_formulaire', intval($id_formulaire)],
			sql_in('statut', ['prop', 'publie']),
			$where
		],
		'',
		'date'
	);

	if (is_array($reponses)) {
		return array_column($reponses, 'id_formulaires_reponse');
	} else {
		return false;
	}
}

/**
 * Vérifier si une réponse de formulaire existe déjà avec la même valeur pour le champ unique
 * Retourne
 * - true si pas de doublon (unicité),
 * - false si une réponse existe déjà (non unicité)
 *
 * @param int         $id_formulaire
 * @param int|null    $id_formulaires_reponse
 * @param string|null $valeur
 * @return boolean
 */
function formidable_verifier_unicite(int $id_formulaire, ?int $id_formulaires_reponse = null, ?string $valeur = null) {
	if(!$id_formulaires_reponse && !$valeur) {
		return true;
	}
	$traitements = sql_getfetsel('traitements', 'spip_formulaires', 'id_formulaire = ' . $id_formulaire);
	include_spip('formidable_fonctions');
	$traitements = formidable_deserialize($traitements);
	$champ_unique = $traitements['enregistrement']['unicite'] ?? null;
	$statuts = $traitements['enregistrement']['unicite_statuts'] ?? ['publie'];

	if (!$valeur && $id_formulaires_reponse) {
		$valeur = sql_getfetsel(
			'valeur',
			'spip_formulaires_reponses_champs',
			'id_formulaires_reponse = ' . $id_formulaires_reponse . ' AND nom = ' . sql_quote($champ_unique)
		);
	}

	if ($champ_unique && $statuts && $valeur) {
		if ($id_formulaires_reponse) {
			$unicite_exclure_reponse = ' AND R.id_formulaires_reponse != ' . $id_formulaires_reponse;
		} else {
			$unicite_exclure_reponse = '';
		}
		if (sql_allfetsel(
			'R.id_formulaire AS id',
			'spip_formulaires_reponses AS R
				LEFT JOIN spip_formulaires AS F
				ON R.id_formulaire=F.id_formulaire
				LEFT JOIN spip_formulaires_reponses_champs AS C
				ON R.id_formulaires_reponse=C.id_formulaires_reponse',
			'R.id_formulaire = ' . $id_formulaire .
			$unicite_exclure_reponse .
			' AND C.nom=' . sql_quote($champ_unique) . '
			  AND C.valeur=' . sql_quote($valeur) . '
			  AND ' . sql_in('R.statut', $statuts)
		)) {
			return false;
		}
	}

	return true;
}

/*
 * Génère la vue d'analyse de toutes les réponses à une saisie
 *
 * @param array $saisie Un tableau décrivant une saisie
 * @param array $env L'environnement, contenant normalement la réponse à la saisie
 * @return string Retour le HTML des vues
 */
function formidable_analyser_saisie($saisie, $valeurs = [], $reponses_total = 0, $format_brut = false) {
	// Si le paramètre n'est pas bon ou que c'est un conteneur, on génère du vide
	if (!is_array($saisie) || ($saisie['saisies'] ?? '')) {
		return '';
	}

	$contexte = ['reponses_total' => $reponses_total, '_saisie' => $saisie, 'type_saisie' => $saisie['saisie']];

	// Peut-être des transformations à faire sur les options textuelles
	$options = $saisie['options'];
	foreach ($options as $option => $valeur) {
		$options[$option] = _T_ou_typo($valeur, 'multi');
	}

	// On ajoute les options propres à la saisie
	$contexte = array_merge($contexte, $options);

	// On récupère toutes les valeurs du champ
	if (
		is_array($valeurs[$contexte['nom']] ?? '')
	) {
		$contexte['valeurs'] = $valeurs[$contexte['nom']];
	} else {
		$contexte['valeurs'] = [];
	}

	// On génère la saisie
	if ($format_brut) {
		return analyser_saisie($contexte);
	} else {
		return recuperer_fond(
			'saisies-analyses/_base',
			$contexte
		);
	}
}

/*
 * Renvoie une analyse d'une saisie spécifique
 *
 * @param array $saisie Un tableau décrivant une saisie,
 *	avec une clé spéciale `valeurs` indiquer les valeurs
 * @return array Tableau contenant le résultat de l'analyse
 */
function analyser_saisie($saisie) {
	if (($saisie['type_saisie'] ?? '') === '') {
		return '';
	}

	$ligne = [];

	switch ($saisie['type_saisie']) {
		case 'selecteur_rubrique':
		case 'selecteur_rubrique_article':
		case 'selecteur_article':
			$ligne['plein'] = count(array_filter($saisie['valeurs']));
			$ligne['vide'] = count(array_diff_key($saisie['valeurs'], array_filter($saisie['valeurs'])));
			break;
		case 'radio':
		case 'selection':
		case 'selection_multiple':
		case 'choix_couleur':
		case 'checkbox':
			$stats = [];
			foreach ($saisie['valeurs'] as $valeur) {
				if (is_array($valeur)) {
					foreach ($valeur as $choix) {
						if (isset($stats["choix-$choix"])) {
							$stats["choix-$choix"]++;
						} else {
							$stats["choix-$choix"] = 1;
						}
					}
				} else {
					if (isset($stats["choix-$valeur"])) {
						$stats["choix-$valeur"]++;
					} else {
						$stats["choix-$valeur"] = 1;
					}
				}
			}
			if (isset($saisie['datas'])) {
				$saisie['data'] = $saisie['datas'];
			}
			$data = is_string($saisie['data'])
				? saisies_chaine2tableau(saisies_aplatir_chaine($saisie['data']))
				: $saisie['data'];
			foreach ($data as $key => $val) {
				$nb = (isset($stats["choix-$key"]))
					? $stats["choix-$key"]
					: 0;
				$ligne[$val] = $nb;
			}
			break;
		case 'destinataires':
			$stats = [];
			foreach ($saisie['valeurs'] as $valeur) {
				foreach ($valeur as $choix) {
					if (isset($stats["choix-$choix"])) {
						$stats["choix-$choix"]++;
					} else {
						$stats["choix-$choix"] = 1;
					}
				}
			}
			foreach ($stats as $key => $val) {
				$key = str_replace('choix-', '', $key);
				if ($key == '') {
					$key = '<valeur vide>';
				}
				$auteur = sql_getfetsel('nom', 'spip_auteurs', "id_auteur=$key");
				$ligne[$auteur] = $val;
			}
			break;
	}

	$vide = 0;
	foreach ($saisie['valeurs'] as $valeur) {
		if ($valeur == '') {
			$vide++;
		}
		switch ($saisie['type_saisie']) {
			case 'case':
			case 'oui_non':
				if (isset($ligne['oui']) == false) {
					$ligne['oui'] = 0;
				}
				if (isset($ligne['non']) == false) {
					$ligne['non'] = 0;
				}
				if ($valeur) {
					$ligne['oui']++;
				} else {
					$ligne['non']++;
				}
				break;
			case 'input':
			case 'hidden':
			case 'explication':
				break;
		}
	}
	$ligne['sans_reponse'] = $vide;
	$ligne['header'] = $saisie['label'] ?? $saisie['type_saisie'];

	return $ligne;
}




/**
 * Retourne un texte du nombre de réponses
 *
 * @param int $nb
 *	 Nombre de réponses
 * @return string
 *	 Texte indiquant le nombre de réponses
**/
function titre_nb_reponses($nb) {
	if (!$nb) {
		return _T('formidable:reponse_aucune');
	}
	if ($nb == 1) {
		return _T('formidable:reponse_une');
	}
	return _T('formidable:reponses_nb', ['nb' => $nb]);
}

/**
 * Transforme le hash MD5 en une valeur numérique unique
 *
 * trouvé ici : http://stackoverflow.com/questions/1422725/represent-md5-hash-as-an-integer
 * @param string $hex_str La valeur alphanumérique à transformer
 * @return string Valeur numérique
*/
function md5_hex_to_dec($hex_str) {
	$arr = str_split($hex_str, 4);
	$dec = [];
	foreach ($arr as $grp) {
		$dec[] = str_pad(hexdec($grp), 5, '0', STR_PAD_LEFT);
	}

	/* on s'assure que $result ne commence pas par un zero */
	$result = implode('', $dec);
	for ($cpt = 0; $cpt < strlen($result); $cpt++) {
		if ($result[$cpt] != '0') {
			break;
		}
	}
	$result = substr($result, $cpt);
	return $result;
}

/**
 * Transforme un login en une valeur numérique de 19 caractères
 *
 * NOTE: il devient impossible de retrouver la valeur d'origine car le HASH
 * est coupé à 19cars et est donc incomplet. L'unicité n'est pas garantie mais
 * les chances pour que deux logins tombent sur le même HASH sont de 1 sur
 * 10 milliards de milliards
 * A la fin, on recherche et supprime les éventuels zéros de début
 * @param string $login Login à transformer
 * @param string $id_form ID du formulaire concerné
 * @return string Un nombre de 19 chiffres
*/
function formidable_scramble($login, $id_form) {
	if (isset($GLOBALS['formulaires']['passwd']['interne']) == false) {
		$passwd = $GLOBALS['formulaires']['passwd']['interne'];
	} else {
		$passwd = 'palabresecreta';
	}
	$login_md5 = md5("$login$passwd$id_form");
	$login_num = md5_hex_to_dec($login_md5);
	$login_num = substr($login_num, 0, 19);

	return $login_num;
}

/**
 * Dans une chaîne, remplace les @raccourci@
 * par la valeur saisie.
 * @param string $chaine la chaîne à transformer
 * @param array $saisies la liste des saisies du formulaire
 * @param array $options
 *	- 'brut' bool mettre à true pour indiquer si on veut utiliser les valeurs brutes;
 *  - 'sans_reponse' string indiquant la chaîne 'sans_reponse', mettre à true pour chaine par défaut
 *	- 'source' : 'request' (défaut) pour prendre dans `_request()?; 'base' pour prendre dans une réponse enregistrée en base
 *  - 'id_formulaires_reponse' : le cas échéant le numéro de réponse en base
 *  - 'id_formulaire' : le cas échéant le numéro du formulaire en base
 *  - 'contexte' : le contexte d'appel (ex. 'affiche_resume_reponse')
 * @param string|bool $deprecated_sans_reponse
 * @param string $deprecated_source
 * @param int|string $deprecated_id_formulaires_reponse
 * @param int|string $deprecated_id_formulaire
 * @return string la chaîne transformée
 */
function formidable_raccourcis_arobases_2_valeurs_champs(string $chaine, array $saisies, $options = [], $deprecated_sans_reponse = true, $deprecated_source = 'request', $deprecated_id_formulaires_reponse = 0, $deprecated_id_formulaire = 0): string {
	// Retrocompatibilité
	if (!is_array($options)) {
		$options = [
			'brut' => $options,
			'source' => $deprecated_source,
			'sans_reponse' => $deprecated_sans_reponse,
			'id_formulaires_reponse' => $deprecated_id_formulaires_reponse,
			'id_formulaire' => $deprecated_id_formulaire
		];
		$deprecate = 'Le troisième argument de `formidable_raccourcis_arobases_2_valeurs_champs()` doit être un tableau. Les 4e, 5e, 6e, 7e arguments sont dépréciés. La retrocompatibilité sera supprimée en formidable v8.0.0.';
		trigger_error($deprecate, E_USER_DEPRECATED);
		spip_log($deprecate, 'deprecated_formidable');
	}

	// Initialiser les options par défaut
	$brut = $options['brut'] ?? false;
	$sans_reponse = $options['sans_reponse'] ?? true;
	$source = $options['source'] ?? 'request';
	$id_formulaires_reponse = $options['id_formulaires_reponse'] ?? 0;
	$id_formulaire = $options['id_formulaire'] ?? 0;
	$contexte = $options['contexte'] ?? '';


	$valeurs = [];
	$valeurs_libellees = [];

	if ($source === 'request') {
		[$valeurs, $valeurs_libellees] = formidable_tableau_valeurs_saisies($saisies, $sans_reponse);
	} elseif ($source === 'base' && $id_formulaires_reponse && $id_formulaire) {

		// Retrouver les valeurs en base pour appliquer les afficher_si le cas échéant (pour les saisies de type "explication")
		$valeurs_en_base =  [];
		$res = sql_select('nom, valeur', 'spip_formulaires_reponses_champs', 'id_formulaires_reponse = ' . intval($id_formulaires_reponse));
		while ($row = sql_fetch($res)) {
			$valeurs_en_base[$row['nom']] = $row['valeur'];
		}
		$saisies_post_afficher_si = saisies_verifier_afficher_si($saisies, $valeurs_en_base);

		$saisies = saisies_lister_par_nom($saisies, false);
		$saisies_post_afficher_si = saisies_lister_par_nom($saisies_post_afficher_si, false);

		foreach ($saisies as $nom => $saisie) {
			if (!array_key_exists ($nom, $saisies_post_afficher_si)) {
				$valeurs_libellees[$nom] = $valeurs[$nom] = '';
			} elseif (saisies_saisie_est_champ($saisie)) {
				$valeurs[$nom] = formidable_nettoyer_saisie_vue(saisies_tableau2chaine(calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $nom, '', 'brut', $sans_reponse)));
				$valeurs_libellees[$nom] =  formidable_nettoyer_saisie_vue(calculer_voir_reponse($id_formulaires_reponse, $id_formulaire, $nom, '', 'valeur_uniquement', $sans_reponse));
			} else {
				$valeurs_libellees[$nom] = $valeurs[$nom] = $saisie['options']['texte'];
			}
		}
	}

	$chaine = pipeline(
		'formidable_pre_raccourcis_arobases',
		[
			'data' => $chaine,
			'args' => [
				'saisies' => $saisies,
				'brut' => $brut,
				'sans_reponse' => $sans_reponse,
				'source' => $source,
				'id_formulaires_reponse' => $id_formulaires_reponse,
				'id_formulaire' => $id_formulaire,
				'contexte' => $contexte,
				'valeurs' => $valeurs,
				'valeurs_libellees' => $valeurs_libellees
			]
		]
	);
	$a_remplacer = [];
	if (preg_match_all('/@[\w]+@/', $chaine, $match)) {
		$a_remplacer = $match[0];
		foreach ($a_remplacer as $cle => $val) {
			$a_remplacer[$cle] = trim($val, '@');
		}
		$a_remplacer = array_flip($a_remplacer);
		if ($brut) {
			if (is_array($valeurs)) {
				$a_remplacer = array_intersect_key($valeurs, $a_remplacer);
			}
		}
		else {
			if (is_array($valeurs_libellees)) {
				$a_remplacer = array_intersect_key($valeurs_libellees, $a_remplacer);
			}
		}
		$a_remplacer = array_merge(
			$a_remplacer,
			[
				'nom_site_spip' => lire_config('nom_site'),
				'id_formulaires_reponse' => $id_formulaires_reponse
			]
		);
		if (strpos($chaine, '@message_retour@') !== false) {// test pour éviter recurrence infinie
			$message_retour = sql_getfetsel('message_retour', 'spip_formulaires', "id_formulaire=$id_formulaire");
			$message_retour = formidable_raccourcis_arobases_2_valeurs_champs(
				$message_retour,
				$saisies,
				[
					'brut' => $brut,
					'sans_reponse' => $sans_reponse,
					'source' => $source,
				]
			);
			$a_remplacer = array_merge($a_remplacer, ['message_retour' => $message_retour]);
		}
	}
	$a_remplacer = array_map('trim', $a_remplacer);
	$chaine = trim(_L($chaine, $a_remplacer));
	$chaine = pipeline(
		'formidable_post_raccourcis_arobases',
		[
			'data' => $chaine,
			'args' => [
				'saisies' => $saisies,
				'brut' => $brut,
				'sans_reponse' => $sans_reponse,
				'source' => $source,
				'id_formulaires_reponse' => $id_formulaires_reponse,
				'id_formulaire' => $id_formulaire,
				'contexte' => $contexte,
				'valeurs' => $valeurs,
				'valeurs_libellees' => $valeurs_libellees
			]
		]
	);
	return $chaine;
}
/**
 * Récupère l'ensemble des valeurs postée dans un formulaires
 * Les retourne sous deux formes : brutes et libellés (par ex. pour les @select@
 * @param array $saisies les saisies du formulaires
 * @param string|bool $sans_reponse chaine à afficher si pas de réponse. Si true, prend la chaîne par défaut
 * @return array (brutes, libellées)
 * On met les résultats en statiques pour gagner un peu de temps
 */
function formidable_tableau_valeurs_saisies($saisies, $sans_reponse = true) {
	static $valeurs = [];
	static $valeurs_libellees = [];
	static $done = false;
	if ($done) {
		return [$valeurs,$valeurs_libellees];
	}
	// On parcourt les champs pour générer le tableau des valeurs
	if ($sans_reponse === true) {
		$sans_reponse =  _T('saisies:sans_reponse');
	}
	include_spip('inc/saisies_afficher_si_php');
	$saisies_par_nom_apres_afficher_si = saisies_lister_par_nom(saisies_verifier_afficher_si($saisies));
	$champs = saisies_lister_champs($saisies);


	// On n'utilise pas formulaires_formidable_fichiers,
	// car celui-ci retourne les saisies fichiers du formulaire dans la base… or, on sait-jamais,
	// il peut y avoir eu une modification entre le moment où l'utilisateur a vu le formulaire et maintenant
	foreach ($saisies_par_nom_apres_afficher_si as $champ => $saisie) {
		if (saisies_saisie_est_fichier($saisie) || saisies_saisie_est_avec_sous_saisies($saisie)) {
			// si on a affaire à une saisie de type fichiers ou conteneur, on considère qu'il n'y pas vraiment de valeur brute
		} elseif (!saisies_saisie_est_champ($saisie) && isset($saisies_par_nom_apres_afficher_si[$champ])) {
			$valeurs[$champ] = $saisie['options']['texte'];
			$valeurs_libellees[$champ] =  $valeurs[$champ];
		}	else {
			// On récupère la valeur postée
			$valeurs[$champ] = _request($champ);
			$valeurs_libellees[$champ] = formidable_nettoyer_saisie_vue(recuperer_fond(
				'saisies-vues/_base',
				array_merge(
					[
						'type_saisie' => $saisie['saisie'],
						'valeur' => $valeurs[$champ],
						'valeur_uniquement' => 'oui',
						'sans_reponse' => $sans_reponse
					],
					$saisie['options']
				)
			));
		}
	}
	$done = true;
	return [$valeurs, $valeurs_libellees];
}


/**
 * Retourne la valeur "scrambelisée" de la variable PHP d'identification.
 * pour les deux variables proposés par formidable, recherche directement dans $_SERVER
 * sinon utilise un eval() si une autre variable a été défini en global.
 * Mais peu probable que le cas se présente, car pas d'interface dans le .yaml pour proposer d'autres variables que celle définies par formidable
 * @param string $nom_variable le nom de la variable
 * @param string $id_formulaire le formulaire concerné
 * @return string
 */
function formidable_variable_php_identification($nom_variable, $id_formulaire) {
	//Pour compat ascendante
	if (isset($GLOBALS['formulaires']['variables_anonymisation'])) {
		$nom_variable = $GLOBALS['formulaires']['variables_anonymisation'][$nom_variable];
		$valeur_variable = eval("return $nom_variable;");
	}

	if (in_array($nom_variable, ['remote_user', 'php_auth_user'])) {
		$nom_variable = strtoupper($nom_variable);
		$valeur_variable = isset($_SERVER[$nom_variable]) ? $_SERVER[$nom_variable] : 0;
	}  else {
		$valeur_variable = 0;
	}

	if ($valeur_variable) {
		$valeur_variable = formidable_scramble($valeur_variable, $id_formulaire);
	}
	return $valeur_variable;
}

/**
 * Retourne une valeur hashée de l'id_auteur + secret du site.
 * @param string $id_auteur
 * @return string
 */
function formidable_hasher_id_auteur($id_auteur = '') {
	include_spip('inc/securiser_action');
	$pass = secret_du_site();
	return md5($pass . $id_auteur);
}

/**
 * Fonction de rappel pour array_walk_recursive
 * pour automatiquement transformer les valeur numérique en strval
 * comme SPIP le fait (mais par quel biais?) lorsqu'on envoie un tableau en environnement
 * @param &$value
 * @param $key
**/
function formidable_array_walk_recursive_strval(&$value, $key) {
	if (!is_array($value)) {
		$value = strval($value);
	}
}


/**
 * Sérialize un tableau, pour stockage en base.
 * On sérialize en json qui est plus robuse que la serialization PHP (pb #99)
 *
 * @param array $tab Le tableau
 * @return string
**/
function formidable_serialize(array $tab): string {
	return json_encode($tab);
}


/**
 * Trim un peu étendu
 * pour tenir compte des espaces insécables
 * que les gens insèrent parfois au clavier
 * @see https://git.spip.net/spip-contrib-extensions/formidable/issues/146
 * @param string|null $string
 * @return string
**/
function formidable_trim(?string $string): string {
	if (!$string) {
		return '';
	}
	return trim(
		$string,
		" \n\r\t\v\x00" // defaut
		. "\u{00A0}" // Espace fine insécable
		. "\u{202F}" // Espace insécable
	);
}

/**
 * Prend une liste de traitements et les tri
 * en placant les traitements qui ont un necessite/utilise après le traitement utilisé/necessité
 * @param array $traitements non classés
 * @return array $traitements
 **/
function formidable_traitements_trier(array $traitements): array {
	$traitements = formidable_traitements_trouver_apres_quoi($traitements);
	uksort($traitements, function ($a, $b) use ($traitements) {
		$a_after = $traitements[$a]['_apres'];
		$b_after = $traitements[$b]['_apres'];

		if (in_array($a, $b_after)) {
			return -1;
		}
		if (in_array($b, $a_after)) {
			return 1;
		}

		return 0;
	});
	return $traitements;
}

/**
 * Prend le tableau des traitements
 * Et complète chacun d'entre eux en indiquant après quels traitements ils passent nécessairement
 * en remontant l'arbre de dépendance
 *
 * @param array $traitements
 * @return array $traitements
 **/
function formidable_traitements_trouver_apres_quoi(array $traitements): array {
	foreach ($traitements as &$traitement) {
		$traitement = formidable_traitements_trouver_apres_quoi_un_traitement($traitement, $traitements);
	}
	return $traitements;
}


/**
 * Prend un traitement
 * Et le complète en indiquant après quels traitements il passe nécessairement
 * en remontant l'arbre de dépendance
 * @param array $un_traitement
 * @param array $les_traitements
 * @return array $un_traitements
 * @internal
 **/
function formidable_traitements_trouver_apres_quoi_un_traitement(array $un_traitement, array &$les_traitements): array {
	if (isset($un_traitement['_apres'])) {
		return $un_traitement;
	}
	$un_traitement['_apres'] = array_merge($un_traitement['necessite'] ?? [], $un_traitement['utilise'] ?? []);
	$apres_actuel = $un_traitement['_apres'];
	foreach ($apres_actuel as $apres) {
		if (!array_key_exists('_apres', $les_traitements[$apres])) {
			$les_traitements[$apres] = formidable_traitements_trouver_apres_quoi_un_traitement($les_traitements[$apres], $les_traitements);
		}
		$un_traitement['_apres'] = array_merge($les_traitements[$apres]['_apres'], $un_traitement['_apres']);
		$un_traitement['_apres'] = array_unique($un_traitement['_apres']);
	}
	sort($un_traitement['_apres']);
	return $un_traitement;
}
