<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/formidable');
include_spip('inc/config');

function formulaires_exporter_formulaire_analyse_charger($id_formulaire = 0) {
	$contexte = [];
	$contexte['id_formulaire'] = intval($id_formulaire);
	return $contexte;
}

function formulaires_exporter_formulaire_analyse_verifier($id_formulaire = 0) {
	$erreurs = [];

	return $erreurs;
}

function formulaires_exporter_formulaire_analyse_traiter($id_formulaire = 0) {
	$retours = [];

	if (_request('type_export') == 'csv') {
		action_exporter_analyse_reponses($id_formulaire);
	} elseif (_request('type_export') == 'xls') {
			action_exporter_analyse_reponses($id_formulaire, 'TAB');
	}

	return $retours;
}


/*
 * Exporter les analyses d'un formulaire (anciennement action/exporter_analyse_reponses_dist)
 * @param integer $id_formulaire
 * @return unknown_type
 */
function action_exporter_analyse_reponses($id_formulaire, $delim = ',') {
	// on ne fait des choses seulements si le formulaire existe et qu'il a des enregistrements
	include_spip('formidable_fonctions');
	$ok = false;
	if (
		$id_formulaire > 0
		&& ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $id_formulaire))
		&& ($reponses = sql_allfetsel('*', 'spip_formulaires_reponses', 'id_formulaire = ' . $id_formulaire . ' and statut = ' . sql_quote('publie')))
	) {
		include_spip('inc/saisies');
		include_spip('classes/facteur');
		include_spip('inc/filtres');
		$reponses_completes = [];

		$saisies = saisies_lister_par_nom(formidable_deserialize($formulaire['saisies']), false);

		// exclure les champs non analysés
		$traitement = formidable_deserialize($formulaire['traitements']);
		foreach (explode('|', $traitement['enregistrement']['analyse_exclure_champs']) as $exclure) {
			unset($saisies[$exclure]);
		}
		$res = sql_select(
			['nom, valeur'],
			'spip_formulaires_reponses_champs AS FRC,
			spip_formulaires_reponses AS FR,
			spip_formulaires AS F',
			"FRC.id_formulaires_reponse=FR.id_formulaires_reponse AND FR.statut='publie'
			AND F.id_formulaire=FR.id_formulaire
			AND F.id_formulaire=$id_formulaire"
		);

		$valeurs = [];
		while ($r = sql_fetch($res)) {
			$valeurs[$r['nom']][] =  formidable_deserialize($r['valeur']);
		}

		$reponse_complete[] = [];
		foreach ($saisies as $nom => $saisie) {
			$reponse_complete[] = formidable_analyser_saisie($saisie, $valeurs, 0, true);
		}

		$colonnes = [_T('formidable:champs'), _T('formidable:sans_reponses')];
		foreach ($reponse_complete as $reponses) {
			foreach ($reponses as $key => $reponse) {
				if ($key == 'header' || $key == 'sans_reponse') {
					continue;
				}
				if (in_array($key, $colonnes) == false) {
					array_push($colonnes, $key);
				}
			}
		}

		$reponse_complete = array_filter($reponse_complete); // Exclure les champs qui ne produisent pas d'analyse
		foreach ($reponse_complete as $reponses) {
			foreach ($colonnes as $colonne) {
				$csv[$reponses['header']][$colonne] =
					isset($reponses[$colonne])
						? $reponses[$colonne]
						: '';
			}
			$csv[$reponses['header']][_T('formidable:champs')] = $reponses['header'];
			$csv[$reponses['header']][_T('formidable:sans_reponses')] = $reponses['sans_reponse'];
		}

		$cpt_ligne = 1;
		$reponses_completes = [];
		$reponses_completes[0] = $colonnes;
		foreach ($csv as $ligne => $colonnes) {
			$cpt_colonne = 0;
			foreach ($colonnes as $colonne) {
				$reponses_completes[$cpt_ligne][$cpt_colonne++] = $colonne;
			}
			$cpt_ligne++;
		}

		if ($reponses_completes && ($exporter_csv = charger_fonction('exporter_csv', 'inc/', true))) {
			$exporter_csv('analyses-formulaire-' . $formulaire['identifiant'], $reponses_completes, $delim);
			exit();
		}
	}
}
