<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function formulaires_editer_formulaire_champs_charger($id_formulaire, $redirect = '') {
	$id_formulaire = intval($id_formulaire);
	$contexte = [];
	$contexte['id_formulaire'] = $id_formulaire;
	include_spip('formidable_fonctions');

	// On teste si le formulaire existe
	if (
		$id_formulaire
		&& ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $id_formulaire))
		&& autoriser('editer', 'formulaire', $id_formulaire)
	) {
		$saisies = formidable_deserialize($formulaire['saisies']);

		// Est-ce qu'on restaure une révision ?
		if ($id_version = _request('id_version')) {
			include_spip('inc/revisions');
			$old = recuperer_version($id_formulaire, 'formulaire', $id_version);
			$saisies = formidable_deserialize($old['saisies']);
		}
		if (!is_array($saisies)) {
			$saisies = [];
		}
		$contexte['_saisies'] = $saisies;
		$contexte['id'] = $id_formulaire;
		$contexte['saisie_id'] = "formidable_$id_formulaire";

		// Les options globales que l'on permet de configurer pour le contexte de Formidables
		$contexte['_options_globales'] = [
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'avant_apres',
					'label' => _T('formidable:editer_globales_debut_fin_label'),
				],
				'saisies' => [
					[
						'saisie' => 'textarea',
						'options' => [
							'nom' => 'inserer_debut',
							'label' => _T('formidable:editer_globales_inserer_debut_label'),
							'inserer_barre' => 'edition',
						]
					],
					[
						'saisie' => 'textarea',
						'options' => [
							'nom' => 'inserer_fin',
							'label' => _T('formidable:editer_globales_inserer_fin_label'),
							'inserer_barre' => 'edition',
						]
					]
				]
			],
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'submit',
					'label' => _T('formidable:editer_globales_submit_label')
				],
				'saisies' => [
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'texte_submit',
							'label' => _T('formidable:editer_globales_texte_submit_label'),
						],
					],
					[
						'saisie' => 'textarea',
						'options' => [
							'nom' => 'afficher_si_submit',
							'explication' => _T('saisies:option_afficher_si_explication'),
							'label' => _T('formidable:editer_globales_afficher_si_submit_label'),
							'rows' => '10',
						],
						'verifier' => [
							'type' => 'afficher_si',
						]
					]
				]
			],
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'etapes',
					'label' => _T('formidable:editer_globales_etapes_label')
				],
				'saisies' => [
					[
						'saisie' => 'case',
						'options' => [
							'nom' => 'etapes_activer',
							'label_case' => _T('formidable:editer_globales_etapes_activer_label_case'),
							'conteneur_class' => 'pleine_largeur',
							'explication' => _T('formidable:editer_globales_etapes_activer_explication'),
						],
					],
					[
						'saisie' => 'radio',
						'options' => [
							'nom' => 'etapes_presentation',
							'label' => _T('formidable:editer_globales_etapes_presentation_label'),
							'afficher_si' => '@etapes_activer@',
							'afficher_si_avec_post' => true,
							'data' => [
								'defaut' => _T('formidable:editer_globales_etapes_presentation_defaut_label'),
								'courante' => _T('formidable:editer_globales_etapes_presentation_courante_label'),
							],
							'defaut' => 'defaut',
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'etapes_suivant',
							'label' => _T('formidable:editer_globales_etapes_suivant_label'),
							'afficher_si' => '@etapes_activer@',
							'afficher_si_avec_post' => true,
						],
					],
					[
						'saisie' => 'input',
						'options' => [
							'nom' => 'etapes_precedent',
							'label' => _T('formidable:editer_globales_etapes_precedent_label'),
							'afficher_si' => '@etapes_activer@',
							'afficher_si_avec_post' => true,
						],
					],
					[
						'saisie' => 'case',
						'options' => [
							'nom' => 'etapes_precedent_suivant_titrer',
							'label_case' => _T('formidable:editer_globales_etapes_precedent_suivant_titrer_label'),
							'afficher_si' => '@etapes_activer@',
							'afficher_si_avec_post' => true,
							'conteneur_class' => 'pleine_largeur'
						]
					],
					[
						'saisie' => 'case',
						'options' => [
							'nom' => 'etapes_ignorer_recapitulatif',
							'label_case' => _T('formidable:editer_globales_etapes_ignorer_recapitulatif_label_case'),
							'afficher_si' => '@etapes_activer@',
							'afficher_si_avec_post' => true,
							'conteneur_class' => 'pleine_largeur',
						],
					],
				]
			],
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'previsualisation',
					'label' => _T('formidable:editer_globales_previsualisation_label'),
					'afficher_si' => '!@etapes_activer@',
				],
				'saisies' => [
					[
						'saisie' => 'radio',
						'options' => [
							'nom' => 'previsualisation_mode',
							'label' => _T('formidable:editer_globales_previsualisation_mode_label'),
							'data' => [
								'' => _T('formidable:editer_globales_previsualisation_mode_nope_label'),
								'dessus' => _T('formidable:editer_globales_previsualisation_mode_dessus_label'),
								'etape' => _T('formidable:editer_globales_previsualisation_mode_etape_label'),
							],
							'defaut' => '',
						]
					]
				]
			],
			[
				'saisie' => 'fieldset',
				'options' => [
					'nom' => 'technique',
					'label' => _T('formidable:editer_globales_technique_label')
				],
				'saisies' => [
					[
					'saisie' => 'case',
					'options' => [
						'nom' => 'verifier_valeurs_acceptables',
						'label_case' => _T('saisies:verifier_valeurs_acceptables_label'),
						'conteneur_class' => 'pleine_largeur',
						'explication' => _T('saisies:verifier_valeurs_acceptables_explication'),
					]
					],
				]
			]
		];
	}

	return $contexte;
}

function formulaires_editer_formulaire_champs_verifier($id_formulaire, $redirect = '') {
	include_spip('inc/saisies');
	$erreurs = [];
	include_spip('formidable_fonctions');
	// Si c'est pas une confirmation ni une annulation, ni un revert
	if (
		!_request('enregistrer_confirmation')
		&& !($annulation = _request('annulation'))
		&& !_request('revert')
	) {
		// On récupère le formulaire dans la session
		$saisies_nouvelles = session_get("constructeur_formulaire_formidable_$id_formulaire");
		$md5_precedent_formulaire_initial = session_get("constructeur_formulaire_formidable_$id_formulaire" . '_md5_formulaire_initial');

		// On récupère les anciennes saisies
		$saisies_anciennes = sql_getfetsel(
			'saisies',
			'spip_formulaires',
			'id_formulaire = ' . $id_formulaire
		);
		if (!$saisies_anciennes) {
			return $erreurs;
		}
		// On vérifie que les saisies en bases n'ont pas été modifiés depuis le début de la modification du formulaire
		// Si tel est le cas, on demande de recommencer la modif du formulaire, avec la saisie en base
		// Ne pas le faire si on est en train de restaurer une vieille version, puisque dans ce cas ce qui compte sera bien sur la veille version qu'on veut restaurer, et pas la version plus récente en base:)
		// Attention à s'assurer que tout les elements du tableau soit bien soit des tableaux, soit un string
		// En effet, le md5 du formulaire_initial est calculé à partir de ce qui est passé au squelette
		// Or dès qu'une valeur est passée à un squelette, elle est changé en string, à cause du mode de compilation (?)
		$saisies_anciennes = formidable_deserialize($saisies_anciennes);
		$saisies_anciennes_str = $saisies_anciennes;
		array_walk_recursive($saisies_anciennes_str, 'formidable_array_walk_recursive_strval');
		$md5_saisies_anciennes = md5(serialize($saisies_anciennes_str));
		if (
			$md5_precedent_formulaire_initial
			&& $md5_precedent_formulaire_initial != $md5_saisies_anciennes
			&& !_request('id_version')
		) {
			session_set("constructeur_formulaire_formidable_$id_formulaire", $saisies_anciennes);
			session_set("constructeur_formulaire_formidable_$id_formulaire" . '_md5_formulaire_initial', $md5_saisies_anciennes);
			$erreurs['message_erreur'] = _T('formidable:erreur_saisies_modifiees_parallele');
			$erreurs['saisies_modifiees_parallele'] = _T('formidable:erreur_saisies_modifiees_parallele');
			return $erreurs;
		}

		// On compare les anciennes saisies aux nouvelles
		$comparaison = saisies_comparer($saisies_anciennes, $saisies_nouvelles);

		// S'il y a des suppressions, on demande confirmation avec attention
		if ($comparaison['supprimees']) {
			$erreurs['message_erreur'] = _T('saisies:construire_attention_supprime');
		}

		//On vérifie s'il y a pas d'incohérence dans les afficher_si
		$erreurs_afficher_si = saisies_verifier_coherence_afficher_si($saisies_nouvelles ?? []);
		if ($erreurs_afficher_si) {
			$erreurs['saisies_incoherence_afficher_si'] = true;
			if ($erreurs['message_erreur'] ?? '') {
				$erreurs['message_erreur'] .= '<br />' . $erreurs_afficher_si;
			} else {
				$erreurs['message_erreur'] = $erreurs_afficher_si;
			}
		}
	} elseif ($annulation ?? '') {
		// Si on annule on génère une erreur bidon juste pour réafficher le formulaire
		$erreurs['pouetpouet'] = true;
		$erreurs['message_erreur'] = '';
	}



	return $erreurs;
}

function formulaires_editer_formulaire_champs_traiter($id_formulaire, $redirect = '') {
	include_spip('inc/saisies');
	include_spip('inc/formidable');
	$retours = [];
	$id_formulaire = intval($id_formulaire);
	include_spip('formidable_fonctions');

	if (_request('revert')) {
		session_set("constructeur_formulaire_formidable_$id_formulaire");
		$retours = ['editable' => true];
	}

	if (_request('enregistrer') || _request('enregistrer_confirmation')) {
		// On récupère le formulaire dans la session
		$saisies_nouvelles = session_get("constructeur_formulaire_formidable_$id_formulaire");

		// On récupère les anciennes saisies
		$saisies_anciennes = formidable_deserialize(sql_getfetsel(
			'saisies',
			'spip_formulaires',
			'id_formulaire = ' . $id_formulaire
		));

		// On envoie les nouvelles dans la table
		include_spip('action/editer_objet');
		$err = objet_modifier('formulaire', $id_formulaire, ['saisies' => formidable_serialize(saisies_identifier($saisies_nouvelles))]);

		// Si c'est bon on appelle d'éventuelles fonctions d'update des traitements
		// On reinitialise aussi les sessions
		// puis on renvoie vers la config des traitements
		if (!$err) {
			session_set("constructeur_formulaire_formidable_$id_formulaire");
			session_set("constructeur_formulaire_formidable_$id_formulaire" . '_md5_formulaire_initial');
			// On va chercher les traitements
			$traitements = formidable_deserialize(sql_getfetsel(
				'traitements',
				'spip_formulaires',
				'id_formulaire = ' . $id_formulaire
			));

			// Pour chaque traitements on regarde s'i y a une fonction d'update
			if (is_array($traitements)) {
				foreach ($traitements as $type_traitement => $traitement) {
					if ($update = charger_fonction('update', "traiter/$type_traitement", true)) {
						$update($id_formulaire, $traitement, $saisies_anciennes, $saisies_nouvelles);
					}
				}
			}
			// On redirige vers la config suivante
			$retours['redirect'] = parametre_url(
				parametre_url(
					parametre_url(
						generer_url_ecrire('formulaire_edit'),
						'id_formulaire',
						$id_formulaire
					),
					'configurer',
					'traitements'
				),
				'avertissement',
				'oui'
			);
			if ($id_version = _request('id_version')) {
				$retours['redirect'] = parametre_url($retours['redirect'], 'id_version', $id_version);
			}
			if ($redirect) {
				$retours['redirect'] = parametre_url($retours['redirect'], 'redirect', $redirect);
			}
		}
	}

	return $retours;
}
