<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/formidable');
include_spip('inc/formidable_fichiers');

function formulaires_editer_formulaire_traitements_saisies($id_formulaire, $redirect) {

	$traitements_disponibles = traitements_lister_disponibles();
	$traitements_disponibles = formulaires_editer_formulaire_traitements_generer_libelles($traitements_disponibles);
	$saisie_traitements_choisis =	[
		'saisie' => 'checkbox',
		'options' => [
			'nom' => 'traitements_choisis',
			'label' => '<:formidable:traitements_choisis:>',
			'data' => [],
		]
	];


	// Calcul des traitements avec option pour le afficher_si du fieldset `options_des_traitements`
	$traitements_disponibles_avec_options = array_keys(array_filter(
		$traitements_disponibles,
		function ($t) {
			return isset($t['options']);
		}
	));
	$afficher_si_fieldset_options = implode(
		' || ',
		array_map(
			function ($t) {
				return "@traitements_choisis@ IN '$t'";
			},
			$traitements_disponibles_avec_options
		)
	);
	$saisie_options_traitements = [
		'saisie' => 'fieldset',
		'options' => [
			'nom' => 'options_traitements',
			'label' => '<:formidable:options_traitements:>',
			'afficher_si' => $afficher_si_fieldset_options
		],
		'saisies' => [],
	];

	$saisie_pas_de_traitement = [
		'saisie' => 'explication',
		'options' => [
			'nom' => 'explication_pas_de_traitement',
			'afficher_si' => '@traitements_choisis@:TOTAL == 0',
			'texte' => '<:formidable:choisir_traitement_explication:>',
			'alerte_role' => 'alert',
			'alerte_type' => 'info',
		]
	];

	$configurer_traitements = [$saisie_pas_de_traitement, &$saisie_traitements_choisis, &$saisie_options_traitements];

	foreach ($traitements_disponibles as $type_traitement => $traitement) {
		$saisie_traitements_choisis['options']['data'][$type_traitement] = $traitement['libelle'];
		if (isset($traitement['options'])) {
			$saisie_options_traitements['saisies'][] = [
				'saisie' => 'fieldset',
				'options' => [
					'nom' => "options_$type_traitement",
					'label' => $traitement['titre'],
					'class' => "$type_traitement options_traiter",
					'afficher_si' => "@traitements_choisis@ IN '$type_traitement'",
					'onglet' => 'on',
					'onglet_vertical' => 'on',
				],
				'saisies' => saisies_transformer_noms($traitement['options'], '/^.*$/', "traitements[$type_traitement][\\0]")
			];
		}
	}

	$configurer_traitements = saisies_transformer_option($configurer_traitements, 'conteneur_class', '#(.*)#', '\1 pleine_largeur');

	// Ajouter une vérification `formidable_coherence_arobase` sur chaque saisie finale
	$configurer_traitements = saisies_mapper_verifier($configurer_traitements, function ($verifier, $saisie, $id_formulaire) {
		$verifier[] = [
			'type' => 'formidable_coherence_arobase',
			'options' => [
				'id_formulaire' => $id_formulaire
				]
		];
		return $verifier;
	}, [$id_formulaire]);

	$configurer_traitements[] = [
		'saisie' => 'hidden',
		'options' => [
			'nom' => 'id_formulaire',
			'defaut' => $id_formulaire,
		]
	];

	$configurer_traitements['options']['inserer_fin'] = "
		<script type='text/javascript'>
			jQuery(function() {
				$('.editer_traitements_choisis > legend').removeClass('label').removeClass('editer-label');
			});
		</script>
		<style>
			input.checkbox+label {font-weight: normal;}
			input.checkbox+label strong {font-weight: normal;}
			input.checkbox:checked+label {font-weight: bold;}
			input.checkbox:checked+label strong {font-weight: bold;}
			input.radio+label {font-weight: normal;}
			input.radio+label strong {font-weight: normal;}
			input.radio:checked+label {font-weight: bold;}
			input.radio:checked+label strong {font-weight: bold;}

			.formulaire_editer_formulaire_traitements	.editer_traitements_choisis div.choix + .choix {
				padding-top: 0.5ex;
			}
			.formulaire_editer_formulaire_traitements .editer_traitements_choisis .choix > label {
				margin-inline-end: 0px;
			}

			.formulaire_editer_formulaire_traitements .necessite {
				display: block;
				font-style: italic;
			}
			.editer_traitements_choisis .necessite {
				color: grey;
			}
			.formulaire_editer_formulaire_traitements .necessite {
				margin-inline-start: 1.5rem;
			}
			.formulaire_editer_formulaire_traitements .necessite ul {
				margin-bottom: 0px;
			}
			.formulaire_editer_formulaire_traitements .necessite li {
				margin-inline-start: 2.5rem;
				list-style-type: square;
			}
		</style>
	";
	return $configurer_traitements;
}

function formulaires_editer_formulaire_traitements_charger($id_formulaire, $redirect = '') {
	$contexte = [];
	$id_formulaire = intval($id_formulaire);
	include_spip('formidable_fonctions');

	// On teste si le formulaire existe
	if (
		$id_formulaire
		&& ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $id_formulaire))
		&& autoriser('editer', 'formulaire', $id_formulaire)
	) {
		$traitements = formidable_deserialize($formulaire['traitements']);
		if ($id_version = _request('id_version')) {
			include_spip('inc/revisions');
			$old = recuperer_version($id_formulaire, 'formulaire', $id_version);
			$traitements = formidable_deserialize($old['traitements']);
		}
		$saisies = formidable_deserialize($formulaire['saisies']);
		if (!is_array($traitements)) {
			$traitements = [];
		}
		if (!is_array($saisies)) {
			$saisies = [];
		}
		$contexte['traitements'] = $traitements;
		$contexte['traitements_choisis'] = array_keys($traitements);
		$contexte['formulaire'] = _T_ou_typo($saisies, 'multi');


		// Si on demande un avertissement et qu'il y a déjà des traitements de configurés
		if (_request('avertissement') == 'oui') {
			$contexte['message_ok'] = $traitements ? _T('formidable:traitements_avertissement_modification') : _T('formidable:traitements_avertissement_creation');
		}
	} else {
		$contexte['editable'] = false;
	}
	// On enlève l'éventuel avertissement pour le prochain envoi
	$contexte['action'] = parametre_url(self(), 'avertissement', '');

	return $contexte;
}

function formulaires_editer_formulaire_traitements_verifier_post_saisies($id_formulaire, $redirect = '') {
	include_spip('inc/saisies');
	$erreurs = [];
	$traitements_disponibles = traitements_lister_disponibles();

	// On regarde quels traitements sont demandés
	$traitements_choisis = _request('traitements_choisis');
	if (!$traitements_choisis) {
		$traitements_choisis = [];
	}
	$erreur_necessite = formulaires_editer_formulaire_traitements_verifier_necessite($traitements_choisis, $traitements_disponibles);
	$erreur_necessite = formulaires_editer_formulaire_traitements_generer_libelles($erreur_necessite);
	$erreur_necessite = formulaires_editer_formulaire_traitements_formater_erreur_necessite($erreur_necessite);
	if ($erreur_necessite) {
		$erreurs['message_erreur'] = $erreur_necessite;
	}

	if (is_array($traitements_choisis)) {
		foreach ($traitements_choisis as $type_traitement) {
			if (!isset($traitements_disponibles[$type_traitement]['options'])) {
				continue;
			}
		}
	}
	return $erreurs;
}

function formulaires_editer_formulaire_traitements_traiter($id_formulaire, $redirect = '') {
	$retours = [];
	$id_formulaire = intval($id_formulaire);
	include_spip('inc/formidable');

	// On récupère tout le tableau des options de traitement
	$traitements = _request('traitements');
	// Et tout les traitemenrs choisis
	$traitements_choisis = _request('traitements_choisis');

	if (!$traitements_choisis) {
		$traitements_choisis = [];
	}
	$traitements_choisis = array_flip($traitements_choisis);

	// Parmis toutes les options passées, ne prendre que celle des traitements choisis
	$traitements = array_intersect_key($traitements, $traitements_choisis);
	// On gère le cas des traitements sans options
	$traitements_sans_option = array_map(function ($i) {
		return [];
	}, array_diff_key($traitements_choisis, $traitements));//Même si pas d'option, on fait un pseudo tableau d'option
	$traitements = array_merge($traitements, $traitements_sans_option);

	// Si besoin, on crée un dossier pour stocker les fichiers
	$erreur_creation_dossier = formidable_creer_dossier_formulaire($id_formulaire);
	if ($erreur_creation_dossier) {
		$retours['message_erreur'] = $erreur_creation_dossier;
	}
	// Et on l'enregistre tel quel
	include_spip('action/editer_objet');
	$err = objet_modifier('formulaire', $id_formulaire, ['traitements' => formidable_serialize($traitements)]);

	// On va sur la page de visualisation quand c'est fini
	if (!$err) {
		if ($redirect) {
			$retours['redirect'] = $redirect;
		} else {
			$retours['redirect'] = parametre_url(generer_url_ecrire('formulaire'), 'id_formulaire', $id_formulaire);
		}
	} else {
		$retours['editable'] = true;
		$retours['message_erreur'] = _T('formidable:erreur_base');
	}

	return $retours;
}


/**
 * Complète les libellés des traitements avec les dépendances
 * @param array $traitements description des traitements
 * @return array $traitements, avec pour chaque traitement un champ libellé
 **/
function formulaires_editer_formulaire_traitements_generer_libelles(array $traitements): array {
	foreach ($traitements as $traitement => &$description) {
		$necessite = $description['necessite'] ?? [];
		if (!$necessite) {
			$description['libelle'] = $description['description'] ?? '';
			continue;
		}

		$libelle = $description['description'] ?? '';
		$data_afficher_si = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite($necessite);
		$necessite = array_map(function ($x) use ($traitements) {
			$afficher_si = formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite([$x]);
			return '<li data-afficher_si="'. $afficher_si .'">' . $traitements[$x]['description'] . '</li>';
		}, $necessite);

		$libelle .= ' <div class="necessite" data-afficher_si="' . $data_afficher_si .'">' . _T('formidable:editer_formulaire_traitements_necessite') . '<ul>' . implode('', $necessite) . '</ul></div>';
		$description['libelle'] = $libelle;
	}
	return $traitements;
}

/**
 * Prend une liste de necessite pour un traitement
 * et génère un attribut data-afficher_si
 * plus simple que de passer par un parsage d'une syntaxe afficher_si + l'imitaton d'une saisie pas encore définie
 *
 * @param array $necessite
 * @return string
 **/
function formulaires_editer_formulaire_traitements_generer_afficher_si_from_necessite(array $necessite): string {
	include_spip('saisies_afficher_si_js/defaut');
	foreach ($necessite as &$n) {
		$data = [
			'champ' => 'traitements_choisis',
			'operateur' => 'IN',
			'valeur' => $n,
			'negation' => '!',
		];
		$n = saisies_afficher_si_js_defaut($data, []);
	}
	return str_replace('"', '&quot;', implode(' || ', $necessite));
}

/**
 * Prend les traitements choisis,
 * filtre les `necessite` pour n'envoyer que ce qui peut être utile à l'affichage d'erreur
 * @param array $traitements_choisis
 * @param array $traitements_disponibles
 * @return array description des traitements:
 *	- si coché, le `'necessite'`ne sont plus que ce qui n'a pas encore été coché
 *	- si pas coché, le `necessite` est vide
 **/
function formulaires_editer_formulaire_traitements_verifier_necessite(array $traitements_choisis, array $traitements_disponibles): array {
	foreach ($traitements_disponibles as $t => &$traitement) {
		if (in_array($t, $traitements_choisis)) {
			$necessite = $traitement['necessite'] ?? [];
			$traitement['necessite'] = array_diff($necessite, $traitements_choisis);
		} else {
			$traitement['necessite'] = [];
		}
	}
	return $traitements_disponibles;
}

/**
 * Formate les erreurs de traitement choisis qui n'ont pas leur nécessite
 * @param array $erreur_necessite un tableau de tous les traitements, avec ceux qui ont des nécessites
 * @return string un message d'erreur
 **/
function formulaires_editer_formulaire_traitements_formater_erreur_necessite(array $erreur_necessite): string {
	$erreur = '';
	foreach ($erreur_necessite as $l) {
		if ($l['necessite'] ?? []) {
			$erreur .=  '<div>' . $l['libelle'] . '</div>';
		}
	}
	return $erreur;
}
