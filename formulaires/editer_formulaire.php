<?php

// Sécurité
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/saisies');
include_spip('action/editer_liens');
include_spip('inc/config');



function formulaires_editer_formulaire_saisies(int $id_formulaire): array {
	$saisies = [
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'titre',
				'obligatoire' => 'on',
				'label' => '<:formidable:editer_titre:>'
			]
		],
		[
			'saisie' => 'input',
			'options' =>  [
				'nom' => 'identifiant',
				'obligatoire' => 'on',
				'label' =>  '<:formidable:editer_identifiant:>',
				'explication' => '<:formidable:editer_identifiant_explication:>',
			],
			'verifier' => [
				'type' => 'regex',
				'options' => [
					'modele' => '/^[\w]+$/'
				]
			],
		],
		[
			'saisie' => 'textarea',
			'options' =>  [
				'nom' => 'message_retour',
				'label' =>  '<:formidable:editer_message_ok:>',
				'explication' => '<:formidable:editer_message_ok_explication:>',
				'conteneur_class' => 'pleine_largeur',
				'rows' => '5',
				'inserer_barre' => 'edition',
				'previsualisation' => 'oui',
			],
			'verifier' => [
				[
					'type' => 'formidable_coherence_arobase',
					'options' => [
						'id_formulaire' => $id_formulaire
					]
				]
			]
		],
		[
			'saisie' => 'textarea',
			'options' =>  [
				'nom' => 'descriptif',
				'label' =>  '<:formidable:editer_descriptif:>',
				'explication' => '<:formidable:editer_descriptif_explication:>',
				'rows' => '5',
			]
		],
		[
			'saisie' => 'input',
			'options' =>  [
				'nom' => 'css',
				'label' =>  '<:formidable:editer_css:>',
				'rows' => '5',
			]
		],
		[
			'saisie' => 'selection',
			'options' =>  [
				'nom' => 'apres',
				'label' =>  '<:formidable:editer_apres_label:>',
				'explication' =>  '<:formidable:editer_apres_explication:>',
				'data' => [
					'formulaire' => '<:formidable:editer_apres_choix_formulaire:>',
					'valeurs' => '<:formidable:editer_apres_choix_valeurs:>',
					'stats' => '<:formidable:editer_apres_choix_stats:>',
					'rien' => '<:formidable:editer_apres_choix_rien:>',
					'redirige' => '<:formidable:editer_apres_choix_redirige:>'
				],
				'cacher_option_intro' => 'on',
				'defaut' => 'formulaire',
			]
		],
		[
			'saisie' => 'input',
			'options' => [
				'nom' => 'url_redirect',
				'label' => '<:formidable:editer_redirige_url:>',
				'obligatoire' => 'on',
				'afficher_si' => '@apres@ == "redirige"',
			],
			'verifier' => [
				'type' => 'url',
				'options' => [
					'type_protocole' => 'tous',
				]
			]
		],


	];
	return $saisies;
}

/**
 * Chargement du formulaire d'édition de l'objet formulaire
 * @param int $id_formulaire
 * @param string $nouveau (oui si c'est le cas)
 * @param string $associer_objets : objet à associer le cas échéant
 * @param string $redirect : url de redirection
**/
function formulaires_editer_formulaire_charger(int $id_formulaire, string $nouveau, ?string $associer_objets = '', ?string $redirect = '') {
	$id_formulaire = intval($nouveau ? 0 : $id_formulaire);
	include_spip('inc/editer');

	// Est-ce qu'on a le droit ?
	if (!autoriser('editer', 'formulaire', $id_formulaire)) {
		$contexte = [];
		$contexte['editable'] = false;
		$contexte['message_erreur'] = _T('formidable:erreur_autorisation');
	} else {
		$contexte = formulaires_editer_objet_charger('formulaire', $id_formulaire, 0, 0, '', '');
	}
	unset($contexte['id_formulaire']);

	return $contexte;
}

/**
 * Vérification du formulaire d'édition de l'objet formulaire
 * @param int $id_formulaire
 * @param string $nouveau (oui si c'est le cas)
 * @param string $associer_objet : objet à associer le cas échéant
 * @param string $redirect : url de redirection
**/
function formulaires_editer_formulaire_verifier(int $id_formulaire, string $nouveau, ?string $associer_objet = '', ?string $redirect = '') {
	$id_formulaire = intval($nouveau ? 0 : $id_formulaire);
	$erreurs = [];

	include_spip('inc/editer');
	if (!isset($erreurs['identifiant'])) {
		$identifiant = _request('identifiant');
		if (sql_getfetsel('id_formulaire', 'spip_formulaires', 'identifiant = ' . sql_quote($identifiant) . ' AND id_formulaire != ' . intval($id_formulaire))) {
			// unicite de l'identifiant
			$erreurs['identifiant'] = _T('formidable:erreur_identifiant');
		}
	}

	return $erreurs;
}

/**
 * Traitement du formulaire d'édition de l'objet formulaire
 * @param int $id_formulaire
 * @param string $nouveau (oui si c'est le cas)
 * @param string $associer_objet : objet à associer le cas échéant
 * @param string $redirect : url de redirection
**/
function formulaires_editer_formulaire_traiter(int $id_formulaire, string $nouveau, ?string $associer_objet = '', ?string $redirect = '') {
	include_spip('inc/editer');
	$id_formulaire = $id_formulaire ? $id_formulaire : $nouveau;
	$retours = formulaires_editer_objet_traiter('formulaire', $id_formulaire);

	// S'il n'y a pas d'erreur et que le formulaire est bien là
	if (empty($retours['message_erreur']) && $retours['id_formulaire'] > 0) {
		// Si c'était un nouveau on reste sur l'édition
		if (!intval($id_formulaire) && $nouveau === 'oui') {
			// Tout a fonctionné. En fonction de la config, on attribue l'auteur courant
			$auteurs = lire_config('formidable/auteur');
			if ($auteurs == 'on') {
				if ($id_auteur = session_get('id_auteur')) {
					// association (par défaut) du formulaire et de l'auteur courant
					objet_associer(['formulaire' => $retours['id_formulaire']], ['auteur' => $id_auteur]);
				}
			}
			$retours['redirect'] = parametre_url(generer_url_ecrire('formulaire_edit'), 'id_formulaire', $retours['id_formulaire'], '&');
			$retours['redirect'] = parametre_url($retours['redirect'], 'redirect', $redirect, '&');
		} else {
			// Sinon on redirige vers la page de visualisation
			$retours['redirect'] = parametre_url(generer_url_ecrire('formulaire'), 'id_formulaire', $retours['id_formulaire'], '&');
		}
	}

	// Associer le cas échéant à l'objet
	if ($associer_objet) {
		[$objet, $id_objet] = explode('|', $associer_objet);
	} else {
		$objet = '';
		$id_objet = '';
	}
	if (
		$objet
		&& $id_objet
		&& autoriser('modifier', $objet, $id_objet)
		&& autoriser('associerformulaires', $objet, $id_objet)
	) {
		include_spip('action/editer_liens');
		objet_associer(['spip_formulaires' => [$retours['id_formulaire']]], [$objet => [$id_objet]]);
	}

	return $retours;
}
