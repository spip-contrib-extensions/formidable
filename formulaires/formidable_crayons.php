<?php

/**
 * Déclaration des crayons "comme si c'était du cvt"
 * pour CVT Upload
 *
 * @package SPIP\Formidable\Crayons
 **/

/**
 * Déclarons un crayonnage de champ fichier comme si c'était un CVT
 * @param string $nom_crayons
 * @return array
**/
function formulaires_formidable_crayons_fichiers(string $nom_crayons): array {
	return [$nom_crayons];
}
