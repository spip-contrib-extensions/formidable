<?php

/**
* Gestion de l'affichage et traitement d'un formulaire Formidable
*
* @package SPIP\Formidable\Formulaires
**/


include_spip('inc/formidable');
include_spip('inc/formidable_fichiers');
include_spip('inc/saisies');
include_spip('base/abstract_sql');
include_spip('inc/autoriser');
include_spip('plugins/installer');

function formidable_id_formulaire($id) {
	// on utilise une static pour etre sur que si l'appel dans verifier() passe, celui dans traiter() passera aussi
	// meme si entre temps on perds la base
	static $id_formulaires = [];
	if (isset($id_formulaires[$id])) {
		return $id_formulaires[$id];
	}

	if (is_numeric($id)) {
		$where = 'id_formulaire = ' . intval($id);
	} elseif (is_string($id)) {
		$where = 'identifiant = ' . sql_quote($id);
	} else {
		return 0;
	}

	$id_formulaire = intval(sql_getfetsel('id_formulaire', 'spip_formulaires', $where));

	if (
		$id_formulaire
		&& !test_espace_prive()
		&& !objet_test_si_publie('formulaire', $id_formulaire)
	) {
		return $id_formulaires[$id] = 0;
	}

	return $id_formulaires[$id] = $id_formulaire;
}

/**
* Déclaration des saisies du formulaire à l'API Saisies.
*
* @param int|string $id
*     Identifiant numerique ou textuel du formulaire formidable
* @param array|string $valeurs
*     Valeurs par défauts passées au contexte du formulaire
*     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
*     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
* @param int|array $options_appel options diverses à l'appel, voir la doc de `formulaires_formidable_charger_dist()` pour une liste à jour
* @param bool|string $deprecated_url_redirect
* @param bool $deprecated_forcer_modif
* @return array
*     Tableau des saisies
**/
function formulaires_formidable_saisies_dist($id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false) {
	$saisies = [];
	$formulaire = [];
	include_spip('formidable_fonctions');
	if (
		($id_formulaire = formidable_id_formulaire($id))
		&& ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . intval($id_formulaire)))
	) {
		$saisies = formidable_deserialize($formulaire['saisies']);
		if (!$saisies) {
			$saisies = [];
		}

		// Si on est en train de réafficher les valeurs postées,
		// ne pas afficher les saisies hidden
		if (
			$formulaire['apres'] === 'valeurs'
			&& _request('formidable_afficher_apres') === 'valeurs'
			&& _request('formidable_traiter_ok') == true
		) {
			$champs_hidden = saisies_lister_avec_type($saisies, 'hidden');
			foreach ($champs_hidden as $champ => $desc) {
				$saisies = saisies_supprimer($saisies, $champ);
			}
		}
	}
	$valeurs = formulaires_formidable_normaliser_valeurs($valeurs);

	$saisies = formulaires_formidable_saisies_set_options($saisies, $formulaire, $valeurs, $options_appel);


	return $saisies;
}

/**
 * Configure les options globales de saisies pour intégrer les spécificités de formidable
 * @param array $saisies les saisies deja existantes
 * @param array $formulaire le contenu du formulaire en base
 * @param array $valeurs les valeurs passées lors de l'appel
 * @param array $options_appel les options à l'appel du formulaire
 * @return $options modifiées
**/
function formulaires_formidable_saisies_set_options(array $saisies, array $formulaire, array $valeurs, array $options_appel): array {
	$id_formulaire = $formulaire['id_formulaire'];

	// Initialiser au cas où
	if (!isset($saisies['options'])) {
		$saisies['options'] = [];
	}

	$options = &$saisies['options'];

	$options['ajax'] = boolval(!($options_appel['no_ajax'] ?? false));

	// Toutes les classes, et il y a un paquet historiquement
	$options['conteneur_class'] = '';
	$conteneur_class = &$options['conteneur_class'];
	$conteneur_class .= 'formulaire_formidable_' . $formulaire['id_formulaire'];
	$conteneur_class .= ' formulaire_formidable_' . $formulaire['identifiant'];
	$conteneur_class .= ' formulaire_formidable-' . $formulaire['id_formulaire'];
	$conteneur_class .= ' ' . $formulaire['css'];

	// Id, c'est assez simple, ouf !
	$options['conteneur_id'] = 'formulaire_formidable_' . $formulaire['identifiant'];

	// Début et fin en propre
	$options['inserer_debut'] = propre($options['inserer_debut'] ?? '');
	$options['inserer_fin'] = propre($options['inserer_fin'] ?? '');

	// Les boutons, pour celleux qui veulent surcharger formulaire par formulaire (?)
	$options['squelette_boutons'] = 'formulaires/inc-formidable-boutons';

	return $saisies;
}
/**
* Chargement du formulaire CVT de Formidable.
*
* Genere le formulaire dont l'identifiant (numerique ou texte est indique)
*
* @param int|string $id
*     Identifiant numerique ou textuel du formulaire formidable
* @param array|string $valeurs
*     Valeurs par défauts passées au contexte du formulaire
*     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
*     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
* @param int|array $options_appel options diverses à l'appel
* @param bool|string $deprecated_url_redirect
* @param bool $deprecated_forcer_modif
* @return array|void
*     Contexte envoyé au squelette HTML du formulaire.
**/
function formulaires_formidable_charger_dist($id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false) {
	$contexte = [];
	$formulaire = [];
	include_spip('formidable_fonctions');


	$options_appel = formulaires_formidable_normaliser_options_appel($options_appel, $deprecated_url_redirect, $deprecated_forcer_modif);

	$contexte['_no_ajax'] = $options_appel['no_ajax'];

	$id_formulaires_reponse = $options_appel['id_formulaires_reponse'];

	// On peut donner soit un id soit un identifiant
	if (!$id_formulaire = formidable_id_formulaire($id)) {
		return;
	}

	// On cherche si le formulaire existe
	if ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . intval($id_formulaire))) {
		// On ajoute un point d'entrée avec les infos de ce formulaire
		// pour d'eventuels plugins qui en ont l'utilité
		$contexte['_formidable'] = $formulaire;
		// Classes CSS
		$contexte['_css'] = $formulaire['css'];

		// Est-ce que la personne a le droit de répondre ?
		if (autoriser('repondre', 'formulaire', $formulaire['id_formulaire'], null, ['formulaire' => $formulaire])) {
			$traitements = formidable_deserialize($formulaire['traitements']);

			$contexte['mechantrobot'] = '';

			$contexte['id'] = $formulaire['id_formulaire']; // Gardé pour compat historique pour les pipelines qui s'appuieraient dessus, mais préférer `_formidable/id_formulaire`
			$contexte['id_formulaire'] = $formulaire['id_formulaire']; // Gardé pour compat historique pour les gens qui veulent surcharger les boutons formulaire par formulaire mais préférer `_formidable/id_formulaire`
			$contexte['_hidden'] = '<input type="hidden" name="id_formulaire" value="' . $formulaire['id_formulaire'] . '"/>';

			// S'il y a des valeurs par défaut dans l'appel, alors on pré-remplit
			if ($valeurs) {
				$valeurs = formulaires_formidable_normaliser_valeurs($valeurs);
				// On écrase le contexte avec les valeurs données depuis l'appel
				$contexte = array_merge($contexte, $valeurs);
			}

			//trouver la réponse à éditer
			$options_enregistrement = isset($traitements['enregistrement']) ? $traitements['enregistrement'] : null;
			$id_formulaires_reponse = formidable_trouver_reponse_a_editer($formulaire['id_formulaire'], $id_formulaires_reponse, $options_enregistrement);

			// adapter le contexte en conséquence
			$contexte = formidable_definir_contexte_avec_reponse($contexte, $id_formulaires_reponse, $ok);
			if ($ok == false) {
				$contexte['editable'] = false;
				$contexte['message_erreur'] = _T(
					'formidable:traiter_enregistrement_erreur_edition_reponse_inexistante'
				);
			}
		} else {
			$contexte['editable'] = false;
			// le formulaire a déjà été répondu.
			// peut être faut il afficher les statistiques des réponses
			if ($formulaire['apres'] == 'stats') {
				// Nous sommes face à un sondage auquel on a déjà répondu !
				// On remplace complètement l'affichage du formulaire
				// par un affichage du résultat de sondage !
				$contexte['_remplacer_formulaire'] = recuperer_fond('modeles/formulaire_analyse', [
					'id_formulaire' => $formulaire['id_formulaire'],
				]);
			} else {
				$contexte['message_erreur'] = _T('formidable:traiter_enregistrement_erreur_deja_repondu');
				$contexte['message_erreur_class'] = 'deja_repondu';
			}
		}
	} else {
		$contexte['editable'] = false;
		$contexte['message_erreur'] = _T('formidable:erreur_inexistant');
	}
	if (!isset($contexte['_hidden'])) {
		$contexte['_hidden'] = '';
	}
	$contexte['_hidden'] .= "\n" . '<input type="hidden" name="formidable_afficher_apres" value="' . $formulaire['apres'] . '"/>';

	if ($precharger = _request('_formidable_cvtupload_precharger_fichiers')) {
		$contexte['cvtupload_precharger_fichiers'] = $precharger;
	}
	$contexte['formidable_afficher_apres'] = $formulaire['apres'];
	// Si le formulaire via d'être posté, ne pas preremplir le nouveau formulaire avec les valeurs postées
	if ($formulaire['apres'] === 'formulaire' && _request('formidable_traiter_ok')) {
		foreach (saisies_lister_par_nom(formidable_deserialize($formulaire['saisies'])) as $nom => $valeur)	{
			set_request($nom, null);
		}
		set_request('cvtupload_etapes_files', null);
		set_request('_fichiers', null);
		unset($contexte['cvtupload_precharger_fichiers']);
		$_FILES = [];
		set_request('cvtupload_fichiers_precedents', null);
	}
	return $contexte;
}


/**
* Vérification du formulaire CVT de Formidable.
*
* Les vérifications associées aux saisies individuelles sont effectuées
* directement par le plugin saisies, qui utilise le pipeline `formulaires_verifier`.
* La présente fonction se contente pour l'essentielle de faire les vérifications spécifiques à certains traitements.
* Les vérifications ont lieu après les vérifications de saisies.
* @param int|string $id
*     Identifiant numerique ou textuel du formulaire formidable
* @param array|string $valeurs
*     Valeurs par défauts passées au contexte du formulaire
*     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
*     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
* @param array $options_appel options diverses à l'appel, voir https://contrib.spip.net/Formidable-le-generateur-de-formulaires#3emearg
* @param bool|string $deprecated_url_redirect
* @param bool $deprecated_forcer_modif
* @return array
*     Tableau des erreurs
**/
function formulaires_formidable_verifier_post_saisies_dist($id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false) {
	$erreurs = [];

	$options_appel = formulaires_formidable_normaliser_options_appel($options_appel, $deprecated_url_redirect, $deprecated_forcer_modif);

	include_spip('inc/saisies');
	$saisies = saisies_chercher_formulaire('formidable', [$id, $valeurs, $options_appel, $deprecated_url_redirect, $deprecated_forcer_modif]);

	// Si on n'est pas dans un formulaire à étape, on lance les vérifications des traitements
	if ($saisies && !saisies_lister_par_etapes($saisies)) {
		$erreurs = formulaires_formidable_verifier_traitements($id, $valeurs, $options_appel);
	}


	return $erreurs;
}

/**
* Vérification du formulaire CVT de Formidable mais s'il y a des étapes
* La présente fonction se contente pour l'essentielle de faire les vérifications spécifiques à certains traitements.
* Les vérifications ont lieu après les vérifications de saisies.
* @param int $etape
* @param int|string $id
*     Identifiant numerique ou textuel du formulaire formidable
* @param array|string $valeurs
*     Valeurs par défauts passées au contexte du formulaire
*     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
*     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
* @param array $options_appel options diverses à l'appel, voir https://contrib.spip.net/Formidable-le-generateur-de-formulaires#3emearg
* @param bool|string $deprecated_url_redirect
* @param bool $deprecated_forcer_modif
* @return array
*     Tableau des erreurs
**/
function formulaires_formidable_verifier_etape_post_saisies_dist($etape, $id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = '', $deprecated_forcer_modif = false) {
	$erreurs = [];

	$options_appel = formulaires_formidable_normaliser_options_appel($options_appel = [], $deprecated_url_redirect, $deprecated_forcer_modif);

	include_spip('inc/saisies');
	$saisies = saisies_chercher_formulaire('formidable', [$id, $valeurs, $options_appel, $deprecated_url_redirect, $deprecated_forcer_modif]);

	// On lance les vérifications propres aux traitements à chaque étape, pour avoir les messages d'erreurs à chaque étape
	if ($saisies && ($etapes = saisies_lister_par_etapes($saisies))) {
		$erreurs = formulaires_formidable_verifier_traitements($id, $valeurs, $options_appel, $etapes, $etape);
	}

	return $erreurs;
}

/**
 * Lancer des vérifications propres aux traitements
 *
 * @param int|string $id
 *     Identifiant numerique ou textuel du formulaire formidable
 * @param array|string $valeurs
 *     Valeurs par défauts passées au contexte du formulaire
 *     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
 *     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
 * @param int|array $options_appel : options passées à l'appel du formulaire (pour la doc, voir la doc de `formulaire_formidable_verifier()`)
 * @param array $etapes
 *		 Liste des saisies, ordonnées par étape
 * @param int|null $etape le numéro de l'étape courante
 * @return array
 *     Tableau des erreurs
 */
function formulaires_formidable_verifier_traitements($id, $valeurs = [], $options_appel = [], $etapes = [], $etape = null) {
	$erreurs = [];
	$id_formulaires_reponse = $options_appel['id_formulaires_reponse'] ?? false;
	include_spip('formidable_fonctions');
	$formulaire = [];

	if (
		($id_formulaire = formidable_id_formulaire($id))
		&& ($formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . intval($id_formulaire)))
		&& ($traitements = formidable_deserialize($formulaire['traitements']))
		&& is_array($traitements)
	) {
		$saisies = formidable_deserialize($formulaire['saisies']);

		// Pour chaque traitement choisi, on cherche s'il propose une fonction de vérification propre à ses besoins
		foreach ($traitements as $type_traitement => $options) {
			if ($verifier_traitement = charger_fonction('verifier', "traiter/$type_traitement", true)) {
				$erreurs_traitements = $verifier_traitement(
					[
						'formulaire' => $formulaire,
						'options' => $options,
						'traitements' => $traitements,
						'saisies' => $saisies,
						'id_formulaire' => $formulaire['id_formulaire'],
						'valeurs' => $valeurs,
						'id_formulaires_reponse' => $id_formulaires_reponse,
						'options_appel' => $options_appel,
						'etapes' => $etapes,
						'etape' => $etape
					],
					$erreurs
				);
				$erreurs = array_merge($erreurs, $erreurs_traitements);
			}
		}
	}
	// Si jamais on raffiche le formulaire, le reafficher à l'étape 0
	if ($formulaire['apres'] === 'formulaire' && $etapes) {
		set_request('_etape', '1');
	}
	return $erreurs;
}

/**
 * Traitement du formulaire CVT de Formidable.
 *
 * Exécute les traitements qui sont indiqués dans la configuration des
 * traitements de ce formulaire formidable.
 *
 * Une fois fait, gère le retour après traitements des saisies en fonction
 * de ce qui a été configuré dans le formulaire, par exemple :
 * - faire réafficher le formulaire,
 * - faire afficher les saisies
 * - rediriger sur une autre page...
 *
 * @param int|string $id
 *     Identifiant numerique ou textuel du formulaire formidable
 * @param array|string $valeurs
 *     Valeurs par défauts passées au contexte du formulaire
 *     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
 *     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
 * @param int|array $options_appel : options passées à l'appel du formulaire (pour la doc, voir la doc de `formulaire_formidable_verifier()`)
 * @param bool|string $deprecated_url_redirect
 * @param bool $deprecated_forcer_modif
 *     Tableau des erreurs
 **/
function formulaires_formidable_traiter_dist($id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false) {
	$retours = [];
	include_spip('formidable_fonctions');

	$options_appel = formulaires_formidable_normaliser_options_appel($options_appel, $deprecated_url_redirect, $deprecated_forcer_modif);
	$id_formulaires_reponse = $options_appel['id_formulaires_reponse'];
	$url_redirect = $options_appel['url_redirect'];
	$forcer_modif = $options_appel['forcer_modif'];

	include_spip('inc/texte');

	$messages_ok = '';// Stock l'ensemble des message_ok, traitement par traitement
	$messages_erreur = '';// Stock l'ensemble des message_erreur, traitement par traitement

	// POST Mortem de securite : on log le $_POST pour ne pas le perdre si quelque chose se passe mal
	include_spip('inc/json');
	$post = json_encode(['post' => $_POST, 'files' => $_FILES]);
	spip_log($post, 'formidable_post' . _LOG_INFO_IMPORTANTE);

	// On peut donner soit un id soit un identifiant
	if (!$id_formulaire = formidable_id_formulaire($id)) {
		return ['message_erreur' => _T('formidable:erreur_base')];
	}

	$formulaire = sql_fetsel('*', 'spip_formulaires', 'id_formulaire = ' . $id_formulaire);
	$saisies = formidable_deserialize($formulaire['saisies']);

	formulaires_formidable_sanitizer($saisies);
	$traitements = formidable_deserialize($formulaire['traitements']);

	// On met à null les (sous-)saisies masquées par afficher_si
	formidable_saisies_afficher_si_masquees_set_request_null();

	// selon le choix, le formulaire se remet en route à la fin ou non
	$retours['editable'] = ($formulaire['apres'] == 'formulaire');
	$retours['formidable_afficher_apres'] = $formulaire['apres'];
	$retours['id_formulaire'] = $id_formulaire;

	// Indiquer aux traitements s'il y un message de retour general. Par sécurité, on trim. Voir https://git.spip.net/spip-contrib-extensions/formidable/issues/58
	$formulaire['message_retour']  = trim($formulaire['message_retour']);

	// Si on a une redirection valide
	if ($url_redirect) {
		$retours['redirect'] = $url_redirect;
	} elseif (($formulaire['apres'] === 'redirige') && ($formulaire['url_redirect'] != '')) {
		refuser_traiter_formulaire_ajax();
		// traiter les raccourcis artX, brX
		include_spip('inc/lien');
		$url_redirect = typer_raccourci($formulaire['url_redirect']);
		if (count($url_redirect) > 2) {
			$url_redirect = $url_redirect[0] . $url_redirect[2];
		} else {
			$url_redirect = $formulaire['url_redirect']; // URL classique
		}

		$retours['redirect'] = $url_redirect;
	}


	// les traitements deja faits se notent ici
	// pour etre sur de ne pas etre appeles 2 fois
	// ainsi si un traitement A a besoin d'un traitement B,
	// et que B n'est pas fait quand il est appele, il peut rendre la main sans rien faire au premier coup
	// et sera rappele au second tour
	// note : depuis l'ajout du pipeline formidable_traitement, cette fonctionnalité n'a plus grand sens, mais on la maintient tout de même pour historique
	$retours['traitements'] = [];
	$erreur_texte = '';// Message indiquant l'inexecution d'un traitement

	// Stocker pour information les erreurs propres à chaque traiteement
	$retours['erreurs'] = [];


	// Si on a des traitements
	if (is_array($traitements) && !empty($traitements)) {
		$maxiter = 5;
		spip_log("Début des traitements pour le formulaire $id_formulaire", 'formidable' . _LOG_INFO);
		do {
			foreach ($traitements as $type_traitement => $options) {
				// si traitement deja appele, ne pas le relancer
				if (!isset($retours['traitements'][$type_traitement])) {
					if ($appliquer_traitement = charger_fonction($type_traitement, 'traiter/', true)) {
						spip_log("Application du traitement $type_traitement", 'formidable' . _LOG_INFO);
						$retours['scripts_ok'] = '';
						$retours['message_ok'] = '';
						$retours['message_erreur'] = '';
						$retours = $appliquer_traitement(
							[
								'formulaire' => $formulaire,
								'options' => $options,
								'options_appel' => $options_appel,
								'saisies' => $saisies,
								'traitements' => $traitements,
								'id_formulaire' => $id_formulaire,
								'valeurs' => $valeurs,
								'id_formulaires_reponse' => $id_formulaires_reponse,
								'forcer_modif' => $forcer_modif,
								'message_retour_general' => $formulaire['message_retour']
							],
							$retours
						);
						// Collecter les messages de retour afin de les formater correctement par la suite
						if (
							!empty($retours['message_ok'])
						) {
							$messages_ok .= wrap(trim($retours['message_ok']), '<div class="message_retour_traitement message_retour_' . $type_traitement . '">');
						}
						$messages_ok .= $retours['scripts_ok'];
						if (
							!empty($retours['message_erreur'])
						) {
							$messages_erreur .= wrap(trim($retours['message_erreur']), '<div class="message_erreur_traitement message_erreur_' . $type_traitement . '">');
							$retours['erreurs'][$type_traitement] = $retours['message_erreur'];
						}


					} else {
						// traitement introuvable, ne pas retenter
						$retours['traitements'][$type_traitement] = true;
					}
				}
			}
		} while (count($retours['traitements']) < count($traitements) && $maxiter--);
		// si on ne peut pas traiter correctement, alerter le webmestre
		if (count($retours['traitements']) < count($traitements)) {
			$erreur_texte = "Impossible de traiter correctement le formulaire $id\n"
				. 'Traitements attendus :' . implode(',', array_keys($traitements)) . "\n"
				. 'Traitements realises :' . implode(',', array_keys($retours['traitements'])) . "\n";
		}
	}


	// Le message de retour général est mis en premier, mais il est calculé en dernier, dans le cas où des traitements passerait des choses utiles
	if ($formulaire['message_retour']) {
		$messages_ok = _T_ou_typo(
			formidable_raccourcis_arobases_2_valeurs_champs(
				wrap(trim(propre($formulaire['message_retour'])), '<div class="message_retour_defaut">'),
				$saisies,
				[
					'sans_reponse' => '',
					'contexte' => 'message_retour',
					'id_formulaire' => $id_formulaire,
					'id_formulaires_reponse' => $retours['id_formulaires_reponse'] ?? 0,
				]
			)
		)
		. $messages_ok;
	}

	// Envoyer le message de retour
	$retours['message_erreur'] = $messages_erreur;
	$retours['message_ok'] = $messages_ok;
	if (isset($retours['fichiers'])) {// traitement particuliers si fichiers
		if ($erreurs_fichiers = formidable_produire_messages_erreurs_fichiers($retours['fichiers'])) {
			// Inspecter les fichiers pour voir s'il y a des erreurs
			// Avertir l'utilisateur
			if (isset($retours['message_erreur'])) {
				$retours['message_erreur'] .= '<br />' . $erreurs_fichiers['message_public'];
			} else {
				$retours['message_erreur'] = $erreurs_fichiers['message_public'];
			}
			// Avertir le ou la webmestre
			if (isset($retours['id_formulaires_reponse'])) {
				$erreur_fichiers_sujet = '[ERREUR] Impossible de sauvegarder les fichiers de la réponse ' . $retours['id_formulaires_reponse'] . " au formulaire $id";
			} else {
				$erreur_fichiers_sujet = "[ERREUR] Impossible de sauvegarder les fichiers de la réponse au formulaire $id";
			}
			$erreur_fichiers_texte = "Récupérez le plus rapidement possible les fichiers temporaires suivants\n";
			$erreur_fichiers_texte .= $erreurs_fichiers['message_webmestre'];
			$envoyer_mail = charger_fonction('envoyer_mail', 'inc');
			$envoyer_mail($GLOBALS['meta']['email_webmaster'], $erreur_fichiers_sujet, $erreur_fichiers_texte);
		}
		if ($formulaire['apres'] == 'valeurs') {
			// Si on affiche après les valeurs des réponses, modifier _request pour les saisies de types fichiers
			$vignette_par_defaut = charger_fonction('vignette', 'inc/');
			foreach ($retours['fichiers'] as $saisie => $description) {
				foreach ($description as $i => $desc) {
					// ajouter la vignette et l'url
					if (!isset($description[$i]['erreur'])) {
						$description[$i]['vignette'] = $vignette_par_defaut($desc['extension'], false);
						if (isset($retours['id_formulaires_reponse'])) {// si réponse enregistrée
							$description[$i]['url'] =  formidable_generer_url_action_recuperer_fichier($id_formulaire, $retours['id_formulaires_reponse'], $saisie, $desc['nom']);
						} elseif (isset($retours['timestamp'])) { // si réponse simplement envoyée par courriel
							$description[$i]['url'] = formidable_generer_url_action_recuperer_fichier_email(
								$saisie,
								$desc['nom'],
								['timestamp' => $retours['timestamp']]
							);
						}
					}
				}
				set_request($saisie, $description);
			}
		}
	}
	// Si on fait une redirection
	// Et que l'on a enregistré le résultat
	// Alors, passer l'id de la réponse à la page
	if (isset($retours['id_formulaires_reponse']) && isset($retours['redirect'])) {
		$retours['redirect'] = parametre_url($retours['redirect'], 'id_formulaires_reponse', $retours['id_formulaires_reponse'], '&');
	}
	// lorsqu'on affichera à nouveau le html,
	// dire à cvt-upload de ne pas générer le html pour les résultats des saisies fichiers
	if ($formulaire['apres'] === 'formulaire' && isset($retours['fichiers'])) {
		$formidable_cvtupload_precharger_fichiers = [];
		set_request('_fichiers', null);
		set_request('_cvtupload_precharger_fichiers_forcer', true);
		foreach ($retours['fichiers'] as $champ => $valeur) {
			$i = -1;
			foreach ($valeur as $id => $info) {
				$i++;
				if (isset($info['fichier'])) {
					$nom_fichier = $info['fichier'];
				} else {
					$nom_fichier = $info['nom'];
				}
				if (isset($retours['id_formulaires_reponse'])) {
					$chemin_fichier = _DIR_FICHIERS_FORMIDABLE
						. 'formulaire_' . $retours['id_formulaire']
						. '/reponse_' . $retours['id_formulaires_reponse']
						. '/' . $champ
						. '/' . $nom_fichier;
					$formidable_cvtupload_precharger_fichiers[$champ][$i]['url'] = formidable_generer_url_action_recuperer_fichier($retours['id_formulaire'], $retours['id_formulaires_reponse'], $champ, $nom_fichier);
					$formidable_cvtupload_precharger_fichiers[$champ][$i]['chemin'] = $chemin_fichier;
				} elseif (isset($retours['timestamp'])) {
					$chemin_fichier = _DIR_FICHIERS_FORMIDABLE
						. 'timestamp/'
						. $retours['timestamp'] . '/'
						. $champ . '/'
						. $nom_fichier;
					$formidable_cvtupload_precharger_fichiers[$champ][$i]['chemin'] = $chemin_fichier;
					$formidable_cvtupload_precharger_fichiers[$champ][$i]['url'] = formidable_generer_url_action_recuperer_fichier_email(
						$champ,
						$nom_fichier,
						['timestamp' => $retours['timestamp']]
					);
				}
			}
		}
		set_request('_formidable_cvtupload_precharger_fichiers', $formidable_cvtupload_precharger_fichiers);
	}
	// Pas besoin de ça dans le vrai retour final
	unset($retours['traitements']);
	// Drapeau pour dire que tous les traitements sont terminés, afin qu'on le sache dans le charger()
	set_request('formidable_traiter_ok', true);

	// Nettoyer les retours, pour ne pas bloquer la redirection si jamais erreurs est un tableau vide
	$retours = array_filter($retours);

	return $retours;
}

/**
 * Déclare à cvtupload les champs fichiers du formulaire
 *
 * @param int|string $id
 * @param array|string $valeurs
 *     Valeurs par défauts passées au contexte du formulaire
 *     Exemple : array('hidden_1' => 3) pour que champ identifie "@hidden_1@" soit prerempli
 *     Cela peut aussi etre une liste séparée par virgule dans le cas d'un appel via modèle
 * @param int|array $options_appel options diverses à l'appel, voir la doc de `formulaires_formidable_charger_dist()` pour une liste à jour
 * @param bool|string $deprecated_url_redirect
 * @param bool $deprecated_url_redirect
 * @return array
 *     Tableau des champs de type fichier
 **/
function formulaires_formidable_fichiers($id, $valeurs = [], $options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false) {
	$saisies = saisies_chercher_formulaire('formidable', [$id, $valeurs, $options_appel, $deprecated_url_redirect, $deprecated_forcer_modif]);
	// Si pas de saisies
	if (!$saisies) {
		return [];
	}
	$saisies_fichiers = array_keys(saisies_lister_avec_type($saisies, 'fichiers'));
	return $saisies_fichiers;
}
/**
 * Ajoute dans le contexte les elements
 * donnés par une reponse de formulaire indiquée
 *
 * @param array $contexte
 *     Contexte pour le squelette HTML du formulaire
 * @param int $id_formulaires_reponse
 *     Identifiant de réponse
 * @param bool $ok
 *     La reponse existe bien ?
 * @return array $contexte
 *     Contexte complété des nouvelles informations
 *
 **/
function formidable_definir_contexte_avec_reponse($contexte, $id_formulaires_reponse, &$ok) {
	include_spip('formidable_fonctions');

	if ($id_formulaires_reponse == false) {
		$ok = true;
		return $contexte;
	} else {
		$ok = sql_getfetsel('id_formulaires_reponse', 'spip_formulaires_reponses', "id_formulaires_reponse = $id_formulaires_reponse");
	}


	// On prépare des infos si jamais on a des champs fichiers
	$saisies = formidable_deserialize($contexte['_formidable']['saisies']);
	$saisies_fichiers = saisies_lister_avec_type($saisies, 'fichiers');// les saisies de type fichier
	$fichiers = [];
	$id_formulaire = $contexte['_formidable']['id_formulaire'];

	// On va chercher tous les champs
	$champs = sql_allfetsel(
		'nom, valeur',
		'spip_formulaires_reponses_champs',
		'id_formulaires_reponse = ' . $id_formulaires_reponse
	);
	// On remplit le contexte avec les résultats précédents
	foreach ($champs as $champ) {
		if (array_key_exists($champ['nom'], $saisies_fichiers)) {
			$valeur = formidable_deserialize($champ['valeur']);
			$nom = $champ['nom'];
			$fichiers[$nom] = [];
			$chemin = _DIR_FICHIERS_FORMIDABLE
				. "formulaire_$id_formulaire/reponse_$id_formulaires_reponse/"
				. "$nom/";
			if (is_array($valeur)) {
				foreach ($valeur as $f => $fichier) {
					$fichiers[$nom][$f] = [];
					$fichiers[$nom][$f]['url'] =  formidable_generer_url_action_recuperer_fichier($id_formulaire, $id_formulaires_reponse, $champ['nom'], $fichier['nom']);
					$fichiers[$nom][$f]['chemin'] = $chemin . $fichier['nom'];
				}
			}
		} else {
			$test_array = formidable_deserialize($champ['valeur']);
			$contexte[$champ['nom']] = is_array($test_array) ? $test_array : $champ['valeur'];
		}
	}
	if ($fichiers != []) {//s'il y a des fichiers dans les réponses
		$contexte['cvtupload_precharger_fichiers'] = $fichiers;
	}
	return $contexte;
}

/**
 * Produire un message d'erreur concaténant les messages d'erreurs
 * par fichier.
 * Fournir également une forme pour l'envoyer par webmestre
 * @param array $fichiers
 * 		le tableau des fichiers qui a été remplie par formidable_deplacer_fichiers_produire_vue_saisie()
 * @return array|string ('message_public' => 'message', 'message_webmestre' => 'message') ou bien ''
**/
function formidable_produire_messages_erreurs_fichiers($fichiers) {
	$message_public = '';
	$message_webmestre = '';
	foreach ($fichiers as $champ => $description_champ) {
		foreach ($description_champ as $n => $description) {
			if (isset($description['erreur'])) {
				$message_public .= $description['erreur'] . "\n";
				$message_webmestre .= "Pour le champ $champ[$n]:\n"
					. '- Le fichier temporaire : ' . $description['tmp_name'] . "\n"
					. '- Ayant pour véritable nom : ' . $description['nom'] . " \n";
			}
		}
	}
	if ($message_public != '') {
		return ['message_public' => $message_public, 'message_webmestre' => $message_webmestre];
	} else {
		return '';
	}
}

/**
 * Pour les champs mis à '' par afficher_si dans le hit courant
 * placer le request en null
**/
function formidable_saisies_afficher_si_masquees_set_request_null() {
	$champs = saisies_liste_set_request('get');
	foreach ($champs as $champ => $histo) {
		if ($histo[1] === '') {
			saisies_set_request($champ, null);
		}
	}
}

/**
 * Normaliser les options d'appel de `#FORMULAIRE_FORMIDABLE`
 * Le passage à Formidable 5.2.0 (952e1779364534865a3fd5e7abae85b52ec59c22)
 * a en effet rapatrié les 3 derniers arguments en un seul.
 * Remplit également les valeurs par défaut.
 * @param array|int $options_appel
 * @param bool|string $deprecated_url_redirect
 * @param bool $deprecated_forcer_modif
 * @return array
 **/
function formulaires_formidable_normaliser_options_appel($options_appel = [], $deprecated_url_redirect = false, $deprecated_forcer_modif = false): array {
	// Retrocompatiblité
	if (!is_array($options_appel)) {
		$options_appel = ['id_formulaires_reponse' => $options_appel];
		$options_appel['url_redirect'] = $deprecated_url_redirect;
		$options_appel['forcer_modif'] = $deprecated_forcer_modif;

		$deprecate = 'Le troisième argument de `#FORMULAIRE_FORMIDABLE` doit être un tableau. Les 4e et 5e arguments sont dépréciés. La retrocompatiblité sera supprimée en formidable v8. Voir https://contrib.spip.net/3284#3emearg.';
		trigger_error($deprecate, E_USER_DEPRECATED);
		spip_log($deprecate, 'deprecated_formidable');
	}

	// Valeurs par défaut pour les options
	$options_appel['id_formulaires_reponse'] = $options_appel['id_formulaires_reponse'] ?? false;
	$options_appel['url_redirect'] = $options_appel['url_redirect'] ?? false;
	$options_appel['forcer_modif'] = $options_appel['forcer_modif'] ?? false;
	$options_appel['no_ajax'] = $options_appel['no_ajax'] ?? false;

	return $options_appel;
}

/**
 * Sécuriser les input formidable
 * @param array $saisies liste des saisies
 * @return void
**/
function formulaires_formidable_sanitizer(array $saisies): void {
	$saisies = saisies_lister_par_nom($saisies);
	include_spip('inc/texte_mini');
	foreach ($saisies as $name => $description) {
		$valeur = saisies_request($name);
		saisies_set_request($name, formulaires_formidable_sanitizer_valeur($valeur));
	}
}

/**
 * Sanitize une valeur en tenant compte du fait que c'est ou pas un tableau)
 * @param array|string $valeur
 * @return array|string valeur sanitizée
 **/
function formulaires_formidable_sanitizer_valeur($valeur) {
	if (!$valeur) {
		return $valeur;
	} elseif (is_array($valeur)) {
		return array_map('formulaires_formidable_sanitizer_valeur', $valeur);
	} else {
		include_spip('inc/texte_mini');
		return safehtml($valeur);
	}
}

/**
 * Normaliser sous forme de tableau l'option `$valeurs` passée aux fonctions CVT
 * @param string|array $valeurs
 * @return array $valeurs
 **/
function formulaires_formidable_normaliser_valeurs($valeurs): array {
	// Si c'est une chaine on essaye de la parser
	if (is_string($valeurs)) {
		$liste = explode(',', $valeurs);
		$liste = array_map('trim', $liste);
		$valeurs = [];
		foreach ($liste as $i => $cle_ou_valeur) {
			if ($i % 2 == 0) {
				$valeurs[$liste[$i]] = $liste[$i + 1];
			}
		}
	}
	return $valeurs;
}
