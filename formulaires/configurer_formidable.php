<?php

/**
 * Configuration du plugin formidable
 * @return array
 **/
function formulaires_configurer_formidable_saisies(): array {
	return [
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'general',
				'label' => '<:formidable:cfg_general_label:>',
				'onglet' => true,
				'onglet_vertical' => true
			],
			'saisies' => [
				[
					'saisie' => 'choisir_objets',
					'options' => [
						'nom' => 'objets',
						'label' => '<:formidable:cfg_objets_label:>',
						'explication' => '<:formidable:cfg_objets_explication:>',
						'conteneur_class' => 'pleine_largeur',
					]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'auteur',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:formidable:traiter_enregistrement_option_auteur:>',
						'explication' => '<:formidable:traiter_enregistrement_option_auteur_explication:>'
					]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'autoriser_admin_restreint',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:formidable:autoriser_admin_restreint:>'
					]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'activer_pages',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:formidable:activer_pages_label:>',
						'explication' => '<:formidable:activer_pages_explication:>'
					]
				]
			]
		],
		[
			'saisie' => 'fieldset',
			'options' => [
				'nom' => 'enregistrement',
				'label' => '<:formidable:traiter_enregistrement_titre:>',
				'onglet' => true,
			],
			'saisies' => [
				[
					'saisie' => 'case',
				'options' => [
					'nom' => 'admin_reponses_auteur',
					'conteneur_class' => 'pleine_largeur',
					'label_case' => '<:formidable:admin_reponses_auteur:>',
					'explication' => '<:formidable:admin_reponses_auteur_explication:>',
					'afficher_si' => '@auteur@'
				]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'reponses_page_accueil',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:formidable:reponses_page_accueil:>'
					]
				],
				[
					'saisie' => 'case',
					'options' => [
						'nom' => 'exporter_adresses_ip',
						'conteneur_class' => 'pleine_largeur',
						'label_case' => '<:formidable:exporter_adresses_ip:>',
						'explication' => '<:formidable:exporter_adresses_ip_explication:>',
					]
				],
				[
					'saisie' => 'input',
					'options' => [
						'nom' => 'classe',
						'label' => '<:formidable:cfg_analyse_classe_label:>',
						'explication' => '<:formidable:cfg_analyse_classe_explication:>',
						'conteneur_class' => 'pleine_largeur',
					]
				]
			]
		]
	];
}
