<?php

/**
 * Controleur de Crayons pour les champs d'une réponse
 *
 * @param array $regs
 * @param array|null $c
 * @return array Liste html, erreur
 */
function controleurs_formulaires_reponses_champ_dist($regs, $c = null) {
	include_spip('inc/saisies');
	include_spip('formidable_fonctions');

	list(,$crayon, $type, $champ, $id) = $regs;
	$id_formulaires_reponses_champ = $regs[4];

	// Recuperer id_formulaires_reponse et id_formulaire
	// Note, sans doute pourrait-on passer directement cela en classe
	// Mais
	// 1. Cela ferait une exception
	// 2. Des gens utilisent peut être pas #VOIR_REPONSE{xxx,edit}
	$data = sql_fetsel(
		'*',
		['spip_formulaires_reponses_champs AS c', 'spip_formulaires_reponses AS r', 'spip_formulaires AS f'],
		["c.id_formulaires_reponses_champ = $id_formulaires_reponses_champ", 'r.id_formulaires_reponse = c.id_formulaires_reponse', 'f.id_formulaire = r.id_formulaire']
	);
	$id_formulaires_reponse = $data['id_formulaires_reponse'];


	$nom = $data['nom'];
	$valeur = $data['valeur'];
	$saisie = saisies_chercher(
		formidable_deserialize($data['saisies']),
		$nom
	);
	$valeur = $data['valeur'];

	$n = new Crayon(
		$type . '-valeur-' . $id_formulaires_reponses_champ,
		$valeur,
		['controleur' => 'controleurs/formulaires_reponses_champ']
	);
	$key = $n->key;


	$valeur = formidable_deserialize($valeur);

	unset($saisie['options']['label']);
	unset($saisie['options']['explication']);
	unset($saisie['options']['class']);
	unset($saisie['options']['conteneur_class']);


	// Crayons utilise son propre formalisme pour le 'name' des saisies.
	$nom_crayons = 'content_' . $key . '_valeur';
	$saisie['options']['nom'] = $nom_crayons;
	$pre_saisies = '';
	$saisie_html = saisies_generer_html($saisie, [$nom_crayons => $valeur]);

	// Cas spécial de la saisie fichier, on précharge
	if (saisies_saisie_est_fichier($saisie) && is_array($valeur)) {
		include_spip('inc/cvtupload');
		include_spip('inc/formidable_fichiers');
		include_spip('inc/filtres');

		$precharger_fichiers = charger_fonction('cvtupload_precharger_fichiers', 'inc');
		$fichiers = [];
		foreach ($valeur as $f) {
			$fichiers[] = [
				'chemin' => formidable_generer_chemin_fichier([
					'formulaire' => $data['id_formulaire'],
					'reponse' => $data['id_formulaires_reponse'],
					'saisie' => $nom,
					'fichier' => $f['nom']
				]),
				'url' => formidable_generer_url_action_recuperer_fichier(
					$data['id_formulaire'],
					$data['id_formulaires_reponse'],
					$nom,
					$f['nom']
				)
			];
		}
		$fichiers_precharges = $precharger_fichiers([$nom_crayons => $fichiers], 'formidable_crayons');

		$pre_saisie = cvtupload_generer_hidden($fichiers_precharges);
		if ($pre_saisie) {
			$cvtupload_html = cvtupload_generer_html($fichiers_precharges);
			$cvtupload_html = $cvtupload_html[$nom_crayons];
			foreach ($cvtupload_html as $cle => $html) {
				$saisie_html = preg_replace(
					"#<input[^>]*name=['\"]{$nom_crayons}(?:\&\#91;|\[){$cle}(?:\&\#93;|\])[^>]*>#i",
					$html,
					$saisie_html
				);
			}
			$saisie_html = $pre_saisie . $saisie_html;
		}
	}

	$contexte = [
		'saisie_html' => $saisie_html,
	];


	$html = $n->formulaire($contexte);

	$status = $scripts = null;

	// probablement pas la meilleure idée du siècle…
	// mais tenter d'afficher correctement le picker de date du plugin saisies dans l'espace public
	if (!test_espace_prive() && $saisie['saisie'] === 'date') {
		$scripts = '<link rel="stylesheet" type="text/css" href="' . find_in_path('css/ui/jquery-ui.css') . '" />';
	}


	return [$html . $scripts, $status];
}
