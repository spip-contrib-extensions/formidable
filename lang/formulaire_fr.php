<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/formidable.git

return [

	// I
	'icone_creer_formulaire' => 'Créer un nouveau formulaire',

	// M
	'modifier_formulaire' => 'Modifier ce formulaire',

	// T
	'titre_formulaire' => 'Formulaire',
	'titre_formulaires' => 'Formulaires',
	'titre_logo_formulaire' => 'Logo de ce formulaire',

	// V
	'vu' => 'Inséré dans le texte',
	'vu_oui' => 'Inséré',
];
