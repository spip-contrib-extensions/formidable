<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formulaire?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// I
	'icone_creer_formulaire' => 'Crea un nuovo modulo',

	// M
	'modifier_formulaire' => 'Modifica questo modulo',

	// T
	'titre_formulaire' => 'Modulo',
	'titre_formulaires' => 'Moduli',
	'titre_logo_formulaire' => 'Logo di questo modulo',

	// V
	'vu' => 'Inserito nel testo',
	'vu_oui' => 'Inserito',
];
