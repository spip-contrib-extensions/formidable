<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formulaire?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// I
	'icone_creer_formulaire' => 'Create a new form',

	// M
	'modifier_formulaire' => 'Edit this form',

	// T
	'titre_formulaire' => 'Form',
	'titre_formulaires' => 'Forms',
	'titre_logo_formulaire' => 'Logo of this form',

	// V
	'vu' => 'Inserted in text',
	'vu_oui' => 'Inserted',
];
