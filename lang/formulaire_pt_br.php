<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formulaire?lang_cible=pt_br
// ** ne pas modifier le fichier **

return [

	// I
	'icone_creer_formulaire' => 'Criar um novo formulário',

	// M
	'modifier_formulaire' => 'Alterar este formulário',

	// T
	'titre_formulaire' => 'Formulário',
	'titre_formulaires' => 'Formulários',
	'titre_logo_formulaire' => 'Logo deste formulário',

	// V
	'vu' => 'Inserir no texto',
	'vu_oui' => 'Inserido',
];
