<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-formidable?lang_cible=nl
// ** ne pas modifier le fichier **

return [

	// F
	'formidable_description' => 'Formulieren maken met behulp van een grafische interface en configuratie van gekoppelde verwerkingen (per email verzenden, antwoorden registreren, enz).',
	'formidable_slogan' => 'Formulieren genereren',
];
