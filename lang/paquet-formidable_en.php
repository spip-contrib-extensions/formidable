<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-formidable?lang_cible=en
// ** ne pas modifier le fichier **

return [

	// F
	'formidable_description' => 'Create forms within a graphical interface and configure the associated treatments (send email, record responses, etc.).',
	'formidable_slogan' => 'Forms generator',
];
