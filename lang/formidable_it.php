<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// A
	'activer_pages_explication' => 'Per default, le pagine pubbliche di moduli non sono ammesse.',
	'activer_pages_label' => 'Autorizza la creazione di pagine pubbliche per i moduli',
	'admin_reponses_auteur' => 'Autorizza gli autori dei moduli a modificare le risposte',
	'admin_reponses_auteur_explication' => 'Solo gli amministratori possono, di norma, modificare le risposte di un modulo (nella spazzatura, pubblicato, in valutazione). Questa opzione permette all’autore di un modulo di modificarne lo stato (con il rischio di falsare le statistiche).',
	'analyse_avec_reponse' => 'Risposte non vuote',
	'analyse_exclure_champs_explication' => 'Mettere il nome dei campi da escludere dall’analisi, separati da <code>|</code>. Non mettere <code>@</code>.',
	'analyse_exclure_champs_label' => 'Campi da escludere',
	'analyse_exporter' => 'Esporta l’analisi',
	'analyse_longueur_moyenne' => 'Lunghezza media in numero di parole',
	'analyse_nb_reponses_total' => '@nb@ persone hanno risposto a questo modulo.',
	'analyse_sans_reponse' => 'Risposte vuote',
	'analyse_une_reponse_total' => 'Una persona ha risposto a questo modulo.',
	'analyse_zero_reponse_total' => 'Nessuno ha risposto a questo modulo.',
	'autoriser_admin_restreint' => 'Consentire agli amministratori limitati di creare e modificare i moduli',

	// B
	'bouton_formulaires' => 'Moduli',
	'bouton_revert_formulaire' => 'Torna all’ultima versione salvata',

	// C
	'cfg_analyse_classe_explication' => 'E’ possibile specificare le classi CSS che verranno aggiunte sul contenuto di ciascun grafico, come <code>gray</code>,<code>blue</code>,	<code>orange</code>, <code>green</code> o qualsiasi cosa possa piacerti!',
	'cfg_analyse_classe_label' => 'Classe CSS della barra di avanzamento', # MODIF
	'cfg_objets_explication' => 'Scegli il contenuto che vuoi allegare al modulo.',
	'cfg_objets_label' => 'Collega i moduli ai contenuti',
	'cfg_titre_page_configurer_formidable' => 'Configura Formidable',
	'champs' => 'Campi',
	'changer_statut' => 'Questo modulo è:',
	'creer_dossier_formulaire_erreur_impossible_creer' => 'Impossibile creare la cartella @dossier@, necessaria per archiviare i file; controlla i diritti di accesso.',
	'creer_dossier_formulaire_erreur_impossible_ecrire' => 'Impossibile scrivere nella cartella @dossier@, necessaria per archiviare i file; controlla i diritti di accesso.',
	'creer_dossier_formulaire_erreur_possible_lire_exterieur' => 'Il contenuto della cartella @dossier@ può essere consultato a distanza; questo potrebbe essere un problema in termini di riservatezza dei dati.',

	// E
	'echanger_formulaire_forms_importer' => 'Forms & Tables (.xml)',
	'echanger_formulaire_wcs_importer' => 'W.C.S. (.wcs)',
	'echanger_formulaire_yaml_importer' => 'Formidable (.yaml)',
	'editer_apres_choix_formulaire' => 'Il modulo, nuovamente',
	'editer_apres_choix_redirige' => 'Redirigi verso un nuovo indirizzo',
	'editer_apres_choix_rien' => 'Assolutamente niente',
	'editer_apres_choix_stats' => 'Le statistiche delle risposte',
	'editer_apres_choix_valeurs' => 'I valori immessi',
	'editer_apres_explication' => 'Dopo la validazione, mostra al posto del modulo:', # MODIF
	'editer_apres_label' => 'Mostra di seguito', # MODIF
	'editer_css' => 'Classi CSS',
	'editer_descriptif' => 'Descrizione',
	'editer_descriptif_explication' => 'Una spiegazione del modulo destinata all’area riservata.',
	'editer_identifiant' => 'Identificativo',
	'editer_identifiant_explication' => 'Inserisci un identificativo testuale unico che ti permetterà di richiamare più facilmente il modulo',
	'editer_menu_auteurs' => 'Configura gli autori',
	'editer_menu_champs' => 'Configura i campi',
	'editer_menu_formulaire' => 'Configura il modulo',
	'editer_menu_traitements' => 'Configura i trattamenti',
	'editer_message_ok' => 'Messaggio di risposta',
	'editer_message_ok_explication' => 'Puoi personalizzare il messaggio che sarà mostrato all’utente dopo l’invio di un modulo valido.',
	'editer_modifier_formulaire' => 'Modifica il modulo',
	'editer_nouveau' => 'Nuovo modulo',
	'editer_redirige_url' => 'Indirizzo di redirezione dopo la validazione', # MODIF
	'editer_titre' => 'Titolo',
	'erreur_autorisation' => 'Non hai i permessi per la modifica dei moduli del sito.',
	'erreur_base' => 'Si è verificato un problema tecnico durante il salvataggio.',
	'erreur_deplacement_fichier' => 'Il file « @nom@ » non è stato correttamente memorizzato dal sistema; contatta il webmaster.',
	'erreur_generique' => 'Ci sono degli errori nei campi di seguito, si prega di verificare i dati inseriti.',
	'erreur_identifiant' => 'Questo identificativo è già stato utilizzato.',
	'erreur_importer_forms' => 'Errore durante l’importazione del modulo Forms&Tables',
	'erreur_importer_wcs' => 'Errore durante l’importazione del modulo W.C.S.',
	'erreur_importer_yaml' => 'Errore durante l’importazione del file YAML',
	'erreur_inexistant' => 'Il modulo non esiste.',
	'erreur_unicite' => 'Questo valore è già stato utilizzato',
	'exporter_formulaire_format_label' => 'Formato del file',
	'exporter_formulaire_statut_label' => 'Risposte',

	// F
	'formulaires_aucun' => 'Al momento non è presente alcun modulo.',
	'formulaires_dupliquer' => 'Duplica il modulo',
	'formulaires_dupliquer_copie' => '(copia)',
	'formulaires_introduction' => 'Crea e configura qui i moduli del tuo sito.',
	'formulaires_nouveau' => 'Crea un nuovo modulo',
	'formulaires_supprimer' => 'Elimina il modulo',
	'formulaires_supprimer_confirmation' => 'Attenzione, eliminando il modulo verranno eliminate anche tutte le risposte. Sei sicuro di voler eliminare il modulo?',
	'formulaires_tous' => 'Tutti i moduli',

	// I
	'identification_par_cookie' => 'Con i cookie (id casuale, non memorizza alcuna informazione personale) ',
	'identification_par_id_auteur' => 'Con l’identificativo (id_auteur) dell’utente loggato',
	'importer_formulaire' => 'Importa un modulo',
	'importer_formulaire_fichier_label' => 'File da importare',
	'importer_formulaire_format_label' => 'Formato del file',
	'info_1_formulaire' => '1 modulo',
	'info_1_reponse' => '1 risposta',
	'info_aucun_formulaire' => 'Nessun modulo',
	'info_aucune_reponse' => 'Nessuna risposta',
	'info_formulaire_refuse' => 'Archiviato',
	'info_formulaire_utilise_par' => 'Modulo utilizzato da:',
	'info_nb_formulaires' => '@nb@ moduli',
	'info_nb_reponses' => '@nb@ risposte',
	'info_reponse_proposee' => 'Moderato',
	'info_reponse_proposees' => 'Moderati',
	'info_reponse_publiee' => 'Validato',
	'info_reponse_publiees' => 'Validati',
	'info_reponse_supprimee' => 'Eliminato',
	'info_reponse_supprimees' => 'Eliminati',
	'info_reponse_toutes' => 'Tutto',
	'info_utilise_1_formulaire' => 'Modulo utilizzato:',
	'info_utilise_nb_formulaires' => 'Moduli utilizzati:',

	// L
	'liens_ajouter' => 'Aggiungere un modulo',
	'liens_ajouter_lien' => 'Aggiungere questo modulo',
	'liens_creer_associer' => 'Crea e associa un modulo',
	'liens_retirer_lien_formulaire' => 'Rimuovere il modulo',
	'liens_retirer_tous_liens_formulaires' => 'Rimuovere tutti i moduli',

	// M
	'modele_label_formulaire_formidable' => 'Quale modulo?',

	// N
	'noisette_label_afficher_titre_formulaire' => 'Mostro il titolo del modulo?',
	'noisette_label_identifiant' => 'Modulo da mostrare:',
	'noisette_nom_noisette_formulaire' => 'Modulo',

	// R
	'reponse_aucune' => 'Nessuna risposta',
	'reponse_intro' => '@auteur@ ha risposto al modulo @formulaire@',
	'reponse_numero' => 'Risposta numero:',
	'reponse_statut' => 'Questa risposta è:',
	'reponse_supprimer' => 'Elimina questa risposta',
	'reponse_supprimer_confirmation' => 'Sei sicuro di voler eliminare questa risposta?',
	'reponse_une' => '1 risposta',
	'reponses_analyse' => 'Analisi delle risposte',
	'reponses_anonyme' => 'Anonimo',
	'reponses_auteur' => 'Utente',
	'reponses_exporter' => 'Esporta le risposte',
	'reponses_exporter_format_csv' => 'Tabella .CSV',
	'reponses_exporter_format_xls' => 'Excel .XLS',
	'reponses_exporter_statut_publie' => 'Pubblicato',
	'reponses_exporter_statut_tout' => 'Tutto',
	'reponses_exporter_telecharger' => 'Scarica',
	'reponses_ip' => 'Indirizzo IP',
	'reponses_liste' => 'Elenco delle risposte',
	'reponses_liste_prop' => 'Risposte in attesa di validazione',
	'reponses_liste_publie' => 'Tutte le risposte valide',
	'reponses_nb' => '@nb@ risposte',
	'reponses_supprimer' => 'Elimina tutte le risposte', # MODIF
	'reponses_supprimer_confirmation' => 'Sei sicuro di voler rimuovere tutte le risposte di questo modulo?', # MODIF
	'reponses_voir_detail' => 'Vedi la risposta',

	// S
	'sans_reponses' => 'Senza risposta',

	// T
	'texte_statut_poubelle' => 'cancellato',
	'texte_statut_propose_evaluation' => 'proposto',
	'texte_statut_publie' => 'validato',
	'texte_statut_refuse' => 'archiviato',
	'titre_cadre_raccourcis' => 'Scorciatoia',
	'titre_formulaires_archives' => 'Archivio',
	'titre_reponses' => 'Risposte',
	'traitements_actives' => 'Trattamenti abilitati',
	'traitements_avertissement_creation' => 'Le modifiche ai campi del modulo sono state registrate con successo. Ora puoi definire quali trattamenti saranno effettuati all’atto dell’invio del modulo.',
	'traitements_avertissement_modification' => 'La modifiche ai campi del modulo sono state registrate con successo. <strong>Alcuni trattamenti devono forse essere riconfigurati di conseguenza.</strong>',
	'traiter_email_description' => 'Invia il risultato del modulo via e-mail ad un elenco di destinatari.', # MODIF
	'traiter_email_horodatage' => 'Modulo "@formulaire@" inviato il @date@ alle @heure@.',
	'traiter_email_message_erreur' => 'Si è verificato un errore nell’invio dell’email',
	'traiter_email_message_ok' => 'Il tuo messaggio è stato inviato per posta elettronica.',
	'traiter_email_option_activer_accuse_label_case' => 'Invia anche una email all’indirizzo del mittente con un messaggio di conferma.', # MODIF
	'traiter_email_option_activer_ip_label_case' => 'Invia l’indirizzo IP del mittente ai destinatari.', # MODIF
	'traiter_email_option_courriel_envoyeur_accuse_explication' => 'Specificare l’email utilizzata per inviare la conferma. In caso contrario, il destinatario sarà il mittente.', # MODIF
	'traiter_email_option_courriel_envoyeur_accuse_label' => 'E-mail di conferma', # MODIF
	'traiter_email_option_destinataires_champ_form_explication' => 'Se uno dei vostri campi è un indirizzo di posta elettronica e si desidera inviare il modulo a questo indirizzo, selezionare il campo.',
	'traiter_email_option_destinataires_champ_form_label' => 'Destinatario presente in un campo del modulo',
	'traiter_email_option_destinataires_explication' => 'Scegli il campo che corrisponde al destinatario del messaggio.

Scegli il campo che corrisponderà ai destinatari del messaggio. <br />
Questo è un campo di tipo "Destinatario" o "C.C", incluso l’id di un autore del sito.',
	'traiter_email_option_destinataires_label' => 'Destinatari',
	'traiter_email_option_destinataires_plus_explication' => 'Un elenco di indirizzi separati da virgole.',
	'traiter_email_option_destinataires_plus_label' => 'Destinatari supplementari', # MODIF
	'traiter_email_option_destinataires_selon_champ_explication' => 'Permette di indicare uno o più destinatari in base al valore di un campo. 
		Indica il campo, il valore l’indirizzo o gli indirizzi e-mail in questione (separati da una virgola) nel formto come da esempio seguente: "@selection_1@/choix1 : mail@example.tld". E’ possibile specificare più test: uno per linea.',
	'traiter_email_option_destinataires_selon_champ_label' => 'Destinatari basati su un campo',
	'traiter_email_option_envoyeur_courriel_explication' => 'Scegli il campo che contiene l’indirizzo dell’utente.', # MODIF
	'traiter_email_option_envoyeur_courriel_label' => 'Indirizzo del mittente', # MODIF
	'traiter_email_option_envoyeur_nom_explication' => 'Costruisci questo nome grazie all’aiuto dei @segnaposto@ (vedi il promemoria). Se non inserisci nulla, verrà inserito il nome del sito.',
	'traiter_email_option_envoyeur_nom_label' => 'Nome del mittente', # MODIF
	'traiter_email_option_nom_envoyeur_accuse_explication' => 'Specificare il nome del mittente utilizzato per inviare la conferma. In caso contrario, il destinatario sarà il mittente.', # MODIF
	'traiter_email_option_nom_envoyeur_accuse_label' => 'Nome del mittente della conferma', # MODIF
	'traiter_email_option_sujet_accuse_label' => 'Oggetto della conferma di ricezione', # MODIF
	'traiter_email_option_sujet_explication' => 'Costruisci l’oggetto grazie all’aiuto dei @segnaposto@. Se non inserisci nulla, l’oggetto verrà generato automaticamente.',
	'traiter_email_option_sujet_label' => 'Oggetto del messaggio', # MODIF
	'traiter_email_option_vrai_envoyeur_explication' => 'Alcuni server SMTP non permettono di impostare un mittente personalizzato per il campo "From". Per questo motivo Formidable inserisce per default l’indirzzo del mittente nel campo "Reply-To". Metti il check qui per inserire l’indirizzo nel campo "From".', # MODIF
	'traiter_email_option_vrai_envoyeur_label' => 'Inserire l’indirizzo del mittente nel campo "From"', # MODIF
	'traiter_email_page' => '<a href="@url@">Da questa pagina</a>.',
	'traiter_email_sujet' => '@nom@ ti ha scritto.',
	'traiter_email_sujet_accuse' => 'Grazie per aver risposto.',
	'traiter_email_sujet_courriel_label' => 'Oggetto del messaggio', # MODIF
	'traiter_email_titre' => 'Invia per e-mail', # MODIF
	'traiter_email_type_destinataires_label' => 'Destinatari',
	'traiter_email_url_enregistrement' => 'Puoi gestire le risposte <a href="@url@">su questa pagina</a>.',
	'traiter_enregistrement_description' => 'Salva i risultati del modulo nel database',
	'traiter_enregistrement_erreur_base' => 'Si è verificato un errore tecnico durante il salvataggio nel database',
	'traiter_enregistrement_erreur_deja_repondu' => 'Hai già risposto a questo modulo.',
	'traiter_enregistrement_erreur_edition_reponse_inexistante' => 'La risposta da modificare non esiste.',
	'traiter_enregistrement_message_ok' => 'Grazie, le vostre risposte sono state salvate.',
	'traiter_enregistrement_option_anonymiser_label' => 'Anonimizza il modulo', # MODIF
	'traiter_enregistrement_option_auteur' => 'Utilizza gli autori per i moduli',
	'traiter_enregistrement_option_auteur_explication' => 'Attribuire uno o più autori ad un modulo. Se questa opzione è attiva, solo gli autori di un modulo possono accedere ai relativi dati.',
	'traiter_enregistrement_option_identification_explication' => 'Se le risposte sono modificabili, quale procedura si dovrà utilizzare in priorità per individuare la risposta da modificare?',
	'traiter_enregistrement_option_identification_label' => 'Identificazione',
	'traiter_enregistrement_option_ip_label' => 'Registra gli indirizzi IP (vengono nascosti dopo un certo periodo)',
	'traiter_enregistrement_option_moderation_label' => 'Moderazione',
	'traiter_enregistrement_option_modifiable_explication' => 'Modificabile: i visitatori possono cambiare le loro risposte in seguito.', # MODIF
	'traiter_enregistrement_option_modifiable_label' => 'Risposte modificabili',
	'traiter_enregistrement_option_multiple_explication' => 'Multiplo: la stessa persona può rispondere più volte', # MODIF
	'traiter_enregistrement_option_multiple_label' => 'Risposte multiple',
	'traiter_enregistrement_titre' => 'Registra i risultati',

	// V
	'voir_exporter' => 'Esporta il modulo',
	'voir_numero' => 'Modulo numero:',
	'voir_reponses' => 'Vedi le risposte',
	'voir_traitements' => 'Trattamenti',
];
