<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formidable?lang_cible=de
// ** ne pas modifier le fichier **

return [

	// A
	'activer_pages_explication' => 'Standardmäßig sind öffentliche Seiten von Formularen nicht erlaubt ',
	'activer_pages_label' => 'Öffentliche Seiten für Formulare aktivieren',
	'admin_reponses_auteur' => 'Personen, die Formulare verwalten, können die Antworten bearbeiten',
	'admin_reponses_auteur_explication' => 'Standardmäßig können nur die Admins die Antworten auf ein Formular ändern (in den Papierkorb, veröffentlicht, zur Bewertung vorgeschlagen). Diese Option ermöglicht es der Person, die ein Formular verwaltet, den Status des Formulars zu ändern (auf die Gefahr hin, mögliche Statistiken zu verfälschen).',
	'analyse_avec_reponse' => 'Ausgefüllte Antworten',
	'analyse_exclure_champs_explication' => 'Setzen Sie die Namen der Felder, die bei der Analyse ausgeschlossen werden sollen, getrennt durch  <code>|</code>.  Ohne die <code>@</code>.',
	'analyse_exclure_champs_label' => 'Felder, die von der Analyse ausgeschlossen werden sollen',
	'analyse_exporter' => 'Analyse exportieren',
	'analyse_longueur_moyenne' => 'Durchschnittliche Anzahl Worte',
	'analyse_nb_reponses_total' => '@nb@ Personen haben auf dieses Formular geantwortet.',
	'analyse_sans_reponse' => 'Leere Antworten',
	'analyse_une_reponse_total' => 'eine Person hat auf dieses Formular geantwortet.',
	'analyse_zero_reponse_total' => 'Niemand hat auf dieses Formular geantwortet. ',
	'autoriser_admin_restreint' => 'Rubrikadmins können auch Formulare erstellen und bearbeiten.',

	// B
	'bouton_formulaires' => 'Formulare',
	'bouton_revert_formulaire' => 'Zur zuletzt gespeicherten Version zurückkehren',

	// C
	'cfg_analyse_classe_explication' => 'Sie können CSS-Klassen angeben, die zu den Containern jedes Grafen, hinzugefügt werden sollen, wie z. B. <code>gray</code>,<code>blue</code>,
	<code>orange</code>, <code>green</code> oder was auch immer Ihnen gefällt!',
	'cfg_analyse_classe_label' => 'CSS-Klasse des Fortschrittsbalkens', # MODIF
	'cfg_objets_explication' => 'Wählen Sie die Inhalte, mit denen die Formulare verknüpft werden können.',
	'cfg_objets_label' => 'Formulare mit Inhalten verknüpfen',
	'cfg_titre_page_configurer_formidable' => 'Formidable konfigurieren',
	'champs' => 'Felder',
	'changer_statut' => 'Dieses Formular ist',
	'creer_dossier_formulaire_erreur_impossible_creer' => 'Der Ordner @dossier@,  der zum Speichern von Dateien benötigt wird, konnte nicht erstellt werden. Überprüfen Sie die Zugriffsrechte',
	'creer_dossier_formulaire_erreur_impossible_ecrire' => 'Es ist nicht möglich, in den @dossier@ zu schreiben,  der zum Speichern von Dateien benötigt wird. Überprüfen Sie die Zugriffsrechte.',
	'creer_dossier_formulaire_erreur_possible_lire_exterieur' => 'Es ist möglich, den Inhalt des Ordners @dossier@aus der Ferne zu lesen. Dies ist in Bezug auf die Vertraulichkeit der Daten problematisch. ',

	// D
	'date_envoi' => 'Absendedatum',

	// E
	'echanger_formulaire_forms_importer' => 'Forms & Tables (.xml)',
	'echanger_formulaire_wcs_importer' => 'W.C.S. (.wcs)',
	'echanger_formulaire_yaml_importer' => 'Formidable (.yaml)',
	'editer_apres_choix_formulaire' => 'Das Formular, erneut',
	'editer_apres_choix_redirige' => 'Auf eine andere Seite weiterleiten',
	'editer_apres_choix_rien' => 'Nichts',
	'editer_apres_choix_stats' => 'Die Antwortstatistiken',
	'editer_apres_choix_valeurs' => 'Die eingegebenen Werte',
	'editer_apres_explication' => 'Nach der Bestätigung anstelle des Formulars anzeigen', # MODIF
	'editer_apres_label' => 'Anschließend anzeigen', # MODIF
	'editer_css' => 'CSS-Klassen',
	'editer_descriptif' => 'Beschreibung',
	'editer_descriptif_explication' => 'Eine Beschreibung des Formulars für das Backend.',
	'editer_globales_afficher_si_submit_label' => 'Bedingte Anzeige des Bestätigungsbuttons',
	'editer_globales_etapes_activer_explication' => 'Wenn diese Option aktiviert ist, wird jede Gruppe von Feldern der ersten Ebene in einen Abschnitt des Formulars umgewandelt.',
	'editer_globales_etapes_activer_label_case' => 'Abschnittsverwaltung aktivieren',
	'editer_globales_etapes_ignorer_recapitulatif_label_case' => 'Keine Zusammenfassung der Antworten am Ende eines Abschnitts anzeigen
',
	'editer_globales_etapes_label' => 'Mehrere Abschnitte',
	'editer_globales_etapes_precedent_label' => 'Text des Zurück-Buttons (Standard: "Zurück")',
	'editer_globales_etapes_precedent_suivant_titrer_label' => 'Den Namen des Abschnitts in die Schaltflächen Vorheriger/Nächster einfügen',
	'editer_globales_etapes_presentation_courante_label' => 'Nur den aktuellen Abschnitt und die Gesamtzahl der Abschnitte anzeigen (einschließlich der Antwortzusammenfassung) ',
	'editer_globales_etapes_presentation_defaut_label' => 'Alle Abschnitte präsentieren',
	'editer_globales_etapes_presentation_label' => 'Darstellung der Abschnitte',
	'editer_globales_etapes_suivant_label' => 'Text des Weiter-Buttons (standardmäßig "Weiter")',
	'editer_globales_submit_label' => 'Bestätigungsbutton',
	'editer_globales_technique_label' => 'Technik',
	'editer_globales_texte_submit_label' => 'Text des Bestätigungsbuttons',
	'editer_identifiant' => 'Bezeichnung',
	'editer_identifiant_explication' => 'Geben sie eine Bezeichnung an, mit dem sie das Formular ansprechen können. Darf nur Zahlen, Buchstaben und "_" enthalten.',
	'editer_menu_auteurs' => 'Autoren konfigurieren',
	'editer_menu_champs' => 'Felder konfigurieren',
	'editer_menu_formulaire' => 'Formular konfigurieren',
	'editer_menu_traitements' => 'Verarbeitung konfigurieren',
	'editer_message_ok' => 'Bestätigungstext',
	'editer_message_ok_explication' => 'Sie können die Nachricht anpassen, die dem Nutzer angezeigt wird, nachdem er ein gültiges Formular abgeschickt hat. Es ist möglich, den Wert bestimmter Felder anzuzeigen, mithilfe von @raccourci@.',
	'editer_modifier_formulaire' => 'Formular ändern',
	'editer_nouveau' => 'Neues Formular',
	'editer_redirige_url' => 'Adresse für die Weiterleitung nach der Bestätigung', # MODIF
	'editer_titre' => 'Titel',
	'erreur_autorisation' => 'Sie sind nicht berechtigt, die Formulare der Website zu ändern.',
	'erreur_base' => 'Beim Speichern ist ein technischer Fehler aufgetreten.',
	'erreur_deplacement_fichier' => 'Die Datei "@nom@" konnte vom System nicht richtig gespeichert werden. Wenden Sie sich an den Webmaster.
	',
	'erreur_fichier_expire' => 'Der Link zum Herunterladen der Datei ist nicht mehr aktuell.',
	'erreur_fichier_introuvable' => 'Die angeforderte Datei wurde nicht gefunden.',
	'erreur_generique' => 'Die Felder enthalten Fehler. Bitte überprüfen Sie Ihre Eingaben.',
	'erreur_identifiant' => 'Diese Bezeichnung wird bereits verwendet.',
	'erreur_importer_forms' => 'Fehler beim Importieren des Formulars aus Forms&Tables',
	'erreur_importer_wcs' => 'Fehler beim Importieren des Formulars aus W.C.S',
	'erreur_importer_yaml' => 'Fehler beim Importieren des Formulars aus YAML',
	'erreur_inexistant' => 'Formular nicht vorhanden.',
	'erreur_saisies_modifiees_parallele' => 'Die Eingaben im Formular wurden anderweitig geändert. Ihre eigenen Änderungen wurden daher nicht gespeichert. Bitte wiederholen Sie die Bearbeitung der Felder.',
	'erreur_unicite' => 'Dieser Wert wird bereits verwendet',
	'exporter_adresses_ip' => 'IP-Adressen in den Export von Antworten einbeziehen',
	'exporter_adresses_ip_explication' => 'Standardmäßig werden IP-Adressen nicht in den Export von Antworten einbezogen.',
	'exporter_formulaire_cle_ou_valeur_cle_label' => 'Schlüssel',
	'exporter_formulaire_cle_ou_valeur_label' => 'Sollen bei Radiobuttons, Dropdown-Listen usw. die lesbaren Werte (Labels) oder die Schlüssel exportiert werden?',
	'exporter_formulaire_cle_ou_valeur_valeur_label' => 'Lesbare Werte (Labels)',
	'exporter_formulaire_date_debut_label' => 'Ab (inklusive)', # MODIF
	'exporter_formulaire_date_erreur' => 'Das Startdatum muss vor dem Enddatum liegen',
	'exporter_formulaire_date_fin_label' => 'Bis (einschließlich)',
	'exporter_formulaire_format_label' => 'Format der Datei',
	'exporter_formulaire_ignorer_fichiers_explication_label' => 'Dieses Formular enthält Dateifelder. Möchten Sie diese lieber nicht an den Export anhängen, sondern z. B. per FTP laden?',
	'exporter_formulaire_ignorer_fichiers_label' => 'Dateien nicht anhängen',
	'exporter_formulaire_statut_label' => 'Antworten',

	// F
	'formulaires_aucun' => 'Kein Formular vorhanden.',
	'formulaires_corbeille_tous' => '@nb@ Formulare im Papierkorb',
	'formulaires_corbeille_un' => 'Ein Formular im Papierkorb',
	'formulaires_dupliquer' => 'Formular kopieren',
	'formulaires_dupliquer_copie' => '(Kopie)',
	'formulaires_introduction' => 'Erstellen und bearbeiten sie Formulare für ihre Website.',
	'formulaires_nouveau' => 'Neues Formular anlegen',
	'formulaires_reponse_cextras' => 'Mit der Antwort verknüpfte Zusatzfelder',
	'formulaires_reponses_corbeille_tous' => '@nb@ Antworten im Papierkorb',
	'formulaires_reponses_corbeille_un' => 'Eine Antwort im Papierkorb',
	'formulaires_supprimer' => 'Formular löschen',
	'formulaires_supprimer_confirmation' => 'Achtung - alle Daten aus dem Formular werden ebenfalls gelöscht. Sind sie sicher, dass sie das Formular löschen wollen?',
	'formulaires_tous' => 'Alle Formulare',

	// H
	'heures_minutes_secondes' => '@h@h @m@min @s@s',

	// I
	'icone_modifier_formulaires_reponse' => 'Antwort bearbeiten',
	'icone_modifier_formulaires_reponse_cextras' => 'Mit der Antwort verknüpfte Zusatzfelder bearbeiten',
	'icone_retour_formulaires_reponse' => 'Zurück zur Antwort',
	'id_formulaires_reponse' => 'Identifikator der Antwort',
	'identification_par_cookie' => 'Per Cookie (zufällige Kennung, speichert keine persönlichen Informationen)',
	'identification_par_id_auteur' => 'Durch die Kennung (id_autor) der angemeldeten Person',
	'identification_par_id_reponse' => 'Durch die Kennung (id_formulaire_reponse) der Antwort, die explizit beim Aufruf des Formulars in einem Skelett übergeben wird',
	'identification_par_variable_php' => 'Über eine PHP-Verbindungsvariable (Hash)',
	'importer_formulaire' => 'Formular importieren',
	'importer_formulaire_fichier_label' => 'Dateien importeieren',
	'importer_formulaire_format_label' => 'Format der Datei',
	'info_1_formulaire' => '1 Formular',
	'info_1_reponse' => '1 Antwort',
	'info_aucun_formulaire' => 'Kein Formular',
	'info_aucune_reponse' => 'Keine Antwort',
	'info_formulaire_propose' => 'Vorgeschlagen',
	'info_formulaire_publie' => 'Veröffentlicht',
	'info_formulaire_refuse' => 'Archiviert',
	'info_formulaire_supprime' => 'Im Mülleimer',
	'info_formulaire_utilise_par' => 'Formular verwendet von',
	'info_nb_formulaires' => '@nb@ Formulare',
	'info_nb_reponses' => '@nb@ Antworten',
	'info_reponse_poubelle' => 'Im Mülleimer',
	'info_reponse_proposee' => 'Zu moderieren',
	'info_reponse_proposees' => 'Zu moderieren',
	'info_reponse_publiee' => 'Freigegeben',
	'info_reponse_publiees' => 'Freigegeben',
	'info_reponse_refusee' => 'Abgelehnt',
	'info_reponse_refusees' => 'Abgelehnt',
	'info_reponse_supprimee' => 'Im Mülleimer',
	'info_reponse_supprimees' => 'Im Mülleimer',
	'info_reponse_toutes' => 'Alle',
	'info_utilise_1_formulaire' => 'Genutztes Formular',
	'info_utilise_nb_formulaires' => 'Genutzte Formulare',

	// J
	'jours_heures_minutes_secondes' => '@j@d @h@h @m@min @s@s',

	// L
	'lien_expire' => 'Link verfällt in @delai@',
	'liens_ajouter' => 'Formular hinzufügen',
	'liens_ajouter_lien' => 'Dieses Formular hinzufügen',
	'liens_creer_associer' => 'Ein Formular erstellen und verknüpfen',
	'liens_retirer_lien_formulaire' => 'Dieses Formular entfernen',
	'liens_retirer_tous_liens_formulaires' => 'Alle Formulare entfernen',

	// M
	'minutes_secondes' => '@m@min @s@s',
	'modele_label_formulaire_formidable' => 'Welches Formular?',

	// N
	'noisette_label_afficher_titre_formulaire' => 'Den Titel des Formulars anzeigen?',
	'noisette_label_identifiant' => 'Formular zum Anzeigen ',
	'noisette_nom_noisette_formulaire' => 'Formular',

	// O
	'options_traitements' => 'Bearbeitungsmöglichkeiten',

	// P
	'pas_analyse_fichiers' => 'Formidable bietet (noch) keine Analyse der gesendeten Dateien an.',

	// R
	'reponse_aucune' => 'Keine Antwort',
	'reponse_intro' => '@auteur@ hat auf das Formular geantwortet @formulaire@',
	'reponse_maj' => 'Letzte Änderung',
	'reponse_numero' => 'Antwort Nummer',
	'reponse_statut' => 'Diese Antwort ist',
	'reponse_supprimer' => 'Diese Antwort löschen',
	'reponse_supprimer_confirmation' => 'Wollen sie diese Antwor wirklich löschen?',
	'reponse_une' => '1 Antwort',
	'reponses_analyse' => 'Analyse der Antworten',
	'reponses_anonyme' => 'Anonym',
	'reponses_auteur' => 'Nutzer',
	'reponses_exporter' => 'Antworten exportieren',
	'reponses_exporter_format_csv' => 'Tabelle .CSV',
	'reponses_exporter_format_xls' => 'Excel .XLS',
	'reponses_exporter_statut_publie' => 'Veröffentlicht',
	'reponses_exporter_statut_tout' => 'Alle',
	'reponses_exporter_telecharger' => 'Download',
	'reponses_ip' => 'IP-Adresse',
	'reponses_liste' => 'Liste der Antworten',
	'reponses_liste_prop' => 'Nicht freigegebene Antworten',
	'reponses_liste_publie' => 'Alle freigegebenen Antworten',
	'reponses_nb' => '@nb@ Antworten',
	'reponses_page_accueil' => 'Antworten auf der Startseite des privaten Bereichs anzeigen',
	'reponses_supprimer' => 'Alle Antworten löschen', # MODIF
	'reponses_supprimer_confirmation' => 'Sind Sie sicher, dass Sie alle Antworten auf dieses Formular löschen möchten?', # MODIF
	'reponses_voir_detail' => 'Antwort anzeigen',

	// S
	'sans_reponses' => 'Unbeantwortet',
	'secondes' => '@s@s',

	// T
	'texte_statut_poubelle' => 'im Mülleimer',
	'texte_statut_propose_evaluation' => 'vorgeschlagen',
	'texte_statut_publie' => 'freigegeben',
	'texte_statut_refuse' => 'archiviert',
	'texte_statut_refusee' => 'abgelehnt',
	'titre_cadre_raccourcis' => 'Abkürzungen',
	'titre_formulaires_archives' => 'Archive',
	'titre_formulaires_poubelle' => 'Im Mülleimer',
	'titre_reponses' => 'Antworten',
	'traitement_email_sujet_courriel_modif_reponse' => '[Antwort bearbeiten]',
	'traitements_actives' => 'Derzeit in Bearbeitung',
	'traitements_avertissement_creation' => 'Die Änderungen der Felder des Formulars wurden erfolgreich gespeichert. Jetzt können sie festlege, welche Verarbeitungsschritte nach dem Senden des Formulars ausgef',
	'traitements_avertissement_modification' => 'Die Änderungen der Felder des Formulars wurden erfolgreich gespeichert. <strong>Manche Verarbeitungsoptionen müssen entsprechend neu konfiguriert werden.</strong>',
	'traitements_choisis' => 'Auswahl der Verarbeitungsschritte',
	'traiter_email_AR_label' => 'Empfangsbestätigung', # MODIF
	'traiter_email_contenu_courriel_label' => 'Inhalt der E-Mail', # MODIF
	'traiter_email_description' => 'Ergebnisse des Formulars per E-Mail versenden', # MODIF
	'traiter_email_destinataires_courriel_label' => 'Empfänger der E-Mail', # MODIF
	'traiter_email_envoyeur_courriel_label' => 'Herkunft der E-Mail',
	'traiter_email_generalite_label' => 'Allgemeine Einstellungen',
	'traiter_email_horodatage' => 'Formular "@formulaire@" am @date@ um @heure@ gesendet.',
	'traiter_email_horodatage_modif_reponse' => 'Formular "@formulaire@" gepostet am @date@ um @heure@  (ändert die Antwort, die am @date_precedente@ um @heure_precedente@ gesendet wurde).',
	'traiter_email_message_erreur' => 'Beim Versenden des Formulars ist ein Fehler aufgetreten.',
	'traiter_email_message_ok' => 'Ihre Nachricht wurde per Mail gesendet.',
	'traiter_email_option_activer_accuse_label_case' => 'Eine Empfangsbestätigung an den Internetnutzer senden ', # MODIF
	'traiter_email_option_activer_ip_label_case' => 'Senden der IP-Adresse des Internetnutzers an die Empfänger',
	'traiter_email_option_courriel_envoyeur_accuse_explication' => 'Geben Sie die E-Mail-Adresse an, die zum Versenden der Empfangsbestätigung verwendet wird. Wenn Sie nichts eintragen, ist dies die E-Mail-Adresse des Webmasters.', # MODIF
	'traiter_email_option_courriel_envoyeur_accuse_label' => 'Adresse für die Versendung der Empfangsbestätigung', # MODIF
	'traiter_email_option_destinataires_champ_form_attention' => 'Von dieser Option wird abgeraten, da sie einen Aufruf zu SPAM darstellt.
	<br />- Um an Mitglieder des Redaktionsteams der Website zu senden, verwenden Sie die Option "Empfänger" (weiter oben).
	<br />- Um an die Person zu senden, die das Formular ausfüllt, richten Sie die Empfangsbestätigung ein (weiter unten).
	
Diese Option wird nur aus Gründen der Abwärtskompatibilität beibehalten. Sie erscheint nicht auf neuen Formularen.
', # MODIF
	'traiter_email_option_destinataires_champ_form_explication' => 'Wenn eines Ihrer Felder eine E-Mail-Adresse ist und Sie das Formular an diese Adresse senden möchten, wählen Sie das Feld aus.',
	'traiter_email_option_destinataires_champ_form_label' => 'Empfänger, der in einem der Formularfelder vorkommt',
	'traiter_email_option_destinataires_explication' => 'Wählen Sie das Feld, das den Empfängern der Nachricht enthält. <br />
Es muss ein Feld vom Typ "Empfänger" oder "Verborgenes Feld" sein, das die numerische Kennung eines Autors der Website enthält.',
	'traiter_email_option_destinataires_label' => 'Empfänger',
	'traiter_email_option_destinataires_plus_explication' => 'Eine kommagetrennte Empfängerliste.',
	'traiter_email_option_destinataires_plus_label' => 'Weitere Empfänger', # MODIF
	'traiter_email_option_destinataires_selon_champ_explication' => 'Ermöglicht die Angabe von Empfängern auf der Grundlage eines Feldwerts.
	Geben Sie das Feld, seinen Wert und die betreffende(n) E-Mail(s) (durch Komma getrennt) in diesem Format an, z. B.: "@selection_1@/choix1: mail@example.tld".  Sie können mehrere Bedingungen angeben, wobei zwischen den einzelnen Bedingungen ein Zeilenumbruch sein muss.',
	'traiter_email_option_destinataires_selon_champ_label' => 'Empfänger anhand eines Feldes',
	'traiter_email_option_envoyeur_courriel_explication' => 'Legen sie das Feld fest, das die Absenderadresse enthält.', # MODIF
	'traiter_email_option_envoyeur_courriel_label' => 'Absenderadresse', # MODIF
	'traiter_email_option_envoyeur_nom_explication' => 'Definieren sie den Namen mit Hilf der @raccourcis@ (siehe Merkzettel). Wenn sie nichts eintragen, wird der Name der Website verwendet.',
	'traiter_email_option_envoyeur_nom_label' => 'Name des Versands',
	'traiter_email_option_exclure_champs_email_explication' => 'Wenn Sie möchten, dass bestimmte Felder in den gesendeten E-Mails nicht angezeigt werden (z. B. versteckte Felder), definieren Sie sie einfach hier, getrennt durch ein Komma. Wenn Sie ein Fieldset ausschließen, werden alle seine Unterfelder ebenfalls ausgeschlossen.', # MODIF
	'traiter_email_option_exclure_champs_email_label' => 'Felder, die vom Inhalt der Nachricht ausgeschlossen werden sollen',
	'traiter_email_option_masquer_champs_vide_label_case' => 'Leere Felder ausblenden',
	'traiter_email_option_masquer_liens_label_case' => 'Admin-Links in der E-Mail ausblenden',
	'traiter_email_option_masquer_valeurs_accuse_label_case' => 'Die Werte der Antwort nicht in der Empfangsbestätigung senden', # MODIF
	'traiter_email_option_modification_reponse_label_case' => 'Keine E-Mail senden, wenn eine bereits gespeicherte Antwort geändert wird', # MODIF
	'traiter_email_option_nom_envoyeur_accuse_explication' => 'Geben Sie den Namen der Person an, die die Empfangsbestätigung sendet. Wenn Sie nichts eintragen, ist es der Name der Website.', # MODIF
	'traiter_email_option_nom_envoyeur_accuse_label' => 'Name für den Versand der Empfangsbestätigung', # MODIF
	'traiter_email_option_pj_explication' => 'Wenn die geposteten Dokumente klein genug sind (die maximale Größe kann vom Webmaster über die Konstante _FORMIDABLE_TAILLE_MAX_FICHIERS_EMAIL festgelegt werden).',
	'traiter_email_option_pj_label' => 'Dateien an die E-Mail anhängen',
	'traiter_email_option_sujet_accuse_label' => 'Betreff der Empfangsbestätigung', # MODIF
	'traiter_email_option_sujet_explication' => 'Definieren sie den Betreff mit Hilf der @raccourcis@. Wenn sie das Feld freilassen, wird der Inhalt automatisch erstellt.',
	'traiter_email_option_sujet_label' => 'Betreff der E-Mail', # MODIF
	'traiter_email_option_sujet_modif_reponse_label' => 'Antwort berabeitet',
	'traiter_email_option_sujet_modif_reponse_label_case' => 'Wenn eine Antwort geändert wurde, dies im Suffix des Betreffs anzeigen',
	'traiter_email_option_sujet_valeurs_brutes_explication' => 'Aktivieren Sie dieses Kästchen, wenn die E-Mail an einen Computerroboter gerichtet ist. In diesem Fall enthält die Betreffzeile der E-Mail die Rohwerte (für Roboter verständlich) der Felder und nicht die interpretierten Werte (für Menschen verständlich).',
	'traiter_email_option_sujet_valeurs_brutes_label_case' => 'Rohwerte in der Betreffzeile der E-Mail verwenden', # MODIF
	'traiter_email_option_texte_accuse_explication' => 'Bauen Sie den Text mithilfe von @raccourcis@ auf. Wenn Sie nichts einfügen, wird die Rückmeldung des Formulars verwendet.',
	'traiter_email_option_texte_accuse_label' => 'Text der Empfangsbestätigung', # MODIF
	'traiter_email_option_vrai_envoyeur_explication' => 'Einige SMTP-Server erlauben es nicht, eine beliebige E-Mail-Adresse für das "From"-Feld zu verwenden. Aus diesem Grund fügt Formidable standardmäßig die E-Mail-Adresse des Absenders in das Feld "Reply-To" ein und verwendet die E-Mail des Webmasters im Feld "From". Klicken Sie hier, um die E-Mail-Adresse der Absenders in das "From"-Feld einzufügen.',
	'traiter_email_option_vrai_envoyeur_label' => 'Absenderadresse in das Feld "From" einfügen.',
	'traiter_email_page' => '<a href="@url@">Aus der Seite</a>.',
	'traiter_email_sujet' => '@nom@ hat ihnen geschrieben.',
	'traiter_email_sujet_accuse' => 'Danke für ihre Antwort.',
	'traiter_email_sujet_courriel_label' => 'Betreff der E-Mail', # MODIF
	'traiter_email_titre' => 'Per Mail senden', # MODIF
	'traiter_email_type_destinataires_label' => 'Empfänger',
	'traiter_email_url_enregistrement' => 'Sie können alle Antworten <a href="@url@">auf dieser Seite verwalten</a>.',
	'traiter_email_url_enregistrement_precis' => 'Sie können Ihre Antwort <a href="@url@">auf dieser Seite einsehen</a>.',
	'traiter_enregistrement_description' => 'Einträge in das Formular in der Datenbank speichern',
	'traiter_enregistrement_divers' => 'Sonstiges',
	'traiter_enregistrement_donnees_personelles' => 'Persönliche Daten',
	'traiter_enregistrement_erreur_base' => 'Beim Speichern in der Datenbank ist ein Fehler aufgetreten',
	'traiter_enregistrement_erreur_deja_repondu' => 'Sie haben dieses Formular bereits ausgefüllt.',
	'traiter_enregistrement_erreur_edition_reponse_inexistante' => 'Die zu bearbeitende Antwort wurde nicht gefunden.',
	'traiter_enregistrement_identification_reponses' => 'Identifikation der Antworten',
	'traiter_enregistrement_message_ok' => 'Vielen Dank. Ihre Antworten wurden gespeichert.',
	'traiter_enregistrement_option_anonymiser_label' => 'Den Benutzernamen der angemeldeten Person nicht speichern',
	'traiter_enregistrement_option_auteur' => 'Formularverwaltung zuweisen',
	'traiter_enregistrement_option_auteur_explication' => 'Jedem Formular werden Autoren oder Autorinnen zugewiesen. Wenn diese Option aktiviert ist, können nur diese Personen auf die Antworten des Formulars zugreifen.',
	'traiter_enregistrement_option_effacement_delai_label' => 'Anzahl der Tage bis zur Löschung',
	'traiter_enregistrement_option_effacement_label' => 'Regelmäßig die ältesten Ergebnisse löschen',
	'traiter_enregistrement_option_identification_explication' => 'Welches Verfahren sollte zuerst verwendet werden, um die zueltzt vom Internetnutzer gegebene Antwort zu erfahren?',
	'traiter_enregistrement_option_identification_label' => 'Methode zur Identifizierung',
	'traiter_enregistrement_option_identification_variable_php_explication' => 'Erfordert eine Identifizierung über PHP / Server, die nicht nativ in SPIP integriert ist.',
	'traiter_enregistrement_option_identification_variable_php_label' => 'PHP-Variable',
	'traiter_enregistrement_option_invalider_explication' => 'Wenn die Antworten auf dieses Formular öffentlich verwendet werden, können Sie den Cache bei einer neuen Antwort aktualisieren.',
	'traiter_enregistrement_option_invalider_label' => 'Cache aktualisieren',
	'traiter_enregistrement_option_ip_label' => 'IPs aufzeichnen (nach einer Schutzfrist ausgeblendet)',
	'traiter_enregistrement_option_moderation_label' => 'Moderation',
	'traiter_enregistrement_option_moderer_admins_explication' => 'Auch die Antworten der Admins moderieren',
	'traiter_enregistrement_option_modifiable_explication' => 'Bearbeitbar: Die Nutzer können ihre Antworten nachträglich ändern.',
	'traiter_enregistrement_option_modifiable_label' => 'Antworten editierbar',
	'traiter_enregistrement_option_multiple_explication' => 'Mehrfach: Eine Person kann mehrmals antworten', # MODIF
	'traiter_enregistrement_option_multiple_label' => 'Mehrfache Antworten',
	'traiter_enregistrement_option_php_auth_user_label' => 'Server-Variable: PHP_AUTH_USER',
	'traiter_enregistrement_option_remote_user_label' => 'Server-Variable: REMOTE_USER',
	'traiter_enregistrement_option_resume_reponse_explication' => 'Diese Zeichenfolge wird verwendet, um eine Zusammenfassung jeder Antwort in den Listen anzuzeigen. Felder wie  <tt>@input_1@</tt> werden ersetzt, wie im Merkzettel angezeigt.',
	'traiter_enregistrement_option_resume_reponse_label' => 'Anzeige Zusammenfassung der Antwort',
	'traiter_enregistrement_titre' => 'Ergebnisse speichern',
	'traiter_enregistrement_unicite_champ' => 'Eindeutigkeit der Antworten', # MODIF

	// V
	'voir_exporter' => 'Formular exportieren',
	'voir_numero' => 'Formular Nummer:',
	'voir_reponses' => 'Antworten anzeigen',
	'voir_traitements' => 'Verarbeitungsoptionen',
];
