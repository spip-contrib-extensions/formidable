<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-formidable?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// F
	'formidable_description' => 'Creazione di moduli con interfaccia grafica e configurazione dei trattamenti associati (invio di e-mail, registrazione delle risposte ecc...).',
	'formidable_slogan' => 'Generatore di moduli',
];
