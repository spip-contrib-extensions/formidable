<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/formulaires_reponse?lang_cible=it
// ** ne pas modifier le fichier **

return [

	// C
	'changer_statut' => 'Questa risposta al modulo è:',

	// F
	'formulaires_reponse_numero' => 'Modulo di risposta numero @nb@',

	// I
	'icone_creer_formulaire' => 'Crea un nuovo modulo di risposta',

	// M
	'modifier_formulaire' => 'Modifica questo modulo di risposta',

	// R
	'reponses_donnees' => 'Dati delle risposte:',

	// T
	'titre_formulaires_reponse' => 'Modulo di risposta',
	'titre_formulaires_reponses' => 'Moduli di risposta',
	'titre_logo_formulaires_reponse' => 'Logo di questo modulo di risposta',
];
